---
layout: default3
title: VOXL 2
nav_order: 20
has_children: true
permalink: /voxl-2/
parent: VOXL Autopilot & Compute
summary: VOXL 2 is ModalAI's next-gen autonomous computing platform built around the Qualcomm Flight RB5 5G Platform using the QRB5165 processor. 
thumbnail: /voxl2/m0054-hero-f.png
buylink: https://www.modalai.com/products/voxl-2
---

# VOXL 2
{: .no_toc }

VOXL 2 is ModalAI's next-gen autonomous [Blue UAS Framework](https://www.diu.mil/blue-uas) computing platform is built around the Qualcomm Flight RB5 5G Platform using the QRB5165 processor.

<a href="https://www.modalai.com/products/voxl-2" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/26/voxl-2" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

<img src="/images/voxl2/m0054-hero-f.png" alt="m0054-hero-f"/>


