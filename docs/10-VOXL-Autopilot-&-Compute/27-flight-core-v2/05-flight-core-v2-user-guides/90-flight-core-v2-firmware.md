---
layout: default
title: Flight Core v2 Firmware
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 90
permalink: /flight-core-v2-firmware/
---

# Flight Core v2 Firmware
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Release Notes


## PX4 v1.14

Starting with PX4 1.14, our flight core and VOXL2-SDSP PX4 firmwares are now built from the same repo. The flight Core V2 firmware can be found here: [http://voxl-packages.modalai.com/dists/fc-v2/](http://voxl-packages.modalai.com/dists/fc-v2/)

The SDK-1.1 folder contains the PX4 firmware that's tested with SDK-1.1 running on VOXL.

Make sure you upgrade your flight core firmware with the file included in the voxl_SDK_1.1.0.tar.gz SDK installer. The SDK install script will only flash VOXL, you will need to follow the instructions below to flash the flight core.



# General User Guide

## QGroundControl FW Update

The FW update feature in QGC via USB requires QGC 4.3.0 and later. 

## How to Upgrade using `px_uploader` Tool

Alternatively, to update the firmware, you can use the [px_uploader](https://github.com/PX4/PX4-Autopilot/blob/main/Tools/px_uploader.py) tool.

This has been tested on OSX 13.1, Ubuntu 18.04, and Ubuntu 22.04 with success to update firmware over USB.

- Ensure you have Python 3 and `pyserial` module installed (`pip3 install pyserial`)
- Follow build guide below, or download the firmware from links above in release notes (e.g. modalai_fc-v2_default.px4)
- Obtain the PX4 open source community tool (not a ModalAI tool):

```
git clone -b modalai-1.13.2 https://github.com/modalai/px4-firmware.git --recursive
cd px4-firmware
```

- Exit QGC if running
- Disconnect Flight Core v2 from USB
- Run the following (note: your USB device name may be different and your path may be different, Linux default is `/dev/ttyACM1`, OSX default is `/dev/cu.usbmodem01`)

```
./Tools/px_uploader.py --port /dev/ttyACM1 modalai_fc-v2_default.px4 --baud-bootloader 1152000
```

- Connect Flight Core v2 over USB (note, no external power needed)
- Flight Core v2 boots into bootloader mode
- The update should now begin


# Developer Guide

## v1.13.3 PX4 Support

ModalAI makes some mods to mainline and maintains a branch [here](https://github.com/modalai/px4-firmware/tree/modalai-1.13.3) that is shipped on the product by default.

## Mainline PX4 Support

- PX4 mainline Board Support is available at [boards/modalai/fc-v2](https://github.com/PX4/PX4-Autopilot/tree/main/boards/modalai/fc-v2)

## ArduPilot Support

ModalAI's Flight Core 2 is integrated into ArduPilot by the great ArduPilot community. The engineers at ModalAI do not have experience with ArduPilot, so can not offer significant support. It is recommended to find support through the normal ArduPilot channels ([boards](https://discuss.ardupilot.org/) & [Discord](https://ardupilot.org/dev/docs/ardupilot-discord-server.html)). Hopefully ModalAI will gain experience with ArduPilot in the future.

ArduPilot pull request for ModalAI Flight Core v2 base hardware support [Github](https://github.com/hendjoshsr71/ardupilot/tree/pr/add_modalai_fcv2)

ArduPilot pull request for VOXL-compatible Visual Inertial Odometry support [Github](https://github.com/ArduPilot/ardupilot/pull/26277)

## Tools for Flashing

Recommended hardware: similar to [STLink v2](https://www.st.com/en/development-tools/st-link-v2.html) or equivalent.  Please install [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) to get the `STM32_Programmer_CLI` binary.

## How to Build PX4 Application

```
git clone -b modalai-1.13.3 https://github.com/modalai/px4-firmware.git --recursive
cd px4-firmware
make modalai_fc-v2
```

After building, the bootloader binary is located at:

```
build/modalai_fc-v2_default/modalai_fc-v2_default.bin
```

### How to Flash PX4 Bootloader with ST32CubeProgrammer

The application is flashed at `0x08020000`:

```
echo "programming application..."
STM32_Programmer_CLI -c port=SWD -w modalai_fc-v2_default.bin 0x08020000 -v
```

## Bootloader

The bootloader is flashed at the factor and generally you do not need to build or flash it.  But, if you want to customize the bootloader, the following can be used.

### How to Build PX4 Bootloader

```
git clone -b modalai-1.13.2 https://github.com/modalai/px4-firmware.git --recursive
cd px4-firmware
make modalai_fc-v2_bootloader
```

After building, the bootloader binary is located at:

```
build/modalai_fc-v2_bootloader/modalai_fc-v2_bootloader.bin
```

### How to Flash PX4 Bootloader with ST32CubeProgrammer

The bootloader is flashed at `0x08000000`:

```
echo "erasing..."
STM32_Programmer_CLI -c port=SWD -e all

echo "programming bootloader..."
STM32_Programmer_CLI -c port=SWD -w modalai_fc-v2_bootloader.bin 0x08000000 -v
```
