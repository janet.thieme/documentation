---
layout: default
title: Flight Core v2
nav_order: 27
has_children: true
permalink: /flight-core-v2/
parent: VOXL Autopilot & Compute
summary: Flight Core is an H7 based PX4 Drone Flight Controller - Assembled in the USA. Flight Core can be paired with VOXL for obstacle avoidance and indoor or outdoor GPS-denied navigation. 
thumbnail: /flight-core-v2/m0087-hero-trans.png
---

# Flight Core v2
{: .no_toc }

<img src="/images/flight-core-v2/m0087-hero.png" width="320"/>

Flight Core v2 is a [Blue UAS Framework](https://www.diu.mil/blue-uas) PX4 Drone Flight Controller (STM32**H7**-based) - Assembled in the USA. Flight Core v2 can be paired with VOXL for obstacle avoidance and indoor or outdoor GPS-denied navigation. Flight Core v2 can also be used independently as a standalone, high-performance, secure flight controller. Flight Core v2 is a part of the Blue UAS Framework and is NDAA '20 Section 848 compliant.

Buy [Here](https://www.modalai.com/products/flight-core-v2)

Still have questions? Check out the forum [Here](https://forum.modalai.com/category/10/flight-core)
