---
layout: default
title: First Flight
parent: Flight Core Quickstarts
nav_order: 7
permalink: /fc-first-flight/
---

# First Flight

## Take Off in Manual Mode

First take off in Manual Flight Mode just to confirm the basic functionality of the quadcopter (motor rotation directions, sensor calibration, radio stick directions). If you configured your radio switches according to the [radio configuration page](/configure-rc-radio/), the following instructions should match exactly:

 * Flip the motor Kill switch DOWN
 * Flip the Flight Mode switch UP (manual mode)
 * Hold the right stick centered and left stick down and right to ARM
 * Move left stick to bottom-middle position.
 * Slowly raise left stick up to spool up the motors and take off.

## To Land in Manual Mode

 * Slowly drop throttle until the quadcopter touches the ground.
 * Hold the throttle stick all the way down until the system DISARMS
 * If connected, QGroundControl will verbally confirm that the system is DISARMED
 * The motors will keep spinning slowly (idling) for a few seconds even after disarming.
 * Flip the motor kill switch UP to kill the motors.


## Next Steps

Now it's time to experiment with [voxl-vision-hub](/voxl-vision-px4/) and try flying in VIO mode!

