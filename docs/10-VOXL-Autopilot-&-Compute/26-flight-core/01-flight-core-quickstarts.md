---
layout: default
title: Flight Core Quickstarts
parent: Flight Core 
nav_order: 1
has_children: true
permalink: /flight-core-getting-started/
---

# Flight Core Getting Started

How to get started with ModalAI Flight Core
