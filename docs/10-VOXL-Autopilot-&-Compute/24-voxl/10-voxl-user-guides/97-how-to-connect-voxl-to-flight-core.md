---
layout: default
title: How to connect VOXL to Flight Core
parent: VOXL User Guides
nav_order: 97
has_children: false
permalink: /how-to-connect-voxl-to-flight-core/
---

# How to connect Flight Core to VOXL
{: .no_toc }

## Board Connections

![voxl-usb-expander-and-debug.png](../../../images/flight-core/flight-core-to-voxl-serial-connection.png)

- The `MCBL-00007` cable can be used.  The following pinout describes the cable:

| Pin # | VOXL - J12,  DF13-6S-1.25C connector | Flight Core - J1, DF13-6S-1.25C connector|
|:---   |:---                                  | :---                                     |
| 1     | NC                                   | NC                                       |
| 2     | UART_TX_5V                           | UART_RX_5V                               |
| 3     | UART_RX_5V                           | UART_TX_5V                               |
| 4     | NC                                   | NC                                       |
| 5     | GND                                  | GND                                      |
| 6     | NC                                   | NC                                       |

## Hardware Setup

- Disconnect power from VOXL and Flight Core
- Connect the VOXL's `J12` to the Flight Core's `J1` using the `MCBL-00007` cable
- Reconnect power to VOXL and Flight Core

## Software Setup

- Flight Core by default is configured to communicate with VOXL using the MAVLINK_1 instance at 916.2kHz in normal mode using the TELEM2 serial port
- Detailed usage is provided in the [voxl-vision-hub](/voxl-vision-px4/) section

[Next: Connecting VOXL to Pixhawk](/how-to-connect-voxl-to-pixhawk/){: .btn .btn-green }