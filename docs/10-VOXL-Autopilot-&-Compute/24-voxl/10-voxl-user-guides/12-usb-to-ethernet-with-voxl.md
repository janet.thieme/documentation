---
layout: default
title: Using USB to Ethernet with VOXL
parent: VOXL User Guides
nav_order: 12
permalink: /usb-to-ethernet-with-voxl/
---

# Using USB to Ethernet with VOXL
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

Resources for using Ethernet on VOXL platform. Allows VOXL to communicate with other devices via Ethernet. There are two different hardware configurations below to choose from.

## Setup using VOXL USB Expander Board

![voxl-dk](../../images/voxl/usb-to-ethernet-1.png)
![voxl-dk](../../images/voxl/usb-to-ethernet-closeup.png)

### Required Hardware

- VOXL 
- VOXL USB Expander Board (Usage info can be found [here](https://docs.modalai.com/usb-expander-and-debug-manual/))
- USB Type-A Female to JST Cable (`MCBL-00009-1`)
- USB to Ethernet Adapter ([Here](https://www.amazon.com/Apple-MC704LL-A-Ethernet-Adapter/dp/B00W7W9FK0/ref=sr_1_3?keywords=usb+to+ethernet+apple&qid=1575492214&s=electronics&sr=1-3) is one that is known to work)
- Ethernet Cable

### Hardware Setup

- Disconnect power from the VOXL
- Attach the daughter board’s `J1` connector to VOXL’s `J13` connector
- Attach the `MCBL-00009-1` cable to the `USB2 Host` connector on the daughter board
- Attach the USB male side of the USB to Ethernet adapter to the USB female side of the `MCBL-00009-1` cable
- Connect the Ethernet cable to other side of the USB to Ethernet adapter
- Connect other side of the Ethernet cable to the device you would like to communicate with
- Reconnect power to the VOXL


[Next: VOXL GPIO](/voxl-gpio-io/){: .btn .btn-green }