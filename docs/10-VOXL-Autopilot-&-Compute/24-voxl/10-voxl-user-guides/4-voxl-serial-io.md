---
layout: default
title: VOXL Serial IO
parent: VOXL User Guides
nav_order: 4
permalink: /voxl-serial-io/
---

# VOXL Serial IO
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

All serial ports (UART & I2C) and GPIO pins that are broken out on VOXL's headers are internally mapped to the Sensors DSP (SDSP). The main benefit of this is that low-level and time-sensitive interaction to sensors and telemetry communications can be handled by the SDSP's real-time operating system. This may also free up CPU cycles on the applications processor.

This does, however, mean that you cannot talk to the serial ports with reads and writes to /dev/i2cX or /dev/ttyOX as you would on other embedded Linux systems. We provide a simple library, libvoxl-io to enable communication with serial ports from the applications processor (Linux userspace). This layer hides the SDSP RPC calls and allows compiling programs that use the serial ports without needed the Hexagon DSP toolchain or build environment.

The libvoxl-io library header and API description can be found here:
[https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/include/voxl_io.h](https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/include/voxl_io.h)

The library is included with the [VOXL Software Bundle](/voxl-supporting-software/).


## Port Numbers

A look inside the libvoxl-io API header, voxl-io.h, shows the VOXL port numbers have pre-defined macros for easy of use.

[https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/include/voxl_io.h](https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/include/voxl_io.h)

```C
#define UART_J7     9   // BLSP 9  on physical port J7  pins 2&3
#define UART_J10    7   // BLSP 7  on physical port J10 pins 2&3
#define UART_J11    12  // BLSP 12 on physical port J11 pins 2&3
#define UART_J12    5   // BLSP 5  on physical port J12 pins 2&3
```

More details on the above ports and their pinouts can be seen on the [datasheet page](/voxl-datasheet/).

## RPC Shared Memory

To speed up communication between the applications processor and SDSP, we recommend the user to use RPC shared memory for read and write buffers so as to avoid the system needing to copy memory back and forth between the two processors. This can be done easily with the following libvoxl-io functions:

```C
uint8_t* voxl_rpc_shared_mem_alloc(size_t bytes);
void voxl_rpc_shared_mem_free(uint8_t* ptr);
void voxl_rpc_shared_mem_deinit();
```

See the examples section for more information:


## Examples

A very simple UART example is the uart loopback tester that's built into libvoxl-io. This performs reads and writes as well as shows some of the configuration options available.

[https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/apps/voxl-uart-loopback.c](https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/apps/voxl-uart-loopback.c)

A more advanced example on how to use the UART interface is the voxl-vision-px4 program which uses UART to communicate with a PX4 flight controller via the MAVlink protocol. This example performs both read and writes to a UART port as well as utilizes RPC shared memory buffers.

[https://gitlab.com/voxl-public/voxl-vision-px4/blob/master/src/uart_mavlink.c](https://gitlab.com/voxl-public/voxl-vision-px4/blob/master/src/uart_mavlink.c)

## I2C

Coming soon!


[Next: VOXL System Image](/voxl-system-image/){: .btn .btn-green }