---
layout: default
title: How to use VOXL as USB Host
parent: VOXL User Guides
nav_order: 32
permalink: /voxl-as-usb-host/
---

# How to use VOXL as USB Host
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

Using VOXL or VOXL Flight's USB 3.0 OTG Port (J8), the system can be used as a USB3.0 Host provided you use a non-standard USB3.0 cable.  It can be used as a USB2.0 host with a standard micro-USB2.0 cable.  One or more additional USB2.0 host ports are available on VOXL Add-ons ([USB Expander and Debug](/usb-expander-and-debug-datasheet/), [LTE v2](/lte-modem-and-usb-add-on-v2-datasheet/), [Microhard v2](/microhard-add-on-datasheet/))

### Background

Because the USB3.0 SS pins on the VOXL cannot support TX/RX swapping, they were set to peripheral mode since the Snapdragon 821 is used mainly in phones and peripheral mode is way more commonly used in a phone configuration.

In order for systems to use the standard micro-b to type A female OTG cables, they need to do TX/RX swapping.

Since VOXL can't do this, it needs to use a non-standard pinout as it's default configuration is peripheral mode.

The pinout can be found here: [USB3.0 Female A Plug to Micro-B](/cable-datasheets/#mcbl-00019)

### Procedure

1. Plug in your USB device via a compatible USB3.0 Female A Plug to Mirco-B cable - in our case, we have a [USB3.0 Female A Plug to Micro-B](/cable-datasheets/#mcbl-00019) plug in the VOXL's J8 port.

_Note: Some customers report success using USB3 with a USB3 OTG adapter like [this](https://www.amazon.de/deleyCON-Super-Speed-Adapter-Buchse-mehrfarbig/dp/B00WHZASVM/)._

1. Run ``lsusb -v``
    * Check ``bcdUSB`` field for USB Version

![USB3.0 Female A Plug to Micro-B](/images/voxl/usb3cable.jpg)

Here's an example of a USB3 device:

```bash
Bus 004 Device 003: ID 0781:5581 SanDisk Corp. Ultra
Device Descriptor:
  bLength                18
  bDescriptorType         1
  bcdUSB               3.20
  bDeviceClass            0 
  bDeviceSubClass         0 
  bDeviceProtocol         0 
  bMaxPacketSize0         9
  idVendor           0x0781 SanDisk Corp.
  idProduct          0x5581 Ultra
  bcdDevice            1.00
  iManufacturer           1  USB
  iProduct                2  SanDisk 3.2Gen1
```


Here's an example of a USB2 device:

```bash
Bus 003 Device 003: ID 045e:00f8 Microsoft Corp. LifeCam NX-6000
Device Descriptor:
  bLength                18
  bDescriptorType         1
  bcdUSB               2.00
  bDeviceClass          239 Miscellaneous Device
  bDeviceSubClass         2 
  bDeviceProtocol         1 Interface Association
  bMaxPacketSize0        64
  idVendor           0x045e Microsoft Corp.
  idProduct          0x00f8 LifeCam NX-6000
  bcdDevice            1.00
  iManufacturer           1 Microsoft
  iProduct                2 Microsoft? LifeCam NX-6000
  iSerial                 0 
  bNumConfigurations      1
```

[Next: VOXL Tracking Sensor Guide](/voxl-tracking-sensor-user-guide/){: .btn .btn-green }