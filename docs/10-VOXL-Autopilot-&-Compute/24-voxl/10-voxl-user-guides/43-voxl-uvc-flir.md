---
layout: default
title: How to stream Flir Video (RTSP)
parent: VOXL User Guides
nav_order: 43
permalink: /voxl-uvc-flir/
---

# VOXL Flir UVC Video Streaming (RTSP)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

This user guide provides the necessary steps to stream Flir Boson or Lepton over RTSP network using the USB UVC interface and the VOXL system.

In short, we'll be doing the following:

- Connect a Flir USB camera to the VOXL's V2 Debug Board
- Use [voxl-streamer](https://docs.modalai.com/voxl-streamer/) on the VOXL to stream the video to the host computer
- Use either VLC or QGC on a host computer to view the video stream

## Requirements

### Hardware

| Part Number | Description | Link |
| --- | --- | --- |
| Host Computer | Ground Control Station (Ubuntu 18.04) | 
| VOXL-DK | VOXL Development Kit | [Purchase](https://www.modalai.com/voxl)
| MPS-00001-1 | 5V/6A Power Supply | [Purchase](https://www.modalai.com/collections/accessories/products/voxl-acc-ps?variant=32029899849779)
| MCBL-00009-1 | USB Cable (Host, 4-pin JST to USB 2.0 Type A Female) | [Purchase](https://www.modalai.com/products/usb-cable-4-pin-jst-to-usb-a-female)
| Flir Boson or Flir Lepton |  | 

#### Boson

The Boson requires more current than the USB 2.0 standard supports. To achieve this current, the power and ground can be connected separately in a "y-cable" to an alternate 5V power source. Alternatively, ModalAI has a debug board with a direct connection to the core 5V supply on VOXL which provides enough current. This Debug Board V2 can be found below.

| Part Number | Description | Link |
| --- | --- | --- |
| MDK-M0078-2 | VOXL Debug Expansion V2 Add-on | [Purchase](https://www.modalai.com/products/voxl-debug-board-v2)

#### Lepton

One of the following VOXL Add-on boards is required.

| Part Number | Description | Link |
| --- | --- | --- |
| MDK-M0048-1-01 | VOXL Microhard Modem Add-on | [Purchase](https://www.modalai.com/microhard-add-on)
| MDK-M0017-1-01| VOXL USB Expansion Board with Fastboot and Emergency Boot | [Purchase](https://www.modalai.com/products/voxl-usb-expansion-board-with-debug-fastboot-and-emergency-boot)
| MDK-M0030-1-01 | VOXL Cellular LTE and USB hub add-on  | [Purchase](https://www.modalai.com/4g-add-on)

### Target Software

[VOXL-SDK](https://docs.modalai.com/voxl-sdk) version XXX or greater.

### Host Software

- On the host computer, install [VLC media player](https://www.videolan.org/vlc/index.html), which we'll used to display the RTSP video:

```bash
sudo apt-get install vlc
```

## Setup Hardware

- Disconnect power from the VOXL
- Attach the Debug Board to VOXL J13
- Attach the JST cable to the JST USB connector
- Set the jumper to 5V APM setting
- Attach the Flir camera to the USB connector on the JST to USB Female A cable
- Connect power to the VOXL

![voxl-boson-setup](/images/voxl-sdk/flir/voxl-boson-setup.jpg)

## Network Configuration

- Configure the VOXL as an Access Point (or connect it to a network)
  - instructions on how to do this are [here](/wifi-setup/)
  - the default IP address of the device will be `192.168.8.1`
- Connect the host computer to the VOXL's Access Point (default password is 1234567890) or to the same network you connected the VOXL to

## Run Software on Target

### voxl-uvc-server

- SSH onto the target (see [here](/ssh-to-voxl/) for details) or ADB in using a micro-usb to USB cable
- First, validate the the USB device is found using the `lsusb` command:

```
root@apq8096:~# lsusb
```

#### Configure voxl-uvc-server

Start [voxl-uvc-server](https://docs.modalai.com/voxl-uvc-server/) with the following command

Boson:
```
voxl-uvc-server -r 640x512
```

Lepton:
```
voxl-uvc-server -r 160x120 -f 9
```

More details in the [voxl-uvc-server](https://docs.modalai.com/voxl-uvc-server/) documentation.

#### Configure voxl-streamer

Start [voxl-streamer](https://docs.modalai.com/voxl-streamer/) with the following command

Boson:
```
voxl-streamer -c uvc-flir-boson
```

Lepton:
```
voxl-streamer -c uvc-flir-lepton
```

More details in the [voxl-streamer](https://docs.modalai.com/voxl-streamer/) documentation.

## Viewing Stream

### VLC

- On the host system, run the VLC software

- Go to `Media > Open Network Stream`

- Enter the following URL: `udp://@:4242`

- Click `Play` to view the video stream

### QGroundControl

Before attempting to view the video stream, follow these instructions on connecting to QGC: [How to Connect QGroundControl to VOXL using WiFi](https://docs.modalai.com/qgc-wifi/#detailed-steps)


- After your connection has been established to QGC, press the purple QGC logo in the top left corner in order to access the `Application Settings` menu.

- Under the `General` tab, scroll down until you find the `Video` section.

- Under the `Video Source` dropdown, choose `RTSP (h.264) Video Stream`.

- In the `UDP Port` field enter the default: `4242`

- You will now be able to view the video stream under QGC's `Fly` view.

[Next: Connecting VOXL to Flight Core](/how-to-connect-voxl-to-flight-core/){: .btn .btn-green }