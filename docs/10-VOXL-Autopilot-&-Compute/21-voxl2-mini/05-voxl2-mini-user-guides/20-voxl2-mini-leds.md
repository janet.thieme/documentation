---
layout: default
title: VOXL 2 Mini LEDs
parent: VOXL 2 Mini User Guides
nav_order: 20
permalink: /voxl2-mini-leds/
---

# VOXL 2 Mini LEDs
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

Below describes the VOXL 2 Mini LEDs.

<img src="/images/voxl2-mini/m0104-datasheets-leds.jpg" alt="m0104-datasheets-leds" width="1280"/>

## D1 - Power Good LED (Green)

| State          | Description  |
|---             |---           |
| Green          | indicates 3P3 VREG and (emulated) VBAT are good (using AND logic) |
| Off            | indicates either 3P3 VREG or emulated VBAT are not good |

## D2 - 5VDC USB Local Power Good

| State          | Description  |
|---             |---           |
| Green          | indicates 5VDC output for USB is good (U9)|
| Off            | indicates output of U9 is not good |

Green 

## DS2 - User Controllable RGB LED

All 3 colors of the DS2 LED are active high ON.

| State          | Description  |
|---             |---           |
| Red            | GPIO_82 set high, 2.1Vf @5mA |
| Green          | GPIO_83 set high, 3.1Vf @5mA |
| Blue           | GPIO_84 set high,  3.0Vf @5mA |
| Off            | GPIO_82, GPIO_83 and GPIO_84 set low |

## DS4 - PM8150L RGB LED Driver

Future use.  The PM8150L can drive this RGB LED.
