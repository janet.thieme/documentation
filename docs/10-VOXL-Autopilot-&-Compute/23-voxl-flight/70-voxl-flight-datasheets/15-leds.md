---
layout: default
title: VOXL Flight LEDs
parent: VOXL Flight Datasheets
nav_order: 15
permalink: /voxl-flight-datasheet-leds/
---

# VOXL Flight LEDs
{: .no_toc }

---

![VOXL-Flight LEDs](/images/voxl-flight/voxl-flight/voxl-flight-leds.png)

## LED1

LED1 is marked on silkscreen as "DS1000".  It's  tri-color RGB LED.

### Bootloader Mode

Upon power up, while attached to USB, the device will stay in the bootloader mode for 5 seconds.

| Color | Pattern | Status |
| --- | --- | --- |
| Green | Fast Flashing (~10 Hz) | In bootloader, waiting |
| Yellow/Red | Alternating | Serial FW update in progress |

### Normal Mode

| Color | Pattern | Status |
| --- | --- | --- |
| Green | Solid | Powered |
| Blue | Flashing (~2 Hz)| Disarmed |
| Blue | Solid | Armed |
| Red | Solid | Error, hard fault |

### Sensor Calibration Mode

| Color | Pattern | Status |
| --- | --- | --- |
| Green | Solid | Powered |
| Blue | Flashing (~20 Hz)| Calibration in progress |

## LED2

LED2 is marked on silkscreen as "DS1001".  It's  tri-color RGB LED.  **For future use**.

## LED3

LED3 is marked on silkscreen as "DS1".  It's  tri-color RGB LED.  **For future use**.


[Next: VOXL Flight IMUs](/voxl-flight-datasheet-imus/){: .btn .btn-green }
