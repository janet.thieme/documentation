---
layout: default
title: VOXL 2/VOXL2 Mini System Image
parent: VOXL 2 System Image
nav_order: 10
permalink: /voxl2-voxl2-mini-system-image/
youtubeId1: IM3PnW7cipQ
youtubeId2: W0b9CxMQOJk
---

# VOXL 2 and VOXL 2 Mini System Image
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

<b>What is it?</b>

The system image is essentially "the operating system", and consists of the kernel, root file system, and various firmware files for the other processors on board.

<b>How to Install</b>

The system image ships in VOXL SDK releases.  Please see [here](/flash-system-image/#flashing-a-voxl-sdk-release) for instructions.

<b>Supported Targets</b>

This documentation supports VOXL 2 (M0054) and VOXL 2 Mini (M0104).

## System Image Changelog

<table>
  <!-- HEADER -->
  <tr>
    <th>Version / Release</th>
    <th>Build Info</th>
    <th>Changes</th>
  </tr>

<!-- template
  <tr>
    <td style="vertical-align:top">

    </td>
    <td style="vertical-align:top">

    </td>

    <td style="vertical-align:top">

    </td>
  </tr>
   -->

   <!-- 1.7.8 -->
  <tr>
    <td style="vertical-align:top">
      1.7.X <br>
      (voxl2_SDK_1.3.X)<br>
      (voxl2_mini_SDK_1.3.X)<br>
      <br>
      <br>
    </td>
    <td style="vertical-align:top">
      SDK Version: 1.3.0<br>
      Release Date: ~5/24 <br> 
    </td>
    <td style="vertical-align:top">
      <b>BSP:</b><br>
      - M0104: Fix issue with hires sensors on slots 0-2<br>
      - Change GPIO 110-114 to default inputs to leave floating (fixes ov7251 stereo sync isuue)
      - Add var00.1 and var02.1 for M0173 support
      - Default gpio109 for fsync for M0173 supported kernels
      <br>
      <b>Camera (camx/chi-cdk):</b><br>
      - backport fix to select multiple tuning files<br>
      - imx412_fpv resolution updates - add 4056x3040 30fps<br>
      - ov9782 tuning updates<br>
      - C26-C28: add cameraId 6 in prep for M0173<br>
      - C27: add slave mode enabled AR0144 for M0173 (slots combo 0, 2, combo 6)
      - C28: add IMX412 slot 6 
    </td>
  </tr>
  
  <!-- 1.7.6 -->
  <tr>
    <td style="vertical-align:top">
      1.7.6 <br>
      (voxl2_SDK_1.2.0)<br>
      (no mini release)<br>
    </td>
    <td style="vertical-align:top">
      SDK Version: 1.2.0<br>
      Release Date: 2024-04-15 <br> 
    </td>
    <td style="vertical-align:top">
      <b>BSP:</b><br>
      - add support for M0104-2 via `var02` variant<br>
      <br>
      <b>Camera (camx/chi-cdk):</b><br>
      - `imx412_fpv` low latency binaries<br>
      - `imx678` latency reduction<br>
      - ship royale-5-8-spectre-6-10<br>
      - `irs2975` binaries (beta level support)<br>
      - `boson_2` binary (alpha level support)<br>
    </td>
     Ubuntu:<br>
      - added neofetch, libsdl2-dev<br>
      <br>
  </tr>

  <!-- 1.7.4 -->
  <tr>
    <td style="vertical-align:top">
      1.7.4 <br>
      (voxl2_SDK_1.1.3)<br>
      (no mini release)<br>
    </td>
    <td style="vertical-align:top">
      SDK Version: 1.1.3<br>
      Release Date: 2024-02-26 <br> 
    </td>
    <td style="vertical-align:top">
      <b>BSP:</b><br>
      - add `voxl-fysnc-mod` kernel module <br>
      - add `voxl-gpio-mod` kernel module <br>
      --- GPIO now exporting to /sys/class/gpio <br>
      - convert SE13 from 4W to 2W UART, freeing GPIO36/37 <br>
      - add `voxl-platform-mod` kernel module <br>
      --- see `/sys/module/voxl_platform_mod/parameters` <br>
      - add new target 'M0054-2' support (same rootfs as M0054-1) <br> - new kernel <br> - new devcfg.mbn<br>
      <br>
      <b>Camera (camx/chi-cdk):</b><br>
      - update `ar0144` binaries<br>
    </td>
  </tr>

  <!-- 1.7.1 -->
  <tr>
    <td style="vertical-align:top">
      1.7.1 <br>
      voxl2_SDK_1.1.0, voxl2_SDK_1.1.1, voxl2_SDK_1.1.2
    </td>
    <td style="vertical-align:top">
      SDK Version: 1.1.0-1.1.2<br>
      Release Date: 2023-11-13 <br> 
      Build Date: TBD
    </td>
    <td style="vertical-align:top">
      <b>BSP:</b><br>
      - disable usb-autosuspend via karg<br>
      - enable CONFIG_HIDRAW<br>
      - M0104: convert SPI14 to UART (/dev/ttyHS0)
      - M0104: remove non-existing peripherals located on B2Bs/camera group 2
      - enable /dev/i2c-5, J5 Pins 98/99 (SDA/SCL) (GPIO 8/9)  <br>
      - disable pcie2 in favor of enabling GPIO 85/86/87/88 on J5 <br>
      - refactor camera-dtsi for readability
      <br>
      <b>Camera (camx/chi-cdk):</b><br>
      - ship `ov7251-fsin` and `ov7251-fsout` binaries<br>
      - ship `imx335` binaries<br>
      - ship `irs1645_2` and `irs1645_3` bins<br>
      - ship `imx412_flip` and `imx678_flip` bins<br>
      - update `imx412` timing to support M0107 based sensor<br>
      - add 4056x3040 and 4208x3120 to camx supported resolutions<br>
      - update ov7251 max gain/exposure settings<br>
      - ship tuned.imx214_m0024 at `/usr/share/modalai/chi-cdk/tuned/`<br>
      - ship `ar0144` at `/usr/share/modalai/chi-cdk/ar0144`<br>
      <b>NON-HLOS:</b><br>
      - update SLPI to 1.1.10 - fix potential buffer overflow bug, increase pass though UART buffer<br>
    </td>
  </tr>

  <!--  1.6.2 -->
  <tr>
    <td style="vertical-align:top">
      1.6.2<br>
      voxl2_SDK_1.0.0
    </td>
    <td style="vertical-align:top">
      SDK Version: 1.0.0 <br>
      Release Date: 2023-07-xx<br> 
      Build Date: 2023-05-19
    </td>
    <td style="vertical-align:top">
      BSP:<br>
        - now using refactored `meta-voxl2-bsp` supporting M0054/M0104<br>
        - add VOXL2 Mini (M0104) support<br>
      <br>
      Camera:<br>
      - use `/usr/share/modalai/chi-cdk` to ship camera sensormodule binaries<br>
      <br>
      HLOS<br>
      - add 8821cu.ko<br>
      - update libfc_sensor 1.0.2 to 1.0.4<br>
      <br>
      NHLOS<br>
      - ModalAI SLPI upgraded to 1.1-7<br>
    </td>
  </tr>

  <!-- 1.5.5 -->
  <tr>
    <td style="vertical-align:top">
      1.5.5<br>
      Platform Release 0.9.5
    </td>
    <td style="vertical-align:top">
      SDK Version: 0.9.5<br>
      Release Date: 2023-03-30<br>
      Build Date: 2023-03-22<br>
    </td>
    <td style="vertical-align:top">
    Camera<br>
    - update IMX678 config:<br>
      - 3840x2160@30 - use 4 lane mode at 891 Mbps<br>
      - 1920x1080@30 - use 4 lane mode at 720 Mbps<br>
      - enable more camera configs (see [here](/voxl2-camera-configs/))<br>
    - revert defaultFpsMax backend flag (use case selection is broken when enabled)<br>
    </td>
  </tr>

  <!-- 1.5.4 -->
  <tr>
    <td style="vertical-align:top">
      1.5.4<br>
      (internal only)
    </td>
    <td style="vertical-align:top">
      Build Date: 2023-02-16
    </td>
    <td style="vertical-align:top">
      HLOS<br>
      - Increase SLPI memory<br>
      - Install `voxl-esc` python depends (pip3,pyserial,numpy)<br>
      - add nano<br>
      <br>
      NHLOS<br>
      - ModalAI SLPI upgraded to 1.1-6<br>
      - Increase SLPI memory<br>
      - Added 420k baud rate UART for SLPI (native ELRS baud rate)<br>
    </td>
  </tr>

  <!-- 1.5.3 -->
  <tr>
    <td style="vertical-align:top">
      1.5.3<br>
      (internal only)
    </td>
    <td style="vertical-align:top">
      Build Date: 2023-02-03
    </td>
    <td style="vertical-align:top">
      BSP (See [Linux User Guide](/voxl2-linux-user-guide/) for details)<br>
      - VOXL2 (M0054)<br>
        - configure GPIO 152 as output, default high, on J5 pin 44 for M0130 (along with GPIO 153, 154, 155 while at it)<br>
        - configure SE1 for I2C `/dev/i2c-4` J7 (camera group 1)<br>
        - enable new GPIO 110/114 pins on J6<br>
        - enable new GPIO 6/7 pins on J7<br>
        - enable new GPIO 12/13 pins on J8<br>
        - enable SE2 (I2C) on J5 pins 8,9 as `/dev/i2c-0`<br>
        - enable SE11 (SPI) on J5 pin 53,56 as `/dev/spidev11.0`<br>
        - update TZ to devcfg ver8<br>
          - see [voxl2-qups](/voxl2-qups/) for details<br>
      <br>
      Camera<br>
      - Added IMX678 Support (M0054-J8, HW sensor ID 4/5 (addresses 0x34/0x20))<br>
        - 1920 x 1080 @ 30 FPS<br>
        - 3840 x 2160 @ 30 FPS<br>
      - ov7251 60/90/120 FPS configs added as dormant (need to add camx proper support)<br>
      <br>
      HLOS<br>
      - Added service file that changes SLPI restart level to avoid board crash when SLPI crashes<br>
      <br>
      NHLOS<br>
      - ModalAI SLPI upgraded to 1.1-4<br>
    </td>
  </tr>

  <!-- 1.4.1 -->
  <tr>
    <td style="vertical-align:top">
      1.4.1<br>
      Platform Release 0.9
    </td>
    <td style="vertical-align:top">
      Date: 2022-11-11
    </td>
    <td style="vertical-align:top">
     Ubuntu:<br>
    - add `i2c-tools`<br>
    - Journalctl system max use limit now set to 1000M<br>
    <br>
    Camera:<br>
    - Add A65 TOF Module (irs10x0c sensor) support on all 6 camera interfaces<br>
    - ov7251 drivers are now 8-bit<br>
    <br>
    XBL (secondary bootloader):<br>
    - Fix occasional error showing "battery's capacity is very low" during flashing<br>
    <br>
    NHLOS:<br>
    - ModalAI SLPI upgraded to 1.1-2<br>
    <br>
    Flashing Script:<br>
    - Made system image flash script compatible with bash 4 and older versions<br>
    </td>
  </tr>

  <!-- 1.3.1 -->
  <tr>
    <td style="vertical-align:top">
      1.3.1<br>
      (internal)
    </td>
    <td style="vertical-align:top">
      Date: 2022-08-11
    </td>
    <td style="vertical-align:top">    
      Meta:<br>
      - Included missing fastrpc binaries for adsp and cdsp in dspso partition<br>
      <br>
      Camera:<br>
      - Update IMX214 drivers to support other resolutions other than 640x480<br>
      - Added ALPHA LEVEL support for 5 concurrent ov9782 (needs tuning)<br>
      <br>
      BSP<br>
      - M0054/M0052 - enable 2W UART on J3 pins 3/5 (legacy B2B) for apps proc usage<br>
      - M0052 - breaking change: change qup5 from SPI to UART.  This is exposed on J8.  Now /dev/spidev5.0 is gone.<br>
      - M0054 Mappings are as follows:<br>
        - `/dev/ttyHS0`  - J8 Camera Connector<br>
        - `/dev/ttyHS1`  - J3 B2B pins 3/5<br>
        - `/dev/ttyHS2`  - J5 HS B2B pins 48/49<br>
      - M0052 Mappings are as follows:<br>
        - `/dev/ttyHS0`  - internal SOM WiFi<br>
        - `/dev/ttyHS1`  - J2 RC input for PX4<br>
        - `/dev/ttyHS2`  - J19 GNSS input for PX4<br>
        - `/dev/ttyHS3`  - J8 Camera Connector<br>
        - `/dev/ttyHS4`  - J3 B2B pins 3/5<br>
        - `/dev/ttyHS5`  - J5 HS B2B pins 48/49<br>
      <br>
      Trustzone:<br>
      - M0054, version 6: update devcfg for UART on qup7 (md5sum: 3698389194c899953c4e337a7b48cb97)<br>
      - M0052, version 6: update devcfg for UART on qup7 (md5sum: cf613de37db6e7d2bb245f4d17cab79e)<br>
    </td>
  </tr>

  <!-- 1.2.3 -->
  <tr>
    <td style="vertical-align:top">
      1.2.3<br>
      (internal)
    </td>
    <td style="vertical-align:top">
      NA
    </td>
    <td style="vertical-align:top">    
      Ubuntu:<br>
      - added jq pciutils aircrack-ng packages<br>
      <br>
      Kernel:<br>
      - add dormant 88XXau_wfb.ko driver at /etc/  Users who choose to use it can install it manually and use voxl-wifi-broadcast <br>
      - regulatory domain tweaks to support wifibroadcast options<br>
      - Update spidev buffer size to 16K<br>
    </td>
  </tr>

  <!-- 1.2.2 -->
  <tr>
    <td style="vertical-align:top">
      1.2.2<br>
      (internal)
    </td>
    <td style="vertical-align:top">
      NA
    </td>
    <td style="vertical-align:top">    
      Changes:<br>
      - System:<br>
        - Bash environment executed during ssh session start<br>
        - ModalAI partitions now include MACHINE type prefix<br>
        - ModalAI SLPI 1.1-0 now flashed in system image via DSP and Firmware partitions<br>
      - Kernel<br>
        - fix for hal3 timestamp drift error<br>
        - enable LAN95XX driver<br>
      - chi-cdk<br>
        - ov9782: add front/rear and tracking support<br>
        - ov7251: fix for missing gain in hal3 metadata<br>
    </td>
  </tr>

  <!-- 1.2.1 -->
  <tr>
    <td style="vertical-align:top">
      1.2.1<br>
      (internal)
    </td>
    <td style="vertical-align:top">
      Date: 2022-04-18
    </td>
    <td style="vertical-align:top">    
      Changes:<br>
      - ROS sources and key included in root filesystem<br>
      - XBL now flashed during system image install<br>
      - Binaries can now be executed in the data partition<br>
    </td>
  </tr>

  <!-- 1.2.0 -->
  <tr>
    <td style="vertical-align:top">
      1.2.0<br>
    </td>
    <td style="vertical-align:top">
      Date: 2022-04-15
    </td>
    <td style="vertical-align:top">    
      Changes:<br>
      - Update chi-cdk - ov7251 FSYNC registers from mm-camera<br>
      - Add rndis host driver to kernel<br>
      - Enable SPI<br>
        - /dev/spidev3.0 - internal IMU<br>
        - /dev/spidev14.0 - external SPI<br>
        - /dev/spidev0.0, /dev/spidev1.0, and /dev/spidev5.0 - camera groups 0, 1 and 2<br>
      - Updated device’s HLOS partitions<br>
        - Updated userdata ( /data ) partition to 64GB<br>
        - Added 64KB modalai_conf ( /etc/modalai ) partition<br>
        - Added 64KB modalai_cal ( /data/modalai ) partition<br>
        - Reduced system ( / ) partition to 47.875GB<br>
      - System image flash<br>
        - devcfg.mbn is now flashed during system image flash<br>
        - NHLOS is now flashed during system image flash<br>
        - LUN0 GPT can be flashed during system image flash<br>
      - System image root filesystem additions<br>
        - Px4-support and libfc now included in rootfs<br>
        - ModalAI metabuild info now in /firmware/verinfo/ver_info.txt<br>
        - Remove /etc/voxl-platform-info.json meta file creation, ModalAI metabuild info includes this information now<br>
        - Removed old incompatible tdk binaries<br>
        - Added Stable ModalAI sources list, library search paths, and qrb5165-bind to root filesystem<br>
        - Included docker sources list and key<br>
    </td>
  </tr>

  <!-- 1.1.4 -->
  <tr>
    <td style="vertical-align:top">
      1.1.4<br>
      (internal)
    </td>
    <td style="vertical-align:top">
      Date: 2022-03-09
    </td>
    <td style="vertical-align:top">    
      Changes:<br>
      - add rtl8188eus driver for tp-link support<br>
      - remove all opencv 3.2 packages<br>
      - apt autoremove and clean removing 400MB! of cached debs<br>
      - permit root ssh login<br>
      - add platform_name file for bash formatting<br>
      - remove ldconfig crying about wayland sink not a symlink<br>
    </td>
  </tr>

  <!-- 1.1.3 -->
  <tr>
    <td style="vertical-align:top">
      1.1.3<br>
      (internal)
    </td>
    <td style="vertical-align:top">
      Date: 2022-02-18
    </td>
    <td style="vertical-align:top">    
      Changes:<br>
      - Modified mv-voxl 32-bit shared object files permissions as they were set incorrectly<br>
      - Added ```/etc/voxl-platform-info.json``` file which includes build and platform metadata<br>
    </td>
  </tr>

   <!-- 1.1.2 -->
  <tr>
    <td style="vertical-align:top">
      1.1.2<br>
    </td>
    <td style="vertical-align:top">
      Date: 2022-02-09
    </td>
    <td style="vertical-align:top">    
      Changes:<br>
      - Initial Release<br>
    </td>
  </tr>

</table>

## Force VOXL 2 into Fastboot

<b>Overview</b>

This procedure is for use if the normal update procedure above is not working.

<b>Video</b>

{% include youtubePlayer.html id=page.youtubeId1 %}

<b>Procedure</b>

- Unplug VOXL 2 from power
- Unplug VOXL 2 from USBC
- Using something soft like a BBQ skewer or toothpick, press and hold the momentary button `SW1` down, as shown in this image:

<img src="/images/voxl2/m0054-fastboot.jpg" alt="m0054-fastboot" width="640"/>

- While holding `SW1` down, power on VOXL 2
- Keep holding `SW1` down for about 5 seconds and then let go
- Attach VOXL 2 to USBC connected to host computer
- Run the `fastboot devices` command and verify the device is showing up

```bash
❯ fastboot devices
f8bb8d44	fastboot
```

- Now, you should be able to proceed with the `./install.sh` command from the Platform Release zip file.

## Missing ADB?

See [here](/setting-up-adb/)
