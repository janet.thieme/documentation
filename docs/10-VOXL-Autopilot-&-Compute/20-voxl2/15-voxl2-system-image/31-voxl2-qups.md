---
layout: default
title: VOXL 2 QUPs
parent: VOXL 2 System Image
nav_order: 31
permalink: /voxl2-qups/
---

# VOXL 2 QUPs
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

Together, the Qualcomm Universal Peripheral v3 and the TrustZone provide access to various protocols on various hardware interfaces.  Trustzone is a necessary component that requires updates to in order to change protocol/interfaces.

Trusztone images are flashed during the system image installation (`devcfg.mbn`).  The toolchain and source code to build the Trustzone image are unfortunately not open source.

## devcfg

NOTE: changing TZ configuration can brick the hardware.

### VOXL 2 Mini - M0104

Note: this is available in System Image 1.7+ for M0104 and will ship in SDK 1.1 and is available as preview here.  To use with SDK 1.0, please contact devops @ modalai .com .

- [v0104.0, VOXL 2 Mini](https://storage.googleapis.com/modalai_public/865-tz-public/M0104/v0104.0-2023-07-27.tar.gz)(md5sum: ccb6dedc4731aec6d80b4ef6db52fbd6)
- convert SE14 from SPI to UART for VOXL 2 Mini

| QUP            | Protocol | QRB5165 GPIO | Connector/Routing | Device            | Notes                                                |
|----------------|----------|--------------|-------------------|-------------------|------------------------------------------------------|
| QUP0  0x980000 | SPI      | 28-31        | J6 - 34,36,38,40  | `/dev/spidev0.0`  | sys image 1.7+, Camera Group 0 SPI                   |
| QUP1  0x984000 | I2C      | 4-5          | J7 - 34,36        | `/dev/i2c-0`      | sys image 1.7+ Camera Group 1 I2C                    |
| QUP2  0x988000 | I2C      | 115-116      |                   |                   | No J3 (B2B connector)                                |
| QUP3  0x98c000 | SPI      | 119-122      | U7,               | `/dev/spidev3.0`  | sys image 1.7+, Onboard IMU1, 42688p                 |
| QUP4  0x990000 |          |              |                   |                   |                                                      |
| QUP5  0x994000 | UART     | 12-15        |                   |                   | No J8 (Camera Group 2)                               |
| QUP6  0x998000 | HS UART  |              |                   |                   |                                                      |
| QUP7  0x99c000 | HS UART  | 22-23        |                   |                   | No J3 (B2B connector)                                |
| QUP8  0xa80000 | NA       |              |                   |                   |                                                      |
| QUP9  0xa84000 | I2C      | 125-126      |                   |                   | No J3 (B2B connector)                                |
| QUP10 0xa88000 | I2C      | 129-130      |                   |                   | No J3 (B2B connector)                                |
| QUP11 0xa8c000 | SPI      | 60-63        |                   |                   | No J5 (B2B connector)                                |
| QUP12 0xa90000 | debug    | 34-35        |                   | Not in perf build | sys image 1.7+, Debug Console (2W UART, 115200 baud) |
| QUP13 0xa94000 | UART     |              |                   |                   | No J5 (B2B connector)                                |
| QUP14 0x880000 | SPI      | 40-43        | J10, apps proc    | `/dev/ttyHS0`     | sys image 1.3+, External SPI J10                     |
| QUP15 0x884000 | I2C      | 44-45        |                   |                   |                                                      |
| QUP16 0x888000 | NA       |              |                   |                   |                                                      |
| QUP17 0x88c000 | NA       |              |                   |                   |                                                      |
| QUP18 0x890000 | NA       |              |                   |                   |                                                      |
| QUP19 0x894000 | HS UART  | 2-3          |                   |                   |  No J5 (B2B connector)                               |

### VOXL 2 - M0054

#### Current Version (0008)

Note: this version works with M0104 SDK 1.0

- [M0054 v0008, VOXL 2](https://storage.googleapis.com/modalai_public/865-tz-public/M0054/v0008.tar.gz)(md5sum: ccb6dedc4731aec6d80b4ef6db52fbd6)
- used in VOXL2 system image 1.5+
  - convert qup1 from SPI to I2C to expose apps_proc I2C on camera group

| QUP            | Protocol | QRB5165 GPIO | Connector/Routing | Device            | Notes                                                |
|----------------|----------|--------------|-------------------|-------------------|------------------------------------------------------|
| QUP0  0x980000 | SPI      | 28-31        | J6 - 34,36,38,40  | `/dev/spidev0.0`  | sys image 1.3+, Camera Group 0 SPI                   |
| QUP1  0x984000 | I2C      | 4-5          | J7 - 34,36        | `/dev/i2c-4`      | sys image 1.5+ Camera Group 1 I2C                    |
| QUP2  0x988000 | I2C      | 115-116      | J5 - 8,9          | `/dev/i2c-0`      | HS B2B I2C2                                          |
| QUP3  0x98c000 | SPI      | 119-122      | U7,               | `/dev/spidev3.0`  | sys image 1.3+, Onboard IMU1, 42688p                 |
| QUP4  0x990000 | I2C      | 8-9          | J5 - 98,99        | `/dev/i2c-5`      | sys image 1.7+, HS B2B I2C                           |
| QUP5  0x994000 | UART     | 12-15        | J8 - 34,36,38,40  | `/dev/ttyHS0`     | sys image 1.4+,  Camera Group 2 UART                 |
| QUP6  0x998000 | HS UART  |              |                   |                   |                                                      |
| QUP7  0x99c000 | HS UART  | 22-23        | J3 - 3,5          | `/dev/ttyHS1`     | sys image 1.3+, B2B 2W UART                          |
| QUP8  0xa80000 | NA       |              |                   |                   |                                                      |
| QUP9  0xa84000 | I2C      | 125-126      | J3 - 13,15        | `/dev/i2c-1`      | sys image 1.3+, B2B I2C9                             |
| QUP10 0xa88000 | I2C      | 129-130      | J3 - 23,25        | `/dev/i2c-2`      | sys image 1.3+, B2B I2C10                            |
| QUP11 0xa8c000 | SPI      | 60-63        | J5 - 53,56        | `/dev/spidev11.0` | sys image 1.5+,  HS B2B SPI                          |
| QUP12 0xa90000 | debug    | 34-35        | J3 - 27,29        | Not in perf build | sys image 1.3+, Debug Console (2W UART, 115200 baud) |
| QUP13 0xa94000 | UART     | 38-39        | J5 - 97-98        | `/dev/ttyHS3`     | sys image 1.5+, HS B2B UART                          |
| QUP14 0x880000 | SPI      | 40-43        | J10, apps proc,   | `/dev/spidev14.0` | sys image 1.3+, External SPI J10                     |
| QUP15 0x884000 | I2C      | 44-45        |                   | `/dev/i2c-3`      | (internal use)                                       |
| QUP16 0x888000 | NA       |              |                   |                   |                                                      |
| QUP17 0x88c000 | NA       |              |                   |                   |                                                      |
| QUP18 0x890000 | NA       |              |                   |                   |                                                      |
| QUP19 0x894000 | HS UART  | 2-3          | J5 - 48,49        | `/dev/ttyHS2`     | sys image 1.3+, HS B2B 2W UART                       |


#### Version 0007

- M0054 VOXL2 - md5sum: 77366973052c03ce3eab57c1e92b490a
- used in system image 1.5+
  - enable qup2:  I2C,  for HS B2B J5 pins 8/9, support for M0130 add-on I2C on J8
  - enable qup11: SPI,  for HS B2B J5 pins 53/56, support for M0130 add-on SPI on J8
  - enable qup13: UART, for HS B2B J5 pins 36-39


#### Version 0006

- version 6, M0054-VOXL2 - md5sum: 3698389194c899953c4e337a7b48cb97
- used in system image 1.3+
  - enable qup7: 2W UART, for B2B J3 pins 3/5, support for M0125 add-on UART


### RB5 Flight M0052

- M0052-RB5-FLIGHT md5sum: cf613de37db6e7d2bb245f4d17cab79e

Deltas from M0054 version 6 above:

| QUP            | Protocol  | QRB5165 GPIO   | Notes                                | Connector/Routing    | Device |
|---             |---        |---             |---                                   |---                   |---     |
| QUP5  0x994000 | UART      | 12-15          | Camera Group 2 UART                  | J8 - 34,36,38,40     | `/dev/ttyHS3`  |
| QUP6  0x998000 | HS UART   |                | SOM WiFi                             | internal             | `/dev/ttyHS0`  |
| QUP7  0x99c000 | HS UART   | 22-23          | B2B 2W UART                          | J3 - 3,5             | `/dev/ttyHS4`  |
| QUP13 0xa94000 | UART      | 38-39          | PX4 RC input                         | apps_proc, M0052-J12 | `/dev/ttyHS1`  |
| QUP18 0x890000 | UART      | 58-59          | PX4 GNSS                             | apps_proc, M0052-J10 | `/dev/ttyHS2`  |
| QUP19 0x894000 | HS UART   | 2-3            | HS B2B 2W UART                       | J5 - 48,49           | `/dev/ttyHS5`  |