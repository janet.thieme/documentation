---
layout: default
title: VOXL 2 RC Configs
parent: VOXL 2 User Guides
nav_order: 2
permalink:  /voxl2-rc-configs/
---

# VOXL 2 Remote Controller Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

We are working to expand the capabilities of VOXL 2 every day. The built-in PX4 flight controller enables industry leading SWAP for an autonomous flight controller, but not every interface and flight controller are supported yet. This page provides an overview of available connectivity. If this connectivity is insufficient for your application, VOXL 2 is a world-class companion computer for autonomous navigation and AI when paired with an [external flight controller](/voxl2-external-flight-controller/).

## Remote Controller Options for Built-in Flight Controller

| R/C               | Protocol     | Includes Telemetry | Instructions                                                                                       | Adapter                                          | Example Hardware |
|-------------------|--------------|--------------------|----------------------------------------------------------------------------------------------------|--------------------------------------------------|------------------|
| Ghost Atto        | UART / GHST  | No                 |                                                                                                    | Not required                                     | Ghost Atto       |
| Ghost Atto        | SBUS         | No                 | See Ghost Atto SBUS Protocol in this document                                                      | [VOXL 2 I/O](https://docs.modalai.com/voxl2-io/) | Ghost Atto       |
| ELRS              | UART         | Yes                | MAVLink and CRSF raw                                                                               |                                                  |                  |
| Spektrum (DSMX)   | UART         | No                 | [Here](/sentinel-user-guide-pre-flight-setup/#rc-radio-setup)                                      | M0094                                            | SPM9745          |
| TBS Crossfire     | UART         | Yes                | MAVLink and CRSF raw                                                                               |                                                  | ES900RX          |
| Graupner          | SBUS / UART  | No                 | [Here](https://docs.modalai.com/voxl2-io-user-guide/#using-sbus-graupner-gr-16/)                  | [VOXL 2 I/O](https://docs.modalai.com/voxl2-io/) | Graupner GR-16   |
| FrSky             |              | No                 | Not yet supported                                                                                  |                                                  |                  |
| Futaba            |              | No                 | Not yet supported                                                                                  |                                                  |                  |
| WiFi              | IP / Mavlink | No                 | [VOXL 2 Wifi Dongle User Guide](/voxl2-wifidongle-user-guide/) [Connect QGC over WiFi](/qgc-wifi/)|                                                  |                  |
| Microhard         | IP / Mavlink | No                 | [Microhard Add-on Manual](/microhard-add-on-manual/)                                               |                                                  |                  |
| Doodle Labs       | IP / Mavlink | No                 | [Doodle Labs User Guide](/doodle-labs-user-guide/)                                                 |                                                  |                  |
| Cellular          | IP / Mavlink | No                 | [4G](/lte-v2-modem-user-guide/) or [5G](/5G-Modem-user-guide) with [voxl-vpn](/voxl-vpn/)          |                                                  |                  |


## Spektrum (DSMX)

### Hardware

Tested hardware:
- Spektrum SPM9X45

NOTE: recomendded to have binding method outside of VOXL 2.

<img src="/images/voxl2/m0054-user-guide-rc-dsmx.png" alt="m0054-user-guide-rc-dsmx.png" width="640"/>

M0054 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S

### Software

#### Checking Status

The `px4-listener input_rc` command can be used to check status.

## CRSF

### Hardware

**PLEASE NOTE**: the baud rates for the receivers likely need to be updated.

The baud rates for the receiver UART are set here when PX4 starts on VOXL 2:
- MAVLink - https://github.com/modalai/px4-firmware/blob/voxl-dev-1.12/boards/modalai/rb5-flight/voxl-px4.config#L172
- CRSF - https://github.com/modalai/px4-firmware/blob/voxl-dev-1.12/boards/modalai/rb5-flight/voxl-px4.config#L175

Tested hardware:
- Happymodel ES900EX (in CRSF mode)
- TBS Crossfire Nano Rx (in MAVLink mode)

<img src="/images/voxl2/m0054-user-guide-rc-crsf.png" alt="m0054-user-guide-rc-crsf.png" width="640"/>

M0054 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S

### Software

#### Checking Status

The `px4-listener input_rc` or `px4-listener rc_channels` command can be used to trouble shoot, if neither have been published it's likely the receiver has yet to get RC data or the serial link isn't working.

## Ghost Atto RC 

### GHST Protocol

### SBUS Protocol

With the VOXL 2 wired up to the connect the [VOXL 2 IO](/voxl2-io/) board with the Ghost Atto receiver attached to port J3:

1. Power on the system, the Atto should have a solid blue LED indicating paring mode.
2. Power on the RC controller with the Ghost TX attached.
3. On the Ghost Module TX, click the nozzle in.
4. on the main menu, Navigate to `Mode...` and click in the nozzle.
5. Change the following:
    - `TX Mode` from `Auto` to `Manual`
    - `Protocol` from `GHST` to`SBus`
6. Move the nozzle to the left.
7. Navigate to `Bind...` then press in the nozzle.
8. Navigate to `Bind + Setup` and change `RX` to `ProtoSBus`.
9. Navigate to and Select `Start Bind` 

At this point the Atto should transition from a solid blue light to a red and yellow light indicating the binding was successful. If you go in to the Radio tab on QGC you should see the values for roll, pitch, and yaw. Change as you move the RC controller's Thumbsticks.

