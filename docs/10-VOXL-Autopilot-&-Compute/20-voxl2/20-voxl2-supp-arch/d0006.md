---
layout: default
title: VOXL 2 - D0006
parent: VOXL 2 Supported Architectures
nav_order: 1
permalink: /voxl2-d0006/
---

# VOXL 2 - D0006 Architecture (Sentinel)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# VOXL2 - D0006-C11-M13-T1

---

## Hardware

### Summary

The `D0006-C11-M13-T1` configuration is used in the base Sentinel reference drone model.

- `D0006` - Sentinel Family
- `C11` - Image Sensor Config 11, QTY2 pair of ov7251 stereo sensors, ov7251 tracking sensor, imx214 hires sensor
- `M13` - WiFi modem config
- `T1` - DSMX receiver config

<br>

### Components

The following core hardware is used for that configuration (note in this guide we are excluding the motor, battery, transmitter):

| Part Number        | Kit Contents                                                            |
|--------------------|-------------------------------------------------------------------------|
| MDK-M0054-1--1-C11 | M0054 - VOXL2                                                           |
|                    | M0084/M0025-2/M0014 - Hires (IMX214)/Tracking (ov7251) sensors and flex |
|                    | QTY2 M0076/M0074/M0010/M0015 - Stereo pair (ov7251) and flex            |
| MDK-M0041-1-B-01   | VOXL Power Module v3 and cable                                          |
| MDK-M0117-1-01     | VOXL ESC                                                                |
| MDK-M0094-2-01     | GPS / Mag / RC Module                                                   |
| MDK-M0078-2-02     | Wi-Fi Modem Add-On                                                      |

<br>

### HW Block Diagram

Below describes the compute HW block diagram for the `D0006-C11-M13-T1` configuration.

- view this drawing in [fullsize](https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1.jpg), view drawing [source](https://gitlab.com/voxl-public/support/drawings/-/blob/master/D0006/D0006-C11-M13-T1.drawio) (using [this tool](https://github.com/jgraph/drawio-desktop/releases))
- for more information on connecting the image sensors, see [video here](https://www.youtube.com/watch?v=J7iYorzm5rs) and or [image here](https://docs.modalai.com/voxl2-camera-configs/#c11---front-stereo-rear-stereo-hires-and-tracking)

<img src="https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1.jpg" alt="D0006-C11-M13-T1"/>

<br>

### HW Theory of Operation

**Power**

The VOXL Power Module (`MDK-M0041`) accepts 2S-6S batteries as input, and passes this straight through to the VOXL ESC (`MDK-M0117`) and also regulates the voltage down to 5VDC.

Output form the power module (`M0041-J1`), the 5V/6A rated regulator powers VOXL2 (`MDK-M0054`) and provides power monitoring information over I2C (to `voxl-px4` over DSP, not apps_proc) through the power connector (`M0054-J4`).

**Debug Connections**

Once powered, a user can connect to a linux based terminal though the USBC connector (`M0054-J9`) using Android Debug bridge (adb).

Additionally, once initially setup over adb, a user can interact with VOXL2 over a network (ssh, `voxl-portal`) using the Wi-Fi add-on kit (`MDK-M0078`).

**Image Sensors**

Two stereo pairs (ov7521, VGA, black and white, 30FPS) connect to VOXL2 (`M0054-J6` and `M0054-J8`) using interposers `M0076`, `M0074` and `M0010`.

A tracking sensor (ov7521, VGA, black and white, 30FPS) and hires (imx214, 4K, 30FPS) connect to VOXL2 `M0054-J7` using the `M0084` dual camera flex.

**Flight Controller**

The VOXL ESC (`MDK-M0117`) communicates over UART to `voxl-px4` between `M0117-J2` and `M0054-J18`.

The VOXL2 GPS/Mag/RC bind assembly (`MDK-M0094`) is a possible solution for GPS (UART) Mag (I2C) and RC (UART).  Both communicate to `voxl-px4` over a UART and I2C (exposed from DSP, not apps_proc)  This kit uses Ublox Neo-M8N/IST8310 from Holybro.

The RC is Spektrum DSMX (mainly for legacy reasons, as a bunch of past product has used it.  For this reason, we continued to use it.  The snapdragon processors can't make a UART RX into an TX at run time.  For this reason, we have to use a GPIO to handle binding the RC, hench the little `M0094` binding board you see in this assembly.)

---

<br>

## Software

The following VOXL2 Platform Releases have been validated with the hardware above

### VOXL2 SDK 1.1.3

#### Summary

|     |     |
|-------|-------|
| Release Date | 2023-Mar-30  |
| Download location | [developer.modalai.com](https://developer.modalai.com/), see `Protected Downloads > VOXL2 Platform Releases` > `VOXL2 SDK 1.1.3`. |
| File name| `voxl2_SDK_1.1.3.tar.gz`|
| System Image / VOXL SDK | 1.7.4 / 1.1.3  |
| PX4 | [VOXL PX4 1.14 based branch](https://github.com/modalai/px4-firmware/tree/voxl-dev) |


#### MPA Configuration Command

To setup the VOXL SDK to use the above hardware configuration, run the MPA command listed below.

|-------|-------|
| MPA command | `voxl-configure-mpa -p --family MRB-D0006-4-V1-C11` |


#### SW Block Diagrams

Below are two block diagrams covering 

|-------|-------|
| `voxl-camera-server` | <img src="https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1-SW-voxl-camera-server.jpg" alt="D0006-C11-M13-T1-SW-voxl-camera-server"/> <br> - view this drawing in [fullsize](https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1-SW-voxl-camera-server.jpg), view drawing [source](https://gitlab.com/voxl-public/support/drawings/-/blob/master/D0006/D0006-C11-M13-T1-SW.drawio) (using [this tool](https://github.com/jgraph/drawio-desktop/releases)) |
| `voxl-px4` | <img src="https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1-SW-voxl-px4.jpg" alt="D0006-C11-M13-T1-SW-voxl-px4"/> <br> - view this drawing in [fullsize](https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1-SW-voxl-px4.jpg), view drawing [source](https://gitlab.com/voxl-public/support/drawings/-/blob/master/D0006/D0006-C11-M13-T1-SW.drawio) (using [this tool](https://github.com/jgraph/drawio-desktop/releases)) |


<br>

#### SW Theory of Operation

**Summary**

VOXL2 runs Linux (Ubuntu 18).  A collection of user space applications run on bootup as systemd services to facilitate the various requirements of the architecture.  These applications ([open source](https://gitlab.com/voxl-public/voxl-sdk), mainly C/C++) communicate between each other using linux named pipes using the light weight [Modal Pipe Architecture](/mpa/).

To facilitate installation and setup, ModalAI ships [Platform Releases](/platform-releases/) (SDK) that allow a simple setup.

**SDK**

The VOXL2 SDK is a a zip file that contains an install script, the [VOXL2 System Image](/voxl2-system-image/) (kernel, root file system) and the [VOXL SDK](/voxl-sdk/), which is a collection of open source software packages.

The VOXL SDK comes with a `voxl-configure-sku` tool that can be used to configure the software to work out of the box on a given hardware architecture, such as this `D0006`.

**voxl-px4**

In SDK 1.1.3, `voxl-px4` is based off of PX4 1.14 and located in this [branch](https://github.com/modalai/px4-firmware/tree/voxl-dev).  More developer information is available in the [VOXL2 PX4 Developer Guide](/voxl-px4-developer-guide/).  More user information is available in the [VOXL2 PX4 User Guide](/voxl-px4-user-guide/).

All communications to ground control stations from `voxl-px4` are facilitated through `voxl-mavlink-server` and `voxl-vision-px4` over an IP link.  See [quick start video](https://www.youtube.com/watch?v=aVHBWbwp488).

Below are links to some of the drivers that are called out above.

|--------|-------|------|
| Use | Driver | Interface |
|---     |---       |---   |
| Power Monitoring |[src/drivers/power_monitor/voxlpm](https://github.com/modalai/px4-firmware/tree/voxl-dev-1.12/src/drivers/power_monitor/voxlpm) | `M0054-J4` |
| ESC | [src/drivers/uart_esc/modalai_esc](https://github.com/modalai/px4-firmware/tree/voxl-dev-1.12/src/drivers/uart_esc/modalai_esc) | `M0054-J18`|
| IMU | [src/drivers/imu/invensense/icm42688p](https://github.com/modalai/px4-firmware/tree/voxl-dev-1.12/src/drivers/imu/invensense/icm42688p) | internal IMU |
| BARO | [src/drivers/barometer/icp10100](https://github.com/modalai/px4-firmware/tree/voxl-dev-1.12/src/drivers/barometer/icp10100) | internal BARO |
| RC | [src/drivers/spektrum_rc](https://github.com/modalai/px4-firmware/tree/voxl-dev-1.12/src/drivers/spektrum_rc) | `M0054-J19 pins 10/11` |
| GPS | [src/drivers/gps](https://github.com/modalai/px4-firmware/tree/voxl-dev-1.12/src/drivers/gps) | `M0054-J19`, GNSS `pins 2/2`, Mag `pins 4/5` |

**Other Services**

Below are links and summaries of the other services that are expected to be running by default:

|--------|-------|------|
| Service | Summary |  Interface |
|---     |---       |--- |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server/-/tree/sdk-1.1) | HAL3 based application used to serve up MIPI camera frames to multiple MPA clients, such as `voxl-portal` | `M0054 J6/J7/J8`, MPA |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server/-/tree/sdk-1.1) | provides an MPA interface to perform MAVLink communication to/from a PX4 flight controller over UART or UDP | IP/UART/MPA |
| [voxl-vision-hub](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-hub/-/tree/sdk-1.1) | Acts as the main hub managing communication between VOXL MPA services and autopilots such as PX4 and Ardupilot | IP/MPA |
| [voxl-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-imu-server/-/tree/sdk-1.1) | For computer vision use cases, this application serves up IMU data to be fused with vision data (different IMU than PX4) | SPI/MPA |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal/-/tree/sdk-1.1) | embedded web server with ever expanding feature set  | MPA/HTTP/REST |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server/-/tree/sdk-1.1) | QVIO processing server  | MPA |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/tree/sdk-1.1) | Tool used to monitor CPU usage and account for Snapdragon scaling, etc. | MPA |
| [voxl-wait-for-fs](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils/-/blob/sdk-1.1/scripts/voxl-wait-for-fs) | Helper service to flush files to disk on cycle | NA |
