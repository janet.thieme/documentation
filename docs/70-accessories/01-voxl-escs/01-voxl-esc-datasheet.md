---
layout: default
title: VOXL 4-in-1 ESC Datasheet
parent: VOXL ESCs
nav_order: 1
permalink: /modal-esc-datasheet/
---

# VOXL 4-in-1 ESC Datasheet
{: .no_toc }

---

## Hardware Overview
![m0134-diagram.jpg](/images/modal-esc/m0134/m0134-diagram.jpg)

## Dimensions

[M0134 VOXL ESC 3D CAD](https://storage.googleapis.com/modalai_public/modal_drawings/M0134_ESC_4_IN_1_32_REVA.step)

![m0134-dimensions.png](/images/modal-esc/m0134/m0134-dimensions.png)

## Specifications

|                 | Details     |
|---              |---          |
| Power Input     | 2-4 S Li-Po (5.5-18V) |
|                 | XT30 connector (suggested) |
| Power Output    | Single AUX power output at 5.0V (adjustable via resistor) 500mA (can be used for Neopixel LEDs) |
| Performance     | 4 channels at 10A+ maximum continuous current per channel (depends on cooling) |
|                 | Maximum RPM : 50,000+RPM for a 12-pole motor |
| Features        | Open-loop control (set desired % power) |
|                 | Closed-loop RPM control (set desired RPM), used in PX4 driver |
|                 | Control LED colors of external LED Strip (Neopixel) |
|                 | Generate tones using motors |
|                 | Real-time RPM feedback |
| Communications  | Supported by VOXL Flight, VOXL and Flight Core |
|                 | Dual Bi-directional UART (3.3VDC logic-level) |
|                 | PWM input supporting 1-2ms and OneShot125 (more protocols coming soon) |
| Connectors      | UART: Hirose DF13 6-pin |
|                 | PWM Input: JST GH 6-pin |
|                 | AUX Regulated Output Connectors:  N/A (solder pads) |
|                 | Motor Output Connectors:  N/A (solder pads) |
| Hardware        | MCU : STM32F051K86 @ 48Mhz, 64KB Flash |
|                 | Mosfet Driver : MP6530 |
|                 | Mosfets	: AON7528 (N-channel) |
|                 | Individual Current Sensing : 4x 2mOhm + INA186 |
|                 | ESD Protection : Yes (on UART and PWM I/O)  |
|                 | Temperature Sensing : Yes |
|                 | On-board Status LEDs : Yes |
|                 | Weight (no wires) : 9.5g |
|                 | Motor Connectors: N/A (solder pads) |
| PX4 Integration | Scheduled to be supported in PX4 1.12 |
|                 | Available [here in developer preview](https://github.com/modalai/px4-firmware/tree/modalai-esc) |
| Resources       | [Manual](/modal-esc-v2-manual/) |
|                 | [PX4 Integration User Guide](/modal-esc-px4-user-guide/) |
