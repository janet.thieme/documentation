---
layout: default
title: M0153 MIPI Boson Image Sensor Adapter
parent: Image Sensor Flex Cables and Adapters
nav_order: 135
has_children: false
nav_exclude: true
search_exclude: true
permalink: /m0153/
---

# M0153 MIPI Boson Image Sensor Adapter
{: .no_toc }


## Important Notes

### BETA RELEASE

This is a beta release and not supported for production use cases.

### Boson+ USB Setup Requirement

The Boson+ must first be configured over USB using the `FLIR Boson+ Application v4.2`.

You need to purchase the Boson+ and USB backback seperately.

### VOXL SDK Requirements

VOXL SDK 1.2.0+ is required.  Only 30 FPS is supported (not 60FPS) in this release.

### Hardware Requirements

- MDK-M0153-1-055 - MIPI Boson+ Add On Development Kit, 55mm coax
- MDK-M0153-2 - DevKit, Boson USB2.0 Adapter (for one time in field setup of BOSON+ sensor over USB)
- Boson sensor supported on VOXL2/VOXL2 Mini J7

## Boson Software Setup

This procedure is done independent of VOXL2 and uses `MDK-M0153-2`

![MDK-M0153-1-055](/docs/70-accessories/12-image-sensor-flexes-adapters/images/MDK-M0153-2.jpg)

#### Connect

- Setup a Windows OS with `FLIR Boson+ Application v4.2` (see https://www.flir.com/oem/boson-family/)
- Connect Boson+ using MDK-M0153-2 USB backpack to Windows PC using a USB connection
- Select the Boson sensor from the "Port" drop down list (bottom right of GUI)
  - when selected, the "Boson Link" should show "Connected"

#### Reset to Factory

- Click on the "System" option in the left side navigation bar
  - Click on the "Configuration Controls" option
    - Click the "Restore Factory Defaults" button

#### Confgure MIPI

- Click on the "Image Apperance" option in the left side navigation bar
  - Click on "USB Video Controls"
    - Ensure "USB Video Enabled" is DISABLED
  - Click on "Analog/CMOS Video Controls"
    - Set "Video Source" to "Post-AGC"
    - Disable telemetry (both "CMOS Enabled" and "MIPI Embedded")
    - Set "Video" to "MIPI"

#### Confgure 30 FPS

- Click on the "System" option in the left side navigation bar
  - Click on the "Sync & Frame Rate" option
    - Set "Frame Skip" to 1, to enable 30 FPS

#### Save Power On Defaults

- Click on the "System" option in the left side navigation bar
  - Click on the "Configuration Controls" option
    - Click the "Save Power-On Defaults" button
- Power off
- Remove `MDK-M0153-2` USB backpack from BOSON.

## VOXL2 Hardware Setup

The `MDK-M0153-1-055`, MIPI Boson+ Add On Development Kit comes with the following.

- MCCA-M0153-1 - MIPI Coax to Boson W I2C GIMBAL PASS-THROUGH - Connects to Boson+
- MCCA-M0162-1 - VOXL2 To BOSON COAX ADAPTER (Code: V2(L)-H26-5L) - Connects to VOXL2/VOXL2 Mini J7
- MCBL-00084-055 - High-Speed Camera Coax Cable, 26-pin, 55mm - Connects between M0153/M0162

![MDK-M0153-1-055](/docs/70-accessories/12-image-sensor-flexes-adapters/images/MDK-M0153-1-055.jpg)

![MDK-M0153-1-055-J7](/docs/70-accessories/12-image-sensor-flexes-adapters/images/MDK-M0153-1-055-J7.jpg)

![MDK-M0153-1-055-J7-Boson](/docs/70-accessories/12-image-sensor-flexes-adapters/images/MDK-M0153-1-055-J7-Boson.jpg)

## VOXL2 Software Setup

This is currently a manual/hacky process:

Use tracking config to get things going:

```
voxl-configure-cameras 2
```

Remove ov7251 binaries, use boson_2:

```
rm /usr/lib/camera/com.qti.sensormodule.ov7251_2.bin
cp /usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_2.bin /usr/lib/camera/
```

Edit `/etc/modalai/voxl-camera-server.conf` file, check the `preview_height` = 512


```
{
	"version":	0.1,
	"fsync_en":	false,
	"fsync_gpio":	111,
	"cameras":	[{
			"type":	"ov7251",
			"name":	"tracking",
			"enabled":	true,
			"camera_id":	0,
			"fps":	30,
			"en_rotate":	false,
			"en_preview":	true,
			"preview_width":	640,
			"preview_height":	512,
			"en_raw_preview":	true,
			"ae_mode":	"lme_msv",
			"ae_desired_msv":	60,
			"exposure_min_us":	20,
			"exposure_max_us":	33000,
			"gain_min":	54,
			"gain_max":	8000,
			"exposure_soft_min_us":	5000,
			"ae_filter_alpha":	0.600000023841858,
			"ae_ignore_fraction":	0.20000000298023224,
			"ae_slope":	0.05000000074505806,
			"ae_exposure_period":	1,
			"ae_gain_period":	1
		}]
}
```
