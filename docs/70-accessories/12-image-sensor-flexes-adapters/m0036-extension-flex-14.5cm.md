---
layout: default
title: M0036 Extension Flex 15cm
parent: Image Sensor Flex Cables and Adapters
nav_order: 36
has_children: false
permalink: /m0036/
---

# M0036 Extension Flex 15cm
{: .no_toc }

## Specification

| Specification      | Value |
|---------------------|-------|
| Length              |    150mm edge-to-edge for an ~145mm extension   |
| VOXL-side Connector |    J1, Panasonic 36-pin AXT436124   |
| Mating Connector    |    J2, Panasonic 36-pin AXT336124   |


### Pin Out VOXL-side, J1

|	Pin	|	Net	|	Notes/Usage	|
|	-----	|	-------	|	-------	|
|	1	|	DGND	|		|
|	2	|	DGND	|		|
|	3	|	AFVDD	|	Normally Unused, 2.8V	|
|	4	|	CAM0_STBY_N	|	Normally Unused, power down signal	|
|	5	|	CCI_I2C_SDA0	|	CCI I2C Bus, SDA	|
|	6	|	1P8_VDDIO	|	1.8V VDDIO (VREG_S4A or switched VREG_S4A)	|
|	7	|	CCI_I2C_SCL0	|	CCI I2C Bus, SCL	|
|	8	|	VREG_DVDD	|	1.1V or 1.2V DVDD	|
|	9	|	CAM_RST0_N	|	Sensor Reset Control, 0	|
|	10	|	CAM_MCLKx_BUFF	|	MCLK, Buffered from Voxl, 1.8V	|
|	11	|	DGND	|		|
|	12	|	DGND	|		|
|	13	|	MIPI_CSI_CLK_CONN_P	|	MIPI CSI High Speed Diff Pair, CLK_P	|
|	14	|	CAM_FLASH	|	Sensor Sideband Control, Normally Unused	|
|	15	|	MIPI_CSI_CLK_CONN_M	|	MIPI CSI High Speed Diff Pair, CLK_M	|
|	16	|	CAM_SYNC_0	|	Sensor Sync Signal, Shorted to SYNC_1 if using M0010	|
|	17	|	MIPI_CSI_LANE0_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 0_P	|
|	18	|	CAMx_MCLK3	|	Stereo MCLK, Buffered from Voxl, 1.8V. Only on J3	|
|	19	|	MIPI_CSI_LANE0_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 0_M	|
|	20	|	VREG_AVDD_2P8	|	Sensor 2.8V AVDD	|
|	21	|	DGND	|		|
|	22	|	DGND	|		|
|	23	|	MIPI_CSI_LANE1_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 1_P	|
|	24	|	CAM1_RST1_N	|	Sensor Reset Control, 1 (Right)	|
|	25	|	MIPI_CSI_LANE1_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 1_M	|
|	26	|	CAM_SYNC_1	|	Sensor Sync Signal, Shorted to SYNC_1 if using M0010	|
|	27	|	MIPI_CSI_LANE2_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 2_P	|
|	28	|	CCI_I2C_SDA1	|	Second CCI I2C Bus, SDA	|
|	29	|	MIPI_CSI_LANE2_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 2_M	|
|	30	|	CCI_I2C_SCL1	|	Second CCI I2C Bus, SCL	|
|	31	|	DGND	|		|
|	32	|	DGND	|		|
|	33	|	MIPI_CSI_LANE3_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 3_P	|
|	34	|	VPH_PWR_3P8V	|	3.8V Primary "Phone" Power (mimics nominal 1S)	|
|	35	|	MIPI_CSI_LANE3_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 3_M	|
|	36	|	DGND	|		|



### Pin Out Mating-side, J2 (same as J1)

|	Pin	|	Net	|	Notes/Usage	|
|	-----	|	-------	|	-------	|
|	1	|	DGND	|		|
|	2	|	DGND	|		|
|	3	|	AFVDD	|	Normally Unused, 2.8V	|
|	4	|	CAM0_STBY_N	|	Normally Unused, power down signal	|
|	5	|	CCI_I2C_SDA0	|	CCI I2C Bus, SDA	|
|	6	|	1P8_VDDIO	|	1.8V VDDIO (VREG_S4A or switched VREG_S4A)	|
|	7	|	CCI_I2C_SCL0	|	CCI I2C Bus, SCL	|
|	8	|	VREG_DVDD	|	1.1V or 1.2V DVDD	|
|	9	|	CAM_RST0_N	|	Sensor Reset Control, 0	|
|	10	|	CAM_MCLKx_BUFF	|	MCLK, Buffered from Voxl, 1.8V	|
|	11	|	DGND	|		|
|	12	|	DGND	|		|
|	13	|	MIPI_CSI_CLK_CONN_P	|	MIPI CSI High Speed Diff Pair, CLK_P	|
|	14	|	CAM_FLASH	|	Sensor Sideband Control, Normally Unused	|
|	15	|	MIPI_CSI_CLK_CONN_M	|	MIPI CSI High Speed Diff Pair, CLK_M	|
|	16	|	CAM_SYNC_0	|	Sensor Sync Signal, Shorted to SYNC_1 if using M0010	|
|	17	|	MIPI_CSI_LANE0_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 0_P	|
|	18	|	CAMx_MCLK3	|	Stereo MCLK, Buffered from Voxl, 1.8V. Only on J3	|
|	19	|	MIPI_CSI_LANE0_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 0_M	|
|	20	|	VREG_AVDD_2P8	|	Sensor 2.8V AVDD	|
|	21	|	DGND	|		|
|	22	|	DGND	|		|
|	23	|	MIPI_CSI_LANE1_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 1_P	|
|	24	|	CAM1_RST1_N	|	Sensor Reset Control, 1 (Right)	|
|	25	|	MIPI_CSI_LANE1_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 1_M	|
|	26	|	CAM_SYNC_1	|	Sensor Sync Signal, Shorted to SYNC_1 if using M0010	|
|	27	|	MIPI_CSI_LANE2_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 2_P	|
|	28	|	CCI_I2C_SDA1	|	Second CCI I2C Bus, SDA	|
|	29	|	MIPI_CSI_LANE2_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 2_M	|
|	30	|	CCI_I2C_SCL1	|	Second CCI I2C Bus, SCL	|
|	31	|	DGND	|		|
|	32	|	DGND	|		|
|	33	|	MIPI_CSI_LANE3_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 3_P	|
|	34	|	VPH_PWR_3P8V	|	3.8V Primary "Phone" Power (mimics nominal 1S)	|
|	35	|	MIPI_CSI_LANE3_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 3_M	|
|	36	|	DGND	|		|


## Technical Drawings

### 3D STEP File

[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0036_145mm.stp)

### 2D Diagram

![MCBL-M0036](/images/other-products/flex-cables/m0036-2d.png)

![MCBL-M0036](/images/other-products/flex-cables/m0036-2d-2.png)

