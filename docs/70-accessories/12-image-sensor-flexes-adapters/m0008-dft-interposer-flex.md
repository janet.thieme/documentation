---
layout: default
title: M0008 DFT Interposer Flex
parent: Image Sensor Flex Cables and Adapters
nav_order: 8
has_children: false
permalink: /m0008/
---

# M0008 DFT Interposer Flex
{: .no_toc }

## Specification

| Specification      | Value |
|---------------------|-------|
| Length              |   75 mm end-to-end    |
| VOXL-side Connector |   J1, AXT436124    |
| Mating Connector    |   J2, 503772-2420    |


### Pin Out VOXL-side

|	Pin	|	Net	|	Notes/Usage	|
|	-----	|	-------	|	-------	|
|	1	|	DGND	|		|
|	2	|	DGND	|		|
|	3	|	NC	|	No Connect	|
|	4	|	NC	|	No Connect	|
|	5	|	CCI_I2C_SDA0	|	CCI I2C Bus, SDA	|
|	6	|	VREG_S4A_1P8	|	1.8V VDDIO	|
|	7	|	CCI_I2C_SCL0	|	CCI I2C Bus, SCL	|
|	8	|	NC	|	No Connect	|
|	9	|	CAM_RST0_N	|	Sensor Reset Control	|
|	10	|	CAM_MCLK_BUFF	|	MCLK, Buffered from Voxl, 1.8V	|
|	11	|	DGND	|		|
|	12	|	DGND	|		|
|	13	|	MIPI_CSI_CLK_CONN_P	|	MIPI CSI High Speed Diff Pair, CLK_P	|
|	14	|	CAM_FLASH	|	Sensor Sideband Control, Unused	|
|	15	|	MIPI_CSI_CLK_CONN_M	|	MIPI CSI High Speed Diff Pair, CLK_M	|
|	16	|	NC	|	No Connect	|
|	17	|	MIPI_CSI_LANE0_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 0_P	|
|	18	|	NC	|	No Connect	|
|	19	|	MIPI_CSI_LANE0_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 0_M	|
|	20	|	VREG_L23A_2P8	|	Sensor 2.8V AVDD	|
|	21	|	DGND	|		|
|	22	|	DGND	|		|
|	23	|	NC	|	No Connect	|
|	24	|	NC	|	No Connect	|
|	25	|	NC	|	No Connect	|
|	26	|	CAM_SYNC_1	|	Sensor Sync Signal, Normally Unused	|
|	27	|	NC	|	No Connect	|
|	28	|	NC	|	No Connect	|
|	29	|	NC	|	No Connect	|
|	30	|	NC	|	No Connect	|
|	31	|	DGND	|		|
|	32	|	DGND	|		|
|	33	|	NC	|	No Connect	|
|	34	|	NC	|	No Connect	|
|	35	|	NC	|	No Connect	|
|	36	|	DGND	|		|


### Pin Out Mating-side

|	Pin	|	Net	|	Notes/Usage	|
|	-----	|	-------	|	-------	|
|	1	|	DGND	|	Note Pin 1 of M0008 is Pin 24 of Sensor Module	|
|	2	|	DGND	|	Note Pin 2 of M0008 is Pin 1 of Sensor Module	|
|	3	|	VREG_L23A_2P8	|	Sensor 2.8V AVDD	|
|	4	|	CAM_MCLK_BUFF	|	MCLK, Buffered from Voxl, 1.8V	|
|	5	|	DGND	|		|
|	6	|	DGND	|		|
|	7	|	NC	|	No Connect	|
|	8	|	CAM_RST_N	|	Sensor Reset Control	|
|	9	|	VREG_S4A_1P8	|	1.8V VDDIO	|
|	10	|	CAM_FLASH	|	Sensor Sideband Control, Unused	|
|	11	|	DGND	|		|
|	12	|	DGND	|		|
|	13	|	CCI_I2C_SCL0	|	CCI I2C Bus, SCL	|
|	14	|	MIPI_CSI_LANE0_CONN_P	|	MIPI CSI High Speed Diff Pair, Data Lane 0_P	|
|	15	|	CCI_I2C_SDA0	|	CCI I2C Bus, SDA	|
|	16	|	MIPI_CSI_LANE0_CONN_M	|	MIPI CSI High Speed Diff Pair, Data Lane 0_M	|
|	17	|	DGND	|		|
|	18	|	DGND	|		|
|	19	|	NC	|	No Connect	|
|	20	|	MIPI_CSI_CLK_CONN_P	|	MIPI CSI High Speed Diff Pair, CLK_P	|
|	21	|	CAM_SYNC_EN	|	Sensor Sync Signal, Normally Unused	|
|	22	|	MIPI_CSI_CLK_CONN_M	|	MIPI CSI High Speed Diff Pair, CLK_M	|
|	23	|	ULPM	|	Sensor Ultra-Low-Power Mode Input, Held HIGH	|
|	24	|	DGND	|		|



## Technical Drawings



### 2D Diagram


![MCBL-M0008](/images/other-products/flex-cables/m0008-2d.png)

### ASSY Drawing 

This drawing shows pin 1' of each connector as related to the pin table above.
Note many older ModalAI image sensors have an alternate pin 1 notation which aligns with pin 2 on this flex. We expect to fix this confusion as we introduce a new 40-pin connector format this year (2024).


![MCBL-M0008-ASSY](/images/other-products/flex-cables/m0008-assy.jpg)