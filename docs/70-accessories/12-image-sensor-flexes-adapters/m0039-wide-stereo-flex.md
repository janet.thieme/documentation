---
layout: default
title: M0039 Wide Stereo Flex
parent: Image Sensor Flex Cables and Adapters
nav_order: 39
has_children: false
permalink: /m0039/
---

# M0010 Stereo Flex
{: .no_toc }

## Specification

| Specification       | Value                                                     |
|---------------------|-----------------------------------------------------------|
| Length              | 218mm baseline with QTY. M0015-1 stereo sensors           |
| VOXL-side Connector | J1, Panasonic 36-pin AXT436124                            |
| Mating Connector    | J2&J3, Molex 24-pin 5037722410                            |

[Buy Here](https://www.modalai.com/products/m0039)

### Pin Out VOXL-side, J1

| Pin | Net                    | Notes/Usage                                               |
|-----|------------------------|-----------------------------------------------------------|
| 1   | DGND                   |                                                           |
| 2   | DGND                   |                                                           |
| 3   | NC                     | No Connect                                                |
| 4   | NC                     | No Connect                                                |
| 5   | CCI_I2C_SDA0           | CCI I2C Bus, SDA                                          |
| 6   | VREG_S4A_1P8           | 1.8V VDDIO                                                |
| 7   | CCI_I2C_SCL0           | CCI I2C Bus, SCL                                          |
| 8   | NC                     | No Connect                                                |
| 9   | CAM1_RST0_N            | Sensor Reset Control, 0 (Left)                            |
| 10  | CAM_MCLK1_BUFF         | MCLK, Buffered from Voxl, 1.8V                            |
| 11  | DGND                   |                                                           |
| 12  | DGND                   |                                                           |
| 13  | MIPI_CSI1_CLK_CONN_P   | MIPI CSI High Speed Diff Pair, CLK_P                      |
| 14  | L_FLASH                | Left Sensor Sideband Control, Unused                      |
| 15  | MIPI_CSI1_CLK_CONN_M   | MIPI CSI High Speed Diff Pair, CLK_M                      |
| 16  | CAM_SYNC_0             | Left Sensor Sync Signal, Shorted to Right                 |
| 17  | MIPI_CSI1_LANE0_CONN_P | MIPI CSI High Speed Diff Pair, Data Lane 0_P              |
| 18  | CAM1_MCLK3             | Stereo MCLK, Buffered from Voxl, 1.8V                     |
| 19  | MIPI_CSI1_LANE0_CONN_M | MIPI CSI High Speed Diff Pair, Data Lane 0_M              |
| 20  | VREG_L23A_2P8          | Sensor 2.8V AVDD                                          |
| 21  | DGND                   |                                                           |
| 22  | DGND                   |                                                           |
| 23  | NC                     | No Connect                                                |
| 24  | CAM1_RST1_N            | Sensor Reset Control, 1 (Right)                           |
| 25  | NC                     | No Connect                                                |
| 26  | CAM_SYNC_1             | Right Sensor Sync Signal, Shorted to Left                 |
| 27  | MIPI_CSI1_LANE2_CONN_P | MIPI CSI High Speed Diff Pair, Stereo Right Data Lane 0_P |
| 28  | CCI_I2C_SDA1           | Second CCI I2C Bus, SDA                                   |
| 29  | MIPI_CSI1_LANE2_CONN_M | MIPI CSI High Speed Diff Pair, Stereo Right Data Lane 0_M |
| 30  | CCI_I2C_SCL1           | Second CCI I2C Bus, SCL                                   |
| 31  | DGND                   |                                                           |
| 32  | DGND                   |                                                           |
| 33  | MIPI_CSI1_LANE3_CONN_P | MIPI CSI High Speed Diff Pair, Stereo Right CLK_P         |
| 34  | NC                     | No Connect                                                |
| 35  | MIPI_CSI1_LANE3_CONN_M | MIPI CSI High Speed Diff Pair, Stereo Right CLK_M         |
| 36  | DGND                   |                                                           |



### Pin Out Mating-side, J2 (Left)

| Pin | Net                    | Notes/Usage                                    |
|-----|------------------------|------------------------------------------------|
| 1   | DGND                   | Note Pin 1 of M0010 is Pin 24 of Sensor Module |
| 2   | DGND                   | Note Pin 2 of M0010 is Pin 1 of Sensor Module  |
| 3   | VREG_L23A_2P8          | Sensor 2.8V AVDD                               |
| 4   | CAM_MCLK1_BUFF         | MCLK, Buffered from Voxl, 1.8V                 |
| 5   | DGND                   |                                                |
| 6   | DGND                   |                                                |
| 7   | NC                     | No Connect                                     |
| 8   | CAM1_RST0_N            | Sensor Reset Control, 0 (Left)                 |
| 9   | VREG_S4A_1P8           | 1.8V VDDIO                                     |
| 10  | L_FLASH                | Left Sensor Sideband Control, Unused           |
| 11  | DGND                   |                                                |
| 12  | DGND                   |                                                |
| 13  | CCI_I2C_SCL0           | CCI I2C Bus, SCL                               |
| 14  | MIPI_CSI1_LANE0_CONN_P | MIPI CSI High Speed Diff Pair, Data Lane 0_P   |
| 15  | CCI_I2C_SDA0           | CCI I2C Bus, SDA                               |
| 16  | MIPI_CSI1_LANE0_CONN_M | MIPI CSI High Speed Diff Pair, Data Lane 0_M   |
| 17  | DGND                   |                                                |
| 18  | DGND                   |                                                |
| 19  | NC                     | No Connect                                     |
| 20  | MIPI_CSI1_CLK_CONN_P   | MIPI CSI High Speed Diff Pair, CLK_P           |
| 21  | CAM_SYNC_0             | Left Sensor Sync Signal, Shorted to Right      |
| 22  | MIPI_CSI1_CLK_CONN_M   | MIPI CSI High Speed Diff Pair, CLK_M           |
| 23  | L_ULPM                 | Sensor Ultra-Low-Power Mode Input, Held HIGH   |
| 24  | DGND                   |                                                |


### Pin Out Mating-side, J3 (Right)

| Pin | Net                    | Notes/Usage                                               |
|-----|------------------------|-----------------------------------------------------------|
| 1   | DGND                   | Note Pin 1 of M0010 is Pin 24 of Sensor Module            |
| 2   | DGND                   | Note Pin 2 of M0010 is Pin 1 of Sensor Module             |
| 3   | VREG_L23A_2P8          | Sensor 2.8V AVDD                                          |
| 4   | CAM1_MCLK3             | Stereo MCLK, Buffered from Voxl, 1.8V                     |
| 5   | DGND                   |                                                           |
| 6   | DGND                   |                                                           |
| 7   | NC                     | No Connect                                                |
| 8   | CAM1_RST1_N            | Sensor Reset Control, 1 (Right)                           |
| 9   | VREG_S4A_1P8           | 1.8V VDDIO                                                |
| 10  | L_FLASH                | Left Sensor Sideband Control, Unused                      |
| 11  | DGND                   |                                                           |
| 12  | DGND                   |                                                           |
| 13  | CCI_I2C_SCL1           | Second CCI I2C Bus, SCL                                   |
| 14  | MIPI_CSI1_LANE2_CONN_P | MIPI CSI High Speed Diff Pair, Stereo Right Data Lane 0_P |
| 15  | CCI_I2C_SDA1           | Second CCI I2C Bus, SDA                                   |
| 16  | MIPI_CSI1_LANE2_CONN_M | MIPI CSI High Speed Diff Pair, Stereo Right Data Lane 0_M |
| 17  | DGND                   |                                                           |
| 18  | DGND                   |                                                           |
| 19  | NC                     | No Connect                                                |
| 20  | MIPI_CSI1_LANE3_CONN_P | MIPI CSI High Speed Diff Pair, Stereo Right CLK_P         |
| 21  | CAM_SYNC_1             | Right Sensor Sync Signal, Shorted to Left                 |
| 22  | MIPI_CSI1_LANE3_CONN_M | MIPI CSI High Speed Diff Pair, Stereo Right CLK_M         |
| 23  | R_ULPM                 | Sensor Ultra-Low-Power Mode Input, Held HIGH              |
| 24  | DGND                   |                                                           |


The following diagram shows the connectors on the [MSU-M0010-1-01](/m0010/) stereo flex cable. The M0039-1 has the same pinouts as the M0010-1.

![MSU-M0010-1-01-pinout.png](/images/other-products/image-sensors/MFPC-M0010A-pinout.png)

![Stereo_TFlex_Pin_Location.JPG](/images/other-products/image-sensors/Stereo_TFlex_Pin_Location.JPG)

![Stereo_TFlex_Complete.JPG](/images/other-products/image-sensors/Stereo_TFlex_Complete.JPG)

![Stereo_TFlex_Location.JPG](/images/other-products/image-sensors/Stereo_TFlex_Location.JPG)


## Technical Drawings

### 2D Diagram



