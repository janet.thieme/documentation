---
layout: default3
title: Image Sensor Flex Cables and Adapters
parent: Accessories
nav_order: 12
has_children: true
permalink: /image-sensor-flexes-adapters/
thumbnail: /other-products/image-sensors/flex.png
buylink: https://www.modalai.com/collections/accessories
summary: Documentation for ModalAI's range of flex cables and adapters that connect VOXL, RB5 Flight and VOXL 2 to various image sensor modules. 
---

# Image Sensor Flex Cables and Adapters
{: .no_toc }

Documentation for ModalAI's range of flex cables and adapters that connect VOXL, RB5 Flight and VOXL 2 to various image sensor modules. 

<a href="https://www.modalai.com/collections/accessories" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


{:toc}

## Extending MIPI Image Sensor Cable Lengths

It is strongly recommended to not connect multiple flex cables back-to-back to increase length beyond what ModalAI has shipped as a valid and supported configuration. The risk here includes:
* Incorrect connector orientation risk resulting in sensor or Voxl failures (including power to ground shorts)
* Adding mating cycles to connectors that have limited life span
* Reducing reliability due to increased interconnect points
* Increasing the length or creating a configuration beyond the data link limits ModalAI has already proven and supports 

If your application needs an extended length for your image sensor, please [contact ModalAI](https://modalai.com/contact) and we can explore a custom flex/cable hardware and software solution that will work for you.

## Image Sensor Flex Catalog

| MPN   | DESCRIPTION                                       | WEIGHT | 3D Model                                                                                                | BUY                                            | DATASHEET            |
|-------|---------------------------------------------------|--------|---------------------------------------------------------------------------------------------------------|------------------------------------------------|----------------------|
| M0008 | DFT Interposer Flex                               | 0.7g   |                                                                                                         |                                                | [Datasheet](/m0008/) |
| M0010 | Stereo Flex                                       | 0.9g   |                                                                                                         | [Buy](https://www.modalai.com/products/m0010/) | [Datasheet](/m0010/) |
| M0036 | 14.5cm Extension Flex                             |        |                                                                                                         | [Buy](https://www.modalai.com/products/m0036/) | [Datasheet](/m0036/) |
| M0074 | 6mc Extension Flex                                |        |                                                                                                         | [Buy](https://www.modalai.com/products/m0074/) | [Datasheet](/m0074/) |
| M0084 | VOXL 2 Y-Flex                                     |        |                                                                                                         | [Buy](https://www.modalai.com/products/m0084/) | [Datasheet](/m0084/) |
| M0109 | VOXL 2 J-Flex                                     |        |                                                                                                         |                                                | [Datasheet](/m0109/) |
| M0135 | VOXL 2 Dual-cam Adapter                           |        | [STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0135_simple.STEP)                  |                                                | [Datasheet](/m0135/) |
| M0173 | VOXL 2 micro-coax breakout (Starling 2 Front-end) | 3.55g  | [STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0173_STARLING_FRONT_END_REVA.step) |                                                |                      |
