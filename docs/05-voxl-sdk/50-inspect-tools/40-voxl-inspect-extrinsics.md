---
layout: default
title: VOXL Inspect Extrinsics
parent: Inspect Tools
search_exclude: true
nav_order: 40
permalink: /voxl-inspect-extrinsics/
---

# VOXL Inspect Extrinsics
{: .no_toc }

This tool reads the `/etc/modalai/extrinsics.conf` file and prints the data to the screen for debug. For more info about this extrinsics config file, see the [Configure Extrinsics page](/configure-extrinsics/).

---

## Arguments

```bash
yocto:/$ voxl-inspect-extrinsics --help

Tool to print extrinsic config as loaded from disk.
This will also add any new entries that have been added in an update.
Run in quiet mode to only validate the file contents and add new defaults.

See also: voxl-inspect-apriltag-config

-h, --help                  print this help message
-q, --quiet                 quiet mode

typical usage:
/# voxl-inspect-extrinsics

yocto:/$
```

## Use

```bash
yocto:/$ voxl-inspect-extrinsics
#0:
    parent:                imu1
    child:                 imu0
    T_child_wrt_parent:   -0.048   0.037   0.002
    RPY_parent_to_child:   0.0     0.0     0.0
    R_child_to_parent:     1.000  -0.000   0.000
                           0.000   1.000  -0.000
                           0.000   0.000   1.000
#1:
    parent:                imu0
    child:                 tracking
    T_child_wrt_parent:    0.065  -0.014   0.013
    RPY_parent_to_child:   0.0    45.0    90.0
    R_child_to_parent:     0.000  -0.707   0.707
                           1.000   0.000  -0.000
                          -0.000   0.707   0.707
#2:
    parent:                imu1
    child:                 tracking
    T_child_wrt_parent:    0.017   0.015   0.013
    RPY_parent_to_child:   0.0    45.0    90.0
    R_child_to_parent:     0.000  -0.707   0.707
                           1.000   0.000  -0.000
                          -0.000   0.707   0.707
#3:
    parent:                body
    child:                 imu0
    T_child_wrt_parent:    0.020   0.014  -0.008
    RPY_parent_to_child:   0.0     0.0     0.0
    R_child_to_parent:     1.000  -0.000   0.000
                           0.000   1.000  -0.000
                           0.000   0.000   1.000
#4:
    parent:                body
    child:                 imu1
    T_child_wrt_parent:    0.068  -0.015  -0.008
    RPY_parent_to_child:   0.0     0.0     0.0
    R_child_to_parent:     1.000  -0.000   0.000
                           0.000   1.000  -0.000
                           0.000   0.000   1.000
#5:
    parent:                body
    child:                 stereo_l
    T_child_wrt_parent:    0.100  -0.040   0.000
    RPY_parent_to_child:   0.0    90.0    90.0
    R_child_to_parent:     0.000  -0.000   1.000
                           1.000   0.000  -0.000
                          -0.000   1.000   0.000
#6:
    parent:                body
    child:                 tof
    T_child_wrt_parent:    0.100   0.000   0.000
    RPY_parent_to_child:   0.0    90.0    90.0
    R_child_to_parent:     0.000  -0.000   1.000
                           1.000   0.000  -0.000
                          -0.000   1.000   0.000
#7:
    parent:                body
    child:                 ground
    T_child_wrt_parent:    0.000   0.000   0.100
    RPY_parent_to_child:   0.0     0.0     0.0
    R_child_to_parent:     1.000  -0.000   0.000
                           0.000   1.000  -0.000
                           0.000   0.000   1.000
```

## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-extrinsics.c).

