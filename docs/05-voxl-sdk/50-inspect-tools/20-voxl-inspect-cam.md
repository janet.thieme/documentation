---
layout: default
title: VOXL Inspect Camera
parent: Inspect Tools
search_exclude: true
nav_order: 20
permalink: /voxl-inspect-cam/
---

# VOXL Inspect Cam
{: .no_toc }


The utility `voxl-inspect-cam` is a tool to check image metadata coming from MPA services that are publishing camera data. It requires that [VOXL Camera Server](/voxl-camera-server/) is running in the background, which can be checked with [VOXL Inspect Services](/voxl-inspect-services/). It can also be used to check non-camera images, such as the overlays coming out of [VOXL TFLite Server](/voxl-tflite-server/) or [VOXL QVIO Server](/voxl-qvio-server/).

---

## Arguments

### Required
<br>
Cam: Which image to display data from. Available images can be seen by typing ```voxl-inspect-cam {TAB} {TAB}```. Options that will regularly be available are:

#### Direct Cameras
- tracking
- stereo
- hires
- tof_depth

#### Other Images
- tof_conf
- tof_noise
- tof_ir
- dfs_disparity
- qvio_overlay
- tflite_overlay

### Optional

| Parameter    | Description                                                                      | Example                            |
|---           |---                                                                               |---                                 |
|-h --help     | Print help message                                                               | ```voxl-inspect-cam --help```      |
|-n --newline  | Print each sample on a new line instead of updating the current output line      | ```voxl-inspect-cam tracking -n``` |
|-t --test     | Test mode, simple pass/fail test after two seconds of waiting for a frame        | ```voxl-inspect-cam tracking -t``` |


## Example Output

```
yocto:/$ voxl-inspect-cam tracking

|size(bytes)| height | width  |exposure(ms)| gain | frame id |latency(ms)|Framerate(hz)| format
|   307200  |    480 |    640 |        3.8 |  100 |   71976  |      18.6 |     30.0    | RAW8
^C
received SIGINT Ctrl-C

closing and exiting
yocto:/$ 

```

## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-cam.c).
