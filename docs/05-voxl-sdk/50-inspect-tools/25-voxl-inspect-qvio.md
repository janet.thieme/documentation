---
layout: default
title: VOXL Inspect qVIO
parent: Inspect Tools
search_exclude: true
nav_order: 25
permalink: /voxl-inspect-qvio/
---

# VOXL Inspect QVIO
{: .no_toc }


`voxl-inspect-qvio` is a tool to check VIO localization data coming from QVIO. It requires that [VOXL QVIO Server](/voxl-qvio-server/) is running in the background, which can be checked with [VOXL Inspect Services](/voxl-inspect-services/).

---

## Arguments

```bash
yocto:/$ voxl-inspect-qvio --help

typical usage
/# voxl-inspect-qvio

This will print out qvio data from Modal Pipe Architecture.
By default this opens the extended qvio pipe /run/mpa/qvio_extended/
but this can be changed with the --pipe option.

Position and rotation will always print. Additional options are:
-a, --imu_angular_vel       print imu_angular_vel
-b, --accl_gyro_bias        print accl and gyro bias
-c, --time_shift_s          print imu_cam_time_shift_s
-f, --n_feature_points      print n_feature_points
-g, --gravity_vector        print gravity_vector
-h, --help                  print this help message
-m, --extrinsics            print cam to imu extrinsics
-n, --newline               print each sample on a new line
-p, --pipe {pipe_name}      optionally specify the pipe name
-q, --quality               print quality
-t, --timestamp_ns          print timestamp_ns
-v, --vel_imu_wrt_vio       print vel_imu_wrt_vio
-z, --print_everything      print everything
```


## Example Output

```
yocto:/$ voxl-inspect-qvio

    T_imu_wrt_vio (m)   |Roll Pitch Yaw (deg)| state| error_code
   -4.96    0.94   -0.00|  17.9  -52.3    9.3| OKAY | 
^C
received SIGINT Ctrl-C

closing and exiting
yocto:/$ 
```


## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server).

