---
layout: default
title: VOXL Inspect Pose
parent: Inspect Tools
search_exclude: true
nav_order: 45
permalink: /voxl-inspect-pose/
---

# VOXL Inspect Pose
{: .no_toc }

This inspect tool can display data from `pose_vel_6dof_t` type pipes. It is usually used for checking the local pose which is sent to PX4, and the fixed pose which is the result of [AprilTag relocalization](/voxl-vision-hub-apriltag-relocalization/). Both of these pipes are published by [voxl-vision-hub](/voxl-vision-px4/).

---

## Arguments

```bash
yocto:/$ voxl-inspect-pose --help

Tool to print pose data to the screen for inspection.
The --local and --fixed arguments are shortcuts to view the local and fixed
frame poses published by voxl-vision-hub. However, any 6-dof pose pipe can
be inspected.

-f, --fixed             print pose of body with respect to fixed frame
-h, --help              print this help message
-l, --local             print pose of body with respect to lcoal frame
-n, --newline           print newline between each pose

typical usage:
/# voxl-inspect-pose -l
/# voxl-inspect-pose vvpx4_body_wrt_local
/# voxl-inspect-pose -f
/# voxl-inspect-pose vvpx4_body_wrt_fixed

```

## Example Use

```bash
yocto:/$ voxl-inspect-pose --local

timestamp(ms)|     Position (m)     | Roll Pitch Yaw (deg) |    Velocity (m/s)    | angular rate (deg/s) |
      791952 |   0.05  -0.03  -0.10 |    1.6   -5.3   -0.1 |   0.00   0.00   0.00 |   0.17  -0.24   0.08 |^
```

## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-pose.c).
