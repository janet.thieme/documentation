---
layout: default
title: VOXL Inspect Battery
parent: Inspect Tools
search_exclude: true
nav_order: 30
permalink: /voxl-inspect-battery/
---

# VOXL Inspect Battery
{: .no_toc }

This tool subscribes to the `/run/mpa/vvpx4_sys_status` pipe published by [voxl-vision-hub](/voxl-vision-px4/) which provides a copy of all SYS_STATUS mavlink messages received from a PX4 flight controller. Among other things, this packet contains battery status information which can be viewed here.

Also note that [voxl-portal](/voxl-portal/) subscribes to the same pipe and also shows battery percentage in the top right corner of the webpage.


---

## Use

```bash
yocto:/$ voxl-inspect-battery

 Voltage |  Charge | Current |
  12.22V |    89%  |   0.49A |
```

## Troubleshooting

If no data is displayed, then either [voxl-vision-hub](/voxl-vision-px4/) is not running or PX4 is not connected or operating.

## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-battery.c).

