---
layout: default
title: VS Code User Guide
parent: Custom VOXL Applications
nav_order: 23
permalink:  /voxl2-vs-code-user-guide/
youtubeId: SO_OG64fK30
---

# VOXL 2 VS Code
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Overview

Microsoft's VS Code is a free and open source IDE.  In the video below, you'll learn how to use VS Code to remotely browse the VOXL2 file system and build a package on target.
