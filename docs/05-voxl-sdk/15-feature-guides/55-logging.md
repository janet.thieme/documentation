---
title: Logging
layout: default
parent: Feature Guides
has_children: true
nav_order: 55
permalink: /voxl-logger/
---

Tool to save data published through Modal Pipe Architecture to disk. By default, this saves images to /data/voxl-logger/ since the /data/ parition is the largest partition on VOXL. You can change this with the -d argument.

The [voxl-replay](/voxl-replay/) tool is used to play back the logs on target.

```
-c, --cam {name}            name of a camera to log, e.g. tracking
-d, --directory {dir}       name of the directory to save the log to, default
                              is /data/voxl-logger/ if argument is not provided.
-i, --imu {name}            name of an imu to log, e.g. imu1.
-k, --skip {n_to_skip}      number of samples to skip between logged samples
-h, --help                  print this help message
-n, --note {note}           optionally add a very short note to the end of the log
                              directory name, e.g. flight_with_new_motors
-o, --preset_odometry       record a preset log containing qvio, tracking cam and both
                              IMUs for testing VIO. This can be used with other pipes.
-s, --samples {samples}     maximum samples to save for one specific channel,
                              otherwise it will run untill you quit the program.
-t, --time {seconds}        time to log for in seconds.
-v, --vio {name}            name of a vio pipe to log, e.g. qvio
-z, --debug                 enable verbose debug mode
```

typical uses:

  To log one camera image:

    ```yocto:/# voxl-logger --cam tracking --samples 1```

  To log 5000 imu samples:

    ```yocto:/# voxl-logger -i imu1 -s 5000 --note "primary imu test"```

  To log 1000 imu0 samples and 2000 imu1 samples:

    ```yocto:/# voxl-logger -i imu0 -s 1000 -i imu1 -s 2000```

  To log every 5th imu sample (skip 4) for 5.5 seconds:

    ```yocto:/# voxl-logger -i imu1 --skip 4 -t 5.5```

  To log tracking camera and both imus until you press ctrl-c

    ```yocto:/# voxl-logger --preset_odometry```

  To record a typical log for testing VIO (imu + tracking) for 1 minute
  
    ```yocto:/# voxl-logger --preset_odometry --time 60 --note "log for vio replay test 1"```

 

[Source code here](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger)
