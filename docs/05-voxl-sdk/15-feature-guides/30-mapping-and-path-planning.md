---
title: Mapping and Path Planning
layout: default
parent: Feature Guides
has_children: true
nav_order: 30
permalink: /voxl-mapper/
---

# VOXL Mapper

The voxl-mapper package provides a way to map and plan through indoor environments. It enables 3D volumetric mapping via onboard sensors and allows for path planning in both known and unknown environments.

[Source Code](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper)

{% include youtubePlayer.html id="gqlSKRP8prc" %}


{: .alert .danger-alert}
**WARNING:** VOXL Mapper is provided as a reference package for users to better understand how such a system would fit within the VOXL ecosystem. Make sure you are following all proper safety protocols before attempting autonomous flight. ModalAI is NOT responsible for any damages caused during the use of VOXL Mapper.
