---
layout: default
title: Connect to QGC over 4G/5G
parent: Feature Guides
nav_order: 53
has_children: false
permalink: /sentinel-user-guide-connect-gcs/
---

# How to Connect to QGC over 4G/5G

TODO CLEANUP THIS PAGE

{: .no_toc }

1. TOC
{:toc}

## Prerequisites

Before attempting to connect over 4G/5G, make sure you understand how to [connect to QGC over WiFi](/qgc-wifi/) and how [Mavlink Routing on VOXL](/mavlink/) works.

## VPN Setup


When both drone and GCS are connected to LTE networks, they will each be assigned IP addresses that are private to the carrier network. This prevents the drone from directly connecting to the GCS since their IP addresses are not visible to each other over the internet.

To get around this, we use a VPN. This diagram below shows how the VPN solution works. In this example a server is allocated with a static IP address, 35.236.55.229. Once the drone has connected to the AT&T network, it obtains the IP address 10.47.x.y and the GCS, once connected, obtains the address 10.47.x.z. With the VPN server software running on the cloud server and VPN client software on both the drone and the GCS, the devices can now connect and get VPN IP address assignments. In this diagram, the drone is assigned 10.8.0.6 and the GCS is assigned 10.8.0.8. The drone can now communicate directly to the GCS using the 10.8.0.8 IP address.

![LTE data flow](/images/rb5/lte-data-flow-v2.png)

We prefer to use [Tailscale](https://tailscale.com/) as our VPN provider. Tailscale is extremely simple, fast, and straight-forward to setup. It’s free tier is a solid choice for robotics use cases. But, if you prefer to host your own server, you can do so with [Google Cloud Platform](https://cloud.google.com/), [Amazon Web Services](https://aws.amazon.com/), [Microsoft Azure](https://azure.microsoft.com/en-us/), etc. Ubuntu is our preferred OS for our servers. Our self-hosted VPN server is setup using the [OpenVPN](https://openvpn.net/) software package.

It is desirable for the drone and the GCS to always get the same IP address when connecting to the VPN. This is possible by assigning each separate network endpoint a unique security certificate. When each endpoint connects using it’s certificate it can be configured to receive the same address every time.

In order to install the Tailscale client on VOXL 2, follow these instructions: [Installing Tailscale on Ubuntu 18.04](https://tailscale.com/download/linux/ubuntu-1804).

If all goes well, you should get a connected status in QGC.




First follow the steps to [connect to the 5G network](/5G-Modem-user-guide/).


A typical use case of an LTE connection is to connect a drone to a Ground Control Station (GCS) such as [QGroundControl](http://qgroundcontrol.com/). Typically the GCS is also connected to an LTE network. In this case, both the drone and the GCS have been assigned IP addresses on the LTE network that are private to the carrier network. This prevents the drone from directly connecting to the GCS since their IP addresses are not visible to each other over the internet.

There are a variety of ways to solve this issue. ModalAI uses a VPN as the preferred solution.

![LTE data flow](/images/rb5/lte-data-flow.png)

This diagram shows how the VPN solution works. In this example a server is allocated with a static IP address, `35.236.55.229`. For VOXL2 based devices, we prefer to use [Tailscale](https://tailscale.com/) as our VPN provider.

If you prefer to host your own server, you can do so with [Google Cloud Platform](https://cloud.google.com/), [Amazon Web Services](https://aws.amazon.com/), [Microsoft Azure](https://azure.microsoft.com/en-us/), etc. Ubuntu is our preferred OS for our servers. Our self-hosted VPN server is setup using the [OpenVPN](https://openvpn.net/) software package.

In the example above, once the drone has connected to the AT&T network, it obtains the IP address `10.47.x.y` and the GCS, once connected, obtains the address `10.47.x.z`.

With the VPN server software running on the cloud server and VPN client software on both the drone and the GCS, the devices can now connect and get VPN IP address assignments. In this diagram, the drone is assigned `10.8.0.6` and the GCS is assigned `10.8.0.8`. The drone can now communicate directly to the GCS using the `10.8.0.8` IP address.

It is desirable for the drone and the GCS to always get the same IP address when connecting to the VPN. This is possible by assigning each separate network endpoint a unique security certificate. When each endpoint connects using it's certificate it can be configured to receive the same address every time.

## Tailscale VPN Setup

In order to install the Tailscale client on VOXL2, follow these instructions: [Installing Tailscale on Ubuntu 18.04](https://tailscale.com/download/linux/ubuntu-1804)

Tailscale is extremely simple, fast, and straight-forward to setup. It's free tier is a solid choice for robotics use cases.

## Confirm Connection

If connection succeeds, you should get a connected status in QGC.


