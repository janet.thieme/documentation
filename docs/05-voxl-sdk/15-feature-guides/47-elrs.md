---
title: ELRS
layout: default
parent: Feature Guides
has_children: true
nav_order: 47
permalink: /voxl-elrs/
---

# voxl-elrs

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>

## Overview

<i class="fab fa-gitlab" style="color: #e66100;"></i>
[voxl-elrs source code](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs)
[ExpressLRS Project Page](https://www.expresslrs.org/faq/)

### What is ExpressLRS?
ExpressLRS (ELRS) aims to provide the best completely open, high refresh radio control link while maintaining a maximum achievable range at that rate with low latency. Vast support of hardware in both 900 MHz and 2.4 GHz frequencies using the crossfire protocol.

### What is voxl-elrs?
`voxl-elrs` is an application introduced in VOXL SDK 1.0.0 which gives a user the ability to flash a receiver or transmitter, put a receiver into bind mode, and get device information from a receiver.

There is a large array of ELRS compatible receivers and transmitters on the market and we haven't tested them all, but below is a list of some that we know `voxl-elrs` supports.

In theory, `voxl-elrs` supports any receiver that is compatible with ELRS, however some functionalities such as setting a receiver into bind mode may be deprecated since this action requires firmware modifications. Here is a list of currently supported receivers and transmitters:


| Receiver            | Frequency | Latest FW | voxl-elrs |
|:-------------------:|:---------:|:---------:|:---------:|
| BetaFPV Nano RX     |  915MHz   |  3.2.1.2  |   0.0.7+  |
| FrSky R9MM/Mini-OTA |  915MHz   |  3.2.1.3  |   0.1.0+  |

| Transmitter         | Frequency | Latest FW | voxl-elrs |
|:-------------------:|:---------:|:---------:|:---------:|
| iFlight Commando 8  |  915MHz   |  3.0.1    |   0.0.7+  |


## Binding Procedure

{% include youtubePlayer.html id="7OwGS-kcFVg" %}

**Note: Ensure that voxl-elrs version is 0.0.8+ and that both the receiver and transmitter are on ELRS version 3.0.0+!** 
## Usage
Currently, `voxl-elrs` does not require any configuration in order to get started. Usage is as follows:

``` 
voxl2:/$ voxl-elrs -h
Description: Flash a RX or TX with ELRS firmware.

Usage: voxl-elrs -h -d -w -q -b -u -v -s

Optional Arguments:
 -h, --help              Show this help message and exit
 -d, --device            Serial port to use for flashing
 -w, --wizard            Activate interactive wizard
 -q, --quiet             Quiet mode, don't print msgs
 -b, --bind              Put receiver in bind mode
 -u, --unbind            Unbind a receiver
 -v, --version           Get receiver version information
 -s, --scan              Scan receiver for firmware information

```

### Argument Descriptions:
- **-h/--help:** Show the help message and exit.    
- **-d/--device:** Specify a specific serial port to use when flashing a receiver or transmitter. By default, this will be set to /dev/ttyUSB0 if flashing a transmitter and /dev/slpi-uart-7 when flashing a receiver through voxl2.    
- **-w/--wizard:** Activate the interactive wizard that provide user options.    
- **-q/--quiet:** Quiet mode, don't print out any messages. Voxl-elrs program will not print out any messages but underlying programs that are called may still print some messages.    
- **-b/--bind:** Set a receiver that has been flashed with ModalAI firmware into bind mode.    
- **-u/--unbind:** Unbind a receiver that has been flashed with ModalAI firmware.    
  - **Note: Unbind option only available in SDK 1.0.1+ and voxl-elrs version 0.0.9+.**
- **-v/--version:** Get version information from a receiver. This includes receiver name (Ex: BetaFPV 900RX) and firmware version (Ex: 3.2.1.1).    
- **-s/--scan:** Scan a connected receiver for firmware information, automatically update to latest firmware if detected version is out of date.    


## Wiring Guide
### BetaFPV Nano RX
![BetaFPV Nano RX Wiring Diagram](/images/voxl-sdk/voxl-elrs/BetaFPV_wiring.png)

### FrSky R9MM/Mini-OTA
![FrSky R9Mini Wiring Diagram](/images/voxl-sdk/voxl-elrs/FrSky_R9Mini_wiring.png)

## Manually flashing a receiver
- In order to manually flash an ELRS receiver, make sure that the receiver is not in bootloader mode and that it is not connected to a transmitter. These conditions are necessary so we can get device information from the receiver.  
- On host computer adb shell onto the voxl2 or ssh:    
``` bash
adb shell
OR
ssh root@<voxl2 ip> 
```
**Note: The default ssh password is oelinux123** 

- Now, on the voxl2 use the `voxl-elrs` wizard and select the "Flash Receiver" option:
``` bash
voxl2:/$ voxl-elrs -w
Select an option:
[1] Quit
[2] Flash Receiver
[3] Flash Transmitter
[4] Unbind Transmitter
[5] Bind Mode
[6] Device Info
[7] Recover Receiver
2
```
- Select which receiver you are flashing:    
```bash 
Select an option:
[1] Quit
[2] Auto-detect
[3] BetaFPV Nano RX
[4] FrSky R9MM/Mini
```

- The receiver you selected will now be flashed with the latest firmware!    
![Successful flash](/images/voxl-sdk/voxl-elrs/elrs_flash_3.png)

## Recovering a bricked receiver
- Manually place the receiver into bootloader mode by holding down the boot button while power cycling the receiver.    
![Bootloader button](/images/voxl-sdk/voxl-elrs/elrs_recover_1.jpg)
- On host computer adb shell onto the voxl2 or ssh:    
``` bash
adb shell
OR
ssh root@<voxl2 ip> 
```
- Now, on the voxl2 use the `voxl-elrs` wizard and select the "Recover Receiver" option:
``` bash
voxl2:/$ voxl-elrs -w
Select an option:
[1] Quit
[2] Flash Receiver
[3] Flash Transmitter
[4] Unbind Transmitter
[5] Bind Mode
[6] Device Info
[7] Recover Receiver
7
```
- Select which receiver you are recovering:    
```bash 
Select an option:
[1] Quit
[2] Auto-detect
[3] BetaFPV Nano RX
[4] FrSky R9MM/Mini
```
**Note: Only recovery for BetaFPV Nano RX is currently available, failed flash of STM32 based receivers doesn't leave them in a "bricked" state**    

- Follow the instructions given and if the recovery is successful you should see an output like the image below    
![Bootloader button](/images/voxl-sdk/voxl-elrs/elrs_recover_2.png)

- If the recovery process fails, double check that the receiver is actually in bootloader mode. Make sure that you hold the bootloader button down for a couple seconds after power cycling the receiver.


## Voxl-configure-elrs
`voxl-configure-elrs` is an application introduced in `voxl-elrs` v0.1.1 (available after SDK 1.0.0) which gives a user the ability to configure PWM channels on a PWM capable receiver.
Input channels from a transmitter or other controller (such as a steam deck) can be assigned to a PWM output channel. The number of output PWM channels is
limited to the amount of exposed PWM capable pins on a particular receiver.      

## Usage
``` 
voxl2:/$ voxl-configure-elrs --help 
Description: Configure ELRS receiver PWM channels.

Usage: voxl-configure-elrs -h -w -o -d -c

Optional Arguments:
 -h, --help              Show this help message and exit
 -w, --wizard            Activate interactive wizard
 -o, --off               Disable PWM output on all pins
 -d, --default           Set PWM input channels to default values
 -c, --custom            Set PWM input channels to custom values
```

### Argument Descriptions:
- **-h/--help:** Show the help message and exit.    
- **-w/--wizard:** Activate the interactive wizard that provide user options.    
- **-o/--off:** Disable PWM output on all pins    
- **-d/--default:** Set PWM input channels to ModalAi factory default values.    
- **-c/--custom:** Set PWM input channels to user defined values.    


## PWM Capable Receivers    

The R9MM/Mini-OTA is used as an example of enabling PWM on an STM32 based ELRS receiver (there is no native ELRS support for PWM on STM32 based receivers in ELRS firmware version 3.2.1.X). This receiver is supported by voxl-elrs but not necessarily sold by ModalAI.

| Receiver            | Frequency | PWM Channels | Latest FW | voxl-elrs |
|:-------------------:|:---------:|:------------:|:---------:|:---------:|
| FrSky R9MM/Mini-OTA |  915MHz   |       3      |  3.2.1.3  |  0.1.1+   |    

### FrSky R9MM/Mini-OTA PWM outputs
![FrSky R9MM/Mini-OTA PWM Channels](/images/voxl-sdk/voxl-elrs/FrSky_R9Mini_PWM.png)


## Tips
- In order for a receiver and transmitter to bind, they need to be on the same major version of ELRS. For voxl-elrs, this means that **both the transmitter and receiver need to be on ELRS version 3.0.0+**.
- The recommended usage of this tool is to use the interactive wizard, by either using `voxl-elrs -w` or `voxl-elrs --wizard` and then following the prompt.
- If you just want to bind a transmitter to your receiver, you can use the `voxl-bind-elrs` helper utility which will set the receiver into bind mode without having to use the wizard.
  - **Note: You cannot set a receiver into bind mode if it is actively connected to a transmitter**. Turn the transmitter off or unbind the receiver and then run either `voxl-bind-elrs`, `voxl-elrs --bind`, or run the wizard and follow the prompt for bind mode.
- If making your own custom modifications to the receiver firmware, be careful using the `voxl-elrs --scan` function. If `voxl-elrs` detects a firmware version that doesn't match the latest for that version of `voxl-elrs`, it will overwrite the firmware you have currently loaded.
- You can determine the state of the receiver by inspecting the LED on your receiver and comparing to the information on [the ELRS LED Status page](https://www.expresslrs.org/quick-start/led-status/).
- Useful information regarding all things ExpressLRS (ELRS) can be found on the [ELRS quickstart page](https://www.expresslrs.org/quick-start/getting-started/).
