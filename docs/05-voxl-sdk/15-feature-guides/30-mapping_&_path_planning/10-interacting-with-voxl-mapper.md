---
layout: default
title: Interacting with VOXL Mapper
parent: Mapping and Path Planning
search_exclude: true
nav_order: 10
permalink: /interacting-with-voxl-mapper/
---

# Interacting with VOXL Mapper
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Interacting with VOXL Mapper through VOXL Portal
### Viewing the map
In VOXL Portal, select the map tab to interact with voxl-mapper. VOXL Portal has two options for viewing. A 2D view which will show the distances to objects for a given height (this view represents a slice through the 3D map at the specified height). There is also a 3D view which will show the mesh of the surrounding area.

### Sending a Plan
Select `Plan to a Point` and click anywhere on the map. A point will appear at a height of 0. Use the arrows to drag the point to your desired location. Next, select `Go` to create a plan to that point. Once a plan is found, press the `Follow Path` button at the bottom of the page to tell the robot to follow the path. You may press `Abort` at anytime to stop the robot from moving. Please adhere to the safety messages before allowing the robot to fly autonomously.

Alternatively, you can press `Go to Home` to have the robot plan to its home position. Home will always be 1.5m above the (0, 0) location.

### Saving and Loading Maps
Within the map view of VOXL Portal, there are options for `Save Map` and `Load Map`. To save a map, select `Save Map`, optionally enter a directory path for the files to be saved at (defaults to /data/voxl-mapper/), and choose a mesh format to save from the radio options. The files that will be saved are: esdf_map, tsdf_map, mesh.<EXT>(depending on file type you select), with the esdf and tsdf maps used by voxl-mapper and the mesh as an export tool. 

For loading maps, simply select `Load Map` and optionally enter a directory path for the files to be loaded from (default again is /data/voxl-mapper).

### Mesh Api
VOXL Portal also serves an Api to directly grab the mesh data in one of three file formats: .ply, .obj, or .gltf. To use this Api, simply fetch <VOXL_IP>/mesh_api/<FORMAT>, where format can be any of the supported file extensions without the . (ply, obj, gltf), 

Example: fetch(192.168.8.1/mesh_api/gltf)
