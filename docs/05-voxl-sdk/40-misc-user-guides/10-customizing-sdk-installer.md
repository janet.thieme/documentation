---
title: Customizing Your SDK Installer
layout: default
parent: Misc. User Guides
has_children: true
nav_order: 10
permalink: /customize-sdk-installer/
<!-- permalink: ADD HERE! Should look like /voxl-2-hardware-quickstart/  -->
---

It is possible to customize your SDK installer to do things like configure VOXL automatically during the SDK flashing process. This may be useful especially if you are manufacturing a large number of drones.

This page will outline some possible ways to do this.

## The Post-Tweaks Method

### Overview
Inside of each SDK tar archive is a folder called `post-tweaks`. In this folder you can add a bash script called `apply-tweaks.sh` containing any extra setup and configuration steps you may want run automatically. This script will be executed from within the `post-tweaks` directory by the top-level install.sh script after the normal SDK installation. Depending on the SDK you are trying to install, this script may already exist and contain something from ModalAI.

You can also add files to this folder and `adb push` them to VOXL in the `apply-tweaks.sh` script.

### Example

This `apply-tweaks.sh` script pushes an [extrinsics configuration](/configure-extrinsics/) file from the host computer and to VOXL's filesystem. It then sets this extrinsics file as VOXL's new extrinsics configuration. Finally, the script configures VOXL's camera setup (`7` means ToF + Stereo + Tracking cameras). This example might be useful, for example, if you are designing a drone with a custom camera configuration, and need to flash the latest firmware to a large number of drones.
```
#!/bin/bash

adb shell "rsync -r /share/qualcomm-proprietary/usr/ /usr"

echo
echo "Configuring Extrinsics"
adb push custom_extrinsics_file.conf /etc/modalai/
adb shell "voxl-configure-extrinsics custom /etc/modalai/custom_extrinsics_file.conf"
adb shell "voxl-configure-cameras 7"

echo
echo "Rebooting"
adb reboot
adb wait-for-device

echo "Done"
```
