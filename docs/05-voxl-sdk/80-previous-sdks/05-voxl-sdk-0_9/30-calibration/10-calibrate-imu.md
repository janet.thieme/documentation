---
layout: default
title: Calibrate IMU 0.9
parent: Calibration 0.9
search_exclude: true
nav_order: 10
permalink: /calibrate-imu-0_9/
---

# Calibrate IMU
{: .no_toc }

Included in the [voxl-imu-server](/voxl-imu-server/) package are two tools for calibrating the two IMUs on VOXL and any auxiliary IMUs connected to an external SPI bus.

Calibration data is saved to `/data/modalai/voxl-imu-server.cal` alongside all other calibration files.


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Standard 6DOF Calibration

This is a multi-step process that will walk you through doing a static offset calibration for the gyroscope followed by a 6DOF calibration for the accelerometer. This is very similar to the gyro and accelerometer calibration procedure for PX4 in QGroundControl.

It will also save the average IMU temperatures as part of this process which will be used in conjunction with the optional temperature calibration results if that is done in addition to this static calibration routine. In either case, we recommend performing this calibration with a warmed up VOXL board. If you intend to do the temperature calibration routine, it may be a good idea to do that FIRST since it will heat the board up. Then come back and do this static calibration after.

Note that starting the calibration routine will put the IMU server into calibration mode which causes it to output raw unrotated data from the IMUs, this will most likely cause any background VIO service to fail. After calibration is complete, the server will automatically load in the new calibration data and resume publishing corrected and rotated IMU values without needing to be restarted!

#### Step 1
{: .no_toc}

ADB or SSH into VOXL. We recommend doing this wirelessly over SSH to allow more mobility when moving the drone around into the 6 required orientations.

#### Step 2
{: .no_toc}

(Optional) make sure voxl-imu-server is running

```
yocto:/$ voxl-inspect-services
 Service Name          |  Enabled  |   Running   |  CPU Usage
----------------------------------------------------------------
 voxl-imu-server       |  Enabled  |   Running   |     1.8
```

#### Step 3
{: .no_toc}

(Optional) Make sure both IMU0 and IMU1 are being published, along with any auxiliary IMUs you may have enabled.

```
yocto:/$ voxl-list-pipes | grep "imu"
imu0
imu1
```

#### Step 4
{: .no_toc}

Launch the voxl-calibrate-imu utility and check that it detected both IMUs. Then press ENTER to start collecting data for the static gyroscope offset. Make sure the drone is upright in a normal flight orientation and is perfectly still! If you have a fan connected to the VOXL fan header, it should have stopped automatically at this point to reduce vibration.

```
yocto:/$ voxl-calibrate-imu
Server has IMU0 enabled and available for calibration
Server has IMU1 enabled and available for calibration
detected voxl-fan utility
turning off fan to reduce imu noise

We're about to start calibrating the gyro, make sure VOXL is
COMPLETELY STILL and press ENTER to start.
```

#### Step 5
{: .no_toc}

After 1 second the results of the static gyroscope should print.

```
imu0 gyro offset (rad/s): -0.0115 -0.0130  0.0101
imu0 gyro baseline temp (C):   61.30
imu1 gyro offset (rad/s): -0.0101  0.0016  0.0134
imu1 gyro baseline temp (C):   55.97
```

#### Step 6
{: .no_toc}

Now we are going to perform the 6DOF accelerometer calibration. This involves placing the drone perfectly still in 6 roughly orthogonal orientations such that the offset and scale of all 3 axis can be measured.

The 6 orientations do not have to be perfectly aligned with gravity or perfectly orthogonal to each other. This calibration is not going to treat each axis individually and assume the accelerometer is experiencing exactly 1G in each orientation. Instead it takes all 6 points and fits an ellipsoid to the data. The centroid and size of this ellipsoid defines the accelerometer offset and scale.

That being said, IMUs are not perfectly linear and orienting the drone such that each axis aligns within about 10 degrees of gravity will help exercise the full range of the sensor and give the best results.


```
We're about to start calibrating the accelerometer.
This process will require you to orient the VOXL board in six roughly
orthogonal orientations. Ideally this should be with the six sides of
the board facing down. Try to do this without touching the board.


When you are ready to collect data for orientation 1 of 6,
press ENTER to start or Ctrl-C to quit
```

#### Step 7
{: .no_toc}

Now go through the remaining 5 orientations. The exact order does not matter, just be sure to keep track and don't double up or the process will fail!

```
RESULTS:
accl0 offsets (m/s2):   0.0118 -0.0010  0.1146
accl0 scales        :   1.0017  1.0011  1.0010
accl0 baseline temp:   47.57C
accl1 offsets (m/s2):  -0.0958 -0.0903  0.0205
accl1 scales        :   1.0015  1.0005  1.0026
accl1 baseline temp:   42.95C

Successfully wrote calibration to disk
voxl-imu-server should reload the new calibration automatically.
You can now run voxl-inspect-imu to check that the new data looks good
You can also optionally run voxl-calibrate-imu-temp to run a more
involved temperature calibration.
```

#### Step 8
{: .no_toc}

If the calibration was successful, the results should now be saved to /data/modalai/voxl-imu-server.cal and voxl-imu-server should have reloaded the new data.


## Temperature Calibration

This is an optional calibration step which uses the CPU to heat up the VOXL PCB and the IMUs, then measures and records a quadratic model of the IMU temperature drift per axis. We recommend doing this with VOXL mounted in the drone and oriented in a normal flight orientation since temperature gradients and stress on the PCB will effect these results.

The process is slow and boring, it should take 2-4 minutes. The on-screen instructions are fairly self explanatory so we simple paste them here for reference:


#### Step 1 Cold Stage
{: .no_toc}

```
yocto:/$ voxl-calibrate-imu-temp
Server has IMU0 enabled and available for temperature calibration
Server has IMU1 enabled and available for temperature calibration

============================================================================
We're now starting the COLD stage of the calibration.
Please stop all unnecessary services in another terminal, and try your best
to cool the PCB with a fan. Alternatively, start this process on a cold PCB.

We will be measuring the static gyro and accel offset as the board heats up.
VOXL must remain completely still through this entire process!
The IMU temperatures will be printed here continuously.

Press ENTER to start the heating process when you are happy that the IMUs
are cold enough for the lower bound. 25-35C is a reasonable goal for this.
============================================================================
CPU: 42.7C   IMU0: 41.7C   IMU1: 33.0C
```


#### Step 2 Start Hot stage
{: .no_toc}

```
============================================================================
We're now starting the HOT stage of the calibration. We just turned off
the fan, and started stressing the CPU to heat up the PCB.
This will run automatically for a while collecting data points until enough
data has been collected, something gets too hot, or the timeout is reached.
============================================================================
CPU: 62.9C   IMU0: 45.8C   IMU1: 38.3C
```

#### Step 3 Wait for Hot stage to finish
{: .no_toc}

```
CPU: 76.4C   IMU0: 53.0C   IMU1: 46.1C   taking sample #2
CPU: 86.7C   IMU0: 59.5C   IMU1: 53.2C   taking sample #3
CPU: 93.2C   IMU0: 66.0C   IMU1: 59.5C   taking sample #4
CPU: 94.8C   IMU0: 72.5C   IMU1: 66.6C   taking sample #5
CPU: 94.4C   IMU0: 75.1C   IMU1: 69.1C   IMU0 hit temp limit, finishing test
taking sample #6
```

#### Step 4 Results
{: .no_toc}

```
COMPUTING RESULTS

Successfully wrote calibration to disk
voxl-imu-server should reload the new calibration automatically.
You can now run voxl-inspect-imu to check that the new data looks good

Now is probably a good time to run the normal voxl-calibrate-imu
calibration routine since the IMUs should both be up to temperature.
```

## Source

The code is available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server)
