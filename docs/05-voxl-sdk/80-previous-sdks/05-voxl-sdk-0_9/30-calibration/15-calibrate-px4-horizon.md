---
layout: default
title: Calibrate PX4 Horizon 0.9
parent: Calibration 0.9
search_exclude: true
nav_order: 15
permalink: /calibrate-px4-horizon-0_9/
---

# Calibrate PX4 Level Horizon

This is a new feature in [voxl-vision-px4](/voxl-vision-px4/) 0.9.5

This calibration process is an in-flight alternative to the level horizon calibration method in QGC. By doing this in flight, airframe asymmetries are taken into account, and you do not need a perfectly level surface with perfectly level landing gear.

We still recommend having QGC open for this process to monitor PX4 and and battery health. voxl-vision-px4 will announce when it has started and completed the calibration process through a mavlink message to QGC so turn the volume up so you can hear when it is finished.

For this you should take off in position mode with VIO enabled indoors in a large enough room to avoid excessive turbulence. Make sure airflow in the room is minimized, including turning off fans and AC units.

When you are ready, take off in position mode and fly around for 20-30 seconds to allow VIO to stabilize and for EKF2 to converge on its IMU bias. Then navigate the drone to the middle of the room and leave it still.

voxl-vision-px4 will monitor the roll/pitch values reported by PX4 until the drone remains still enough for 20 seconds to be confident that the measurement is correct. If the still condition is met, it will write new SENS_BOARD_X_OFF & SENS_BOARD_Y_OFF parameters to PX4. The drone may twitch a little in the air depending on how far off it was. voxl-vision-px4 will then announce through QGC that the cal is complete.

If you have a particularly wobbly drone or a lot of turbulence, then the stationary condition may not ever be met. You can increase the allowable noise tolerance in /etc/modalai/voxl-vision-px4.conf with the field horizon_cal_tolerance. Note that increasing this will potentially reduce the accuracy of the calibration. Allow the drone to hover for at least 2 minutes without hearing QGC announce the cal completion and try to remove any potential sources of turbulence in the room before increasing the allowable tolerance.


```
yocto:/$ voxl-calibrate-px4-horizon

Press ENTER to start the calibration process or Ctrl-C to quit


calibration started
yocto:/$
```

TODO: add Video

