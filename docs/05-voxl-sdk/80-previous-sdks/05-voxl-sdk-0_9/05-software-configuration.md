---
layout: default
title: Software Configuration 0.9
parent: VOXL SDK 0.9
search_exclude: true
nav_order: 05
has_children: true
has_toc: true
permalink: /software-configuration-0_9/
---

# Software Configuration

This section covers how to configure VOXL-SDK software that runs on VOXL itself.
