---
layout: default
title: libmodal_exposure 0.9
parent: Core Libs 0.9
search_exclude: true
nav_order: 35
permalink: /libmodal-exposure-0_9/
---

# libmodal-exposure
{: .no_toc }

libmodal_exposure is a beta project that applies an opencv histogram to an image (with metadata) and returns what the new exposure/gain should be.

Additional usage details are coming soon, in the meantime you can find the code and README on [Gitlab](https://gitlab.com/voxl-public/core-libs/libmodal_exposure)



