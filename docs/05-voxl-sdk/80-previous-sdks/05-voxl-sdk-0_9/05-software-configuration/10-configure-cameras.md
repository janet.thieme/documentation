---
layout: default
title: Configure Cameras 0.9
parent: Software Configuration 0.9
search_exclude: true
nav_order: 10
permalink: /configure-cameras-0_9/
---

# Configure Cameras


When setting up a custom VOXL for the first time or when changing camera configurations, you will need to configure VOXL's software to make it aware of the physical camera configuration. This can easily be done by running the `voxl-configure-cameras` script that's included with the voxl-utils package.

```bash
me@mylaptop:~$ adb shell voxl-configure-cameras
```

By default, this will prompt you for the configuration you are using from the list below. If you like, you can skip the prompt by providing the configuration ID as an argument:

```bash
me@mylaptop:~$ adb shell voxl-configure-cameras 1
```

The currently tested and available configurations on VOXL are:
```
For M500 & Flight Deck use: 3
For uSeeker & VOXLCAM  use: 7 rotate_stereo

0  None
1  Tracking + Stereo
2  Tracking Only
3  Hires + Stereo + Tracking
4  Hires + Tracking
5  TOF + Tracking
6  Hires + TOF + Tracking
7  TOF + Stereo + Tracking
8  Hires Only
9  TOF Only
10 Stereo only
```
Also listed are the default configurations that are shipped when ordering an assembly/airframe.

This script will configure the camera drivers and set up the [camera server](/voxl-camera-server/). Due to the fact that the script messes with drivers and other low-level camera options, it is strongly recommended that you restart the board after running the script.


