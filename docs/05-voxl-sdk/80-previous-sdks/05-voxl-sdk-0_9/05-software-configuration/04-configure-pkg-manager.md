---
layout: default
title: Configure Package Manager 0.9
parent: Software Configuration 0.9
search_exclude: true
nav_order: 4
permalink: /configure-pkg-manager-0_9/
---

# Configure Package Manager
{: .no_toc }

## Table of contents
{: .no_toc }

1. TOC
{:toc}

## Overview

The ```voxl-configure-pkg-manager``` script configures the package manager on your platform (```opkg``` on voxl or ```apt``` on voxl2/rb5 flight) to pull from a specific one of our sections.

## Available Sections

All modalai package repositories are available at [http://voxl-packages.modalai.com/dists/](http://voxl-packages.modalai.com/dists/) allowing opkg or apt to install and update packages using an internet connection.

### SDK-*

We will periodically assemble all of our packages into a tested and stable SDK release. These are enumerated to match the version of [voxl-suite](/voxl-suite/) within (i.e. SDK-0.8 has voxl-suite version 0.8 and its dependencies). These releases are considered "stable" in the debian sense that the packages will never be changed or removed, and packages will not generally receive updates after initial release unless a critical bug is discovered. These SDK releases are considered very reliable and endure a rigorous cycle of candidacy and testing internally before we publish a release.

A SDK release is often accompanied by system image updates and so we generally recommend flashing an entire platform release (our name for a combined system image and SDK release). System image requirements for an SDK release can be found in the [changelogs](/voxl-suite/#changelog) section of the voxl-suite page.

### Stable

Stable is a link to the latest SDK release, packages are never uploaded directly to stable, we simply change the location it points to once we complete testing of a release and push a platform to the [public downloads](https://downloads.modalai.com/) page.

### Staging

This is the default-enabled repository and consists of the latest stable packages. Packages here have their internal functionality tested and validated by our team, but have not necessarily gone through as rigorous environment testing as a full SDK release. Packages pushed here require version tags on their [public repos](https://gitlab.com/voxl-public/voxl-sdk) and thus the source of a specific version of each package can be easily viewed. Packages on this repo will not change, though newer versions may appear. This repo is occasionally cleaned (usually immediately following an SDK release).

### Dev

This is the development repository containing often untested or experimental software that is automatically pushed via CI on every commit made by ModalAI's software team. This repository mostly serves for ModalAI to use internally, but is also useful for customers who are testing new features before they are considered stable. Packages on this repo are updated very regularly and are not guaranteed to remain, as it is regularly purged of older packages for cleanliness.

## Keeping updated

If you decide not to use one of the static SDK release repos, you can easily check to see if any updated packages are available to pull by running ```opkg update && opkg upgrade``` on voxl or ```apt update && apt upgrade``` on voxl2/rb5f. Especially if you decide to use staging or dev, new packages will often be available weekly if not daily.

### Additional Note on APT:
{: .no_toc }

VOXL2/RB5F are built on a standard ubuntu 18 image, so we recommend that you run ```apt update && apt upgrade``` regularly on these platforms no matter what as there will often be updates to the standard ubuntu packages even if there haven't been to ours.
