---
layout: default
title: VOXL Inspect Vibration 0.9
parent: Inspect Tools 0.9
search_exclude: true
nav_order: 60
permalink: /voxl-inspect-vibration-0_9/
---

# VOXL Inspect Vibration
{: .no_toc }

`voxl-inspect-vibration` is a lightweight tool to example accelerometer and gyroscope noise and vibration while in flight. It works by calculating the RMS of each accelerometer and gyro channel over n_samples (default 800).

The data is pre-filtered through a configurable butterworth high-pass filter with default cutoff frequency of 50hz so that steady desirable motion does not contribute to the vibration measurement. This is a reasonable cuttoff frequency for the goal of measuring undesirable vibration on an airframe. Howveer, it can be changed with the `--freq` argument to suite different use cases.

All of the outputs are colorized to red, yellow, or green depending on the vibration level. The exact thresholds for these colors can be found in the [source code](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-vibration.c) and are very rough guidelines for what is acceptible.

---

## Arguments

```
yocto:/$ voxl-inspect-vibration --help

Tool to measure IMU vibration through Modal Pipe Architecture.
This works by calculating the RMS of each accelerometer and gyro channel
over n_samples (default 800). This can also be interpreted as the standard
deviation of the signal.

The data is pre-filtered through a configurable butterworth high-pass filter
with default cutoff frequency of 50hz so that steady desirable motion does not
contribute to the vibration measurement. Change this frequency with the --freq
argument. Set it to 0 to disable the filter altogether.

-f, --freq {freq_hz}      high-pass cutoff frequency (hz)
-h, --help                print this help message
-l, --newline             print new line for every sample
-n, --n_samples {samples} number of samples to use for each calculation

typical usage:
/# voxl-inspect-vibration imu1
```

## Example Use

For a drone sitting still, all values should show up in green. The small amount of vibration detected here is due to sensor noise and the fan.

```
yocto:/$ voxl-inspect-vibration imu0

Noise values are RMS values in units of m/s2 and rad/s.
Value in parenthesis are the max values recorded since the start.
Values in RED are high (>5.00m/s2 or >0.50rad/s).
Values in YELLOW are okay (>2.00m/s2 or >0.20rad/s).
Values in GREEN are great!

 Accl (X) | Accl (Y) | Accl (Z) | Gyro (X) | Gyro (Y) | Gyro (Z)
0.03(0.03) 0.02(0.02) 0.01(0.01) 0.00(0.00) 0.00(0.00) 0.00(0.00)
```

## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-vibration.c).
