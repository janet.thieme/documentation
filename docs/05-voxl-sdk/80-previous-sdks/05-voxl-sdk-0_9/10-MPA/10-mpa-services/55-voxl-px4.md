---
layout: default
title: VOXL PX4 0.9
parent: MPA Services 0.9
search_exclude: true
nav_order: 55
has_children: true
has_toc: true
permalink: /voxl-px4-0_9/
---

# VOXL PX4

The `voxl-px4` package provides the infrastructure to install and run PX4 as a Linux service on QRB5165 based platforms (VOXL 2, RB5 Flight).

![voxl-px4](/images/voxl-sdk/voxl-px4/voxl-px4.jpg)

[Source Code](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4)
