---
layout: default
title: Flight Info 0.9
parent: VOXL Portal 0.9
search_exclude: true
nav_order: 20
has_children: false
permalink: /voxl-portal-flight-info-0_9/
youtubeId:
---

# Flight Info
{: .no_toc }

voxl-portal uses the MPA library and output from voxl-vision-px4 to dynamically display your battery percentage, GPS satellite connections, current flight mode, and arm status in the top right of every page. These bits of flight information are completely optional, and if voxl-vision-px4 is not setup correctly and running, these fields will display some version of "unknown". Please see the [voxl-vision-px4 guide](/voxl-vision-px4/) to properly setup this package if encountering issues.

![voxl-portal-flight-info.png](/images/voxl-sdk/voxl-portal/voxl-portal-flight-info.png)

## Battery
In the header, you will see your current battery percentage next to a battery icon. This data is obtained the same as using the [voxl-inspect-battery tool](/voxl-inspect-battery/).

## GPS
Following the battery icon, you will see a bar labeled "GPS SAT:",  followed by the number of satellite connections your drone currently has. This data is obtained the same as using the [voxl-inspect-gps tool](/voxl-inspect-gps/).

## Flight Mode
Following your satellite count, there is a text field that will be populated with your current flight mode.

## Arm Status
Following your flight mode, there is a text field that will be populated with your current arm status (Armed/Disarmed),