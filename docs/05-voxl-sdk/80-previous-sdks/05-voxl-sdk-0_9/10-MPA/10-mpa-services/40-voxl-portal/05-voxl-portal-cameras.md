---
layout: default
title: Viewing Cameras 0.9
parent: VOXL Portal 0.9
search_exclude: true
nav_order: 5
has_children: false
permalink: /voxl-portal-cameras-0_9/
---

# Viewing Cameras
{: .no_toc }

voxl-portal uses the MPA library to dynamically list and display any open camera/image pipes.
When the "Cameras" dropdown is selected, any available image output will be displayed, regardless of supported type. By default, the first option will always be "Multi-View", followed by all other open cameras.

![voxl-portal-camera-list.png](/images/voxl-sdk/voxl-portal/voxl-portal-camera-list.png)

Select the camera you wish to view, and the image will be displayed as long as the image format is supported.
![voxl-portal-qvio-overlay.png](/images/voxl-sdk/voxl-portal/voxl-portal-qvio-overlay.png)

## Supported image formats
* RAW8
* STEREO_RAW8
* RGB
* YUV420
* YUV422
* NV21
* NV12

## Image Quality
voxl-portal will attempt to adjust to your networks capabilities. If frames are being dropped (i.e. the image is flickering), the image quality will drop until they can be sent at a consistent framerate. If you are expecting a higher resolution image than what you are receiving, the cause is likely a poor connection. 

## Multi-View
The Multi-View option is the first under the camera dropdown, and will display a collage of all active and valid image pipes. Each image stream can be turned on or off individually using the checkbox next to their name. 
 
![voxl-portal-multi-view.png](/images/voxl-sdk/voxl-portal/voxl-portal-multi-view.png)

Using the Multi-View page can be very intensive on your network if you have numerous image streams running, thus the quality may be affected as detailed above. If this is an issue, you can try toggling streams you are not interested in or view the topics one at a time via the dropdown.