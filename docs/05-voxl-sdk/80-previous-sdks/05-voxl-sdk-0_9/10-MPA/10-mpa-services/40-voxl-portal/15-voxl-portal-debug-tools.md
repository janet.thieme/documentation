---
layout: default
title: Debug Tools 0.9
parent: VOXL Portal 0.9
search_exclude: true
nav_order: 15
has_children: false
permalink: /voxl-portal-debug-tools-0_9/
youtubeId:
---

# Debug Tools
{: .no_toc }

voxl-portal is able to produce detailed plots with CPU and IMU information.

### CPU
The CPU debug page contains the VOXL Cpu Plotter along with a live view of the running cpu stats. This tool will plot ten second windows of data, with various cpu, gpu, and memory options. You may choose to plot as few or as many of the listed options as you wish simply by selecting their individual buttons. Red options will be ignored, and green options will be plotted. Additional plot tools are listed in the top right corner of the graph, with a plot, pause, and clear button below.
![voxl-portal-cpu-plot.png](/images/voxl-sdk/voxl-portal/voxl-portal-cpu-plot.png)<br>

### IMU
The IMU debug page contains the VOXL Imu Plotter. This tool will plot ten second windows of data, with gyro and acceleration options. You may choose to plot as few or as many of the listed options as you wish simply by selecting their individual buttons. Red options will be ignored, and green options will be plotted. Additional plot tools are listed in the top right corner of the graph, with a plot, pause, and clear button below.
![voxl-portal-imu-plot.png](/images/voxl-sdk/voxl-portal/voxl-portal-imu-plot.png)<br>
