---
layout: default
title: VOXL Hexagon Docker Image 0.9
parent: Build Environments 0.9
search_exclude: true
nav_order: 20
permalink: /voxl-hexagon-0_9/
---

# VOXL Hexagon Docker Image
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Develop for VOXL's Hexagon DSP (aDSP and sDSP/SLPI) in a Docker running on your Linux PC! This tool lets you use the docker build environments described in the [build environments page](/build-environments/).

The voxl-hexagon docker image is based on the x86_64 Ubuntu Bionic docker image but additionally contains the `Qualcomm Hexagon SDK 3.1` and an ARM cross compiler. For legal reasons these components must be downloaded from their respective sources by the user before building the docker image. For convenience, `Qualcomm Hexagon SDK 3.1` is also available to download here : https://developer.modalai.com/asset/7. `Qualcomm Hexagon SDK 3.3.3` is available from https://developer.qualcomm.com/software/hexagon-dsp-sdk/tools (Linux version)

In this project, we provide instructions and a docker build script.


## Prerequisite

Follow the instructions to install Docker CE and the voxl-docker script [here](https://developer.modalai.com/asset/download/).


## Build and Install the voxl-hexagon Docker Image

Complete instructions for building and installing the voxl-hexagon Docker are found in the voxl-docker [README](https://gitlab.com/voxl-public/utilities/voxl-docker/-/blob/master/README.md)

## Signing aDSP modules for use on VOXL
- tested on SDK 3.1 and 3.3.3
- get the board's / DSP serial number:
   - `cd Hexagon_SDK/3.1/tools/elfsigner/getserial/UbuntuARM_Release$`
   - `adb push getserial /home/root/`
   - `adb shell /home/root/getserial`
   - output should be similar to : `Serial Num : 0xfb6b0021`
   - your serial number, e.g `0xfb6b0021` in thise case, should be used below as ``<serial_number>``
- get the signature for your device from Qualcomm
   - `cd Hexagon_SDK/3.1`
   - `./setup_sdk_env.source`
   - `python tools/elfsigner/elfsigner.py -t <serial_number> -o <path_to_output>/elf-signer-output`
   - (note that without setting up the sdk environment, the elfsigner script behaves strangely)
- push the signature to VOXL
   - `adb push testsig-<serial_number>.so /usr/lib/rfsa/adsp/`
   - REBOOT VOXL !!!
- test using a simple project from https://gitlab.com/voxl-public/other/adsp-proc-examples
