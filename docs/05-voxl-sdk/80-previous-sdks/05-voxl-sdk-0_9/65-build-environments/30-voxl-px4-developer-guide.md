---
layout: default
title: VOXL 2 PX4 Developer Guide 0.9
parent: Build Environments 0.9
search_exclude: true
nav_order: 30
permalink:  /voxl-px4-developer-guide-0_9/
---

# VOXL 2 PX4 Developer Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Preface

This guide is geared towards someone interested in all the bits and bytes that make PX4 on VOXL 2 work.  If you are looking instead for a general user's guide, please refer to the [VOXL 2 PX4 User Guide](/voxl-px4-user-guide/)

### Introduction

VOXL 2 is a flight controller that runs PX4.  On VOXL 2, it runs flight critical parts of PX4 software on a real time DSP, and other parts in Ubuntu 18.04 Linux on an 8 core 64-bit ARMv8 processor.

Typical of other flight controllers, there are connectors for external sensors, power, and ESC actuation.  There are sensors like IMUs and barometers, file systems for logging flights, and interfaces for connecting to ground control stations.

But unlike most other flight controllers, PX4 is actually also running in a companion computer with that is capable of doing things like stream 7 image sensors simultaneously for computer vision and support multiple data links like 5G.  Although we will use the companion computer as a "front end" in some of the instructions below, the other guides [here (TODO)]() contain more detailed usage of that part of the system.

### For the PX4 Developer

If you are a PX4 developer who is used to developing on microcontollers.  On VOXL 2:

- PX4 is installed using `apt` package manager (see [boards/modalai/rb5-flight/debian/DEBIAN/](TODO) to see how)
- Connection to QGC is over IP, not USB or serial
- The Non-RTOS parts of PX4 run in Ubuntu 18 (on the applications processor), these are defined [boards/modalai/rb5-flight/default.cmake](TODO), and can be built in a docker container [available here](https://gitlab.com/voxl-public/rb5-flight/rb5-flight-px4-build-docker/-/tree/master)
- The RTOS parts of PX4 run in an RTOS called QuRT (on the sensors DSP), these are defined [boards/modalai/rb5-flight/qurt.cmake](TODO), and can be built in the same docker container [available here](https://gitlab.com/voxl-public/rb5-flight/rb5-flight-px4-build-docker/-/tree/master)
- PX4 is installed via the VOXL SDK (aka voxl-suite), and this guide is meant to allow users to customize this component. 
   - You currently *cannot* install the PX4 for SLPI build via QGroundControl
- The 'guest OS' firmware that runs in the sDSP is included in the system image, providing access to the drivers for UART/I2C/SPI etc.
- When you are connected to VOXL 2's shell, all the normal PX4 command (like `listener`) get a `px4-` prefix, e.g. to use the `listener` command, try `px4-listener`

### Key PX4 Differences to Note

- Currently, there's no connection from QGroundControl to PX4 through the USBC connector
  - The USBC connection provides ADB access only, which brings you to a shell and you can interface with PX4 command line from Linux
- Communications from VOXL 2 to the ground station are over IP.  Please see the [Connecting Quickstart](/voxl2-connecting-quickstart/)

### Block Diagram - Details

The following provides details about the architecture:

<img src="/images/voxl2/m0054-px4-block-diagram-0.4.png" alt="m0054-px4-block-diagram" width="1280"/>
[View in fullsize](/images/voxl2/m0054-px4-block-diagram-0.4.png){:target="_blank"}


## Component Walkthrough

### Processors

| ID  | Name                  | Description |
|---  |---                    |--- |
| A   | `M0054` or `VOXL 2`    | The entire PCBA, containing the `QRB5165M SIP`, sensors, connectors, etc. |
| B   | `QRB5165M`            | The system in package (SIP) populated on `M0054` containing all the processors listed below |
| C   | `spli_proc` or `sDSP` | Sensors DSP (dDSP) - Hexagon V66 - also called Sensors Low Power Island (slpi) |
|     |                       | runs PX4 code built for Qualcomm's Hexagon DSP RTOS (QuRT) specified in `qurt.cmake` |
| D   | `apps_proc`           | CPU, Applications Processor, 8-core 64-bit ARM v8.  Ubuntu 18.04, Kernel 4.19 |
|     |                       | runs PX4 code built for Linux/POSIX specified in `default.cmake` |
|     | `aDSP`                | Audio DSP (aDSP) - Hexagon V66, not used by PX4, not shown on graphic |
|     | `cDSP`                | Compute DSP (cDSP)  Hexagon V66Q - not used by PX4, not shown on graphic |
|     | `GPU`                 | Qualcomm® Adreno™ 650 - not used by PX4, not shown on graphic |
|     | `NPU`                 | Qualcomm® NPU230 - not used by PX4, not shown on graphic |

### Onboard Sensors

| ID       | Name                  | Description |
|---       |---                    |--- |
| E - U16  | IMU0 (slpi)           | ICM-42688p ([src/drivers/imu/invensense/icm42688p]) |
| E - U17  | IMU1 (apps_proc)      | ICM-42688p ([src/drivers/imu/invensense/icm42688p]) |
| E - U18  | BARO0 (slpi)          | BMP-388 ([src/drivers/barometer/bmp388]) |
| E - U19  | BARO1 (slpi)          | ICP-10100 ([src/drivers/barometer/icp10100]) |

### External Sensors and Interfaces

For detailed pinouts, please reference the [VOXL 2 connectors](/voxl2-connectors/) page.

| ID       | Name                  | Description |
|---       |---                    |--- |
| F - J19  | RC UART               | for example Spektrum      |
| F - J19  | GNSS UART             | for example Ublox Neo-M8N |
| F - J19  | MAG I2C               | for example IST8310       |
| F - J19  | Future I2C            | not yet available              |
| F - J18  | UART ESC              | UART for ModalAI 4-in-1 ESC ([src/drivers/uart_esc/modalai_esc]) |
| F - J4   | Power Module I2C      | I2C for ModalAI Power Module  ([src/drivers/power_monitoring/voxlpm]) |
| F - J10  | External SPI          | Future |
| F - J9   | USBC                  | adb connection |
| F - J3/J5| Board to Board        | For add-on boards, USB 2.0 hub, USB 3.0 hub, UART, I2C, etc |

### Software Interfaces

| ID       | Name                  | Description |
|---       |---                    |--- |
| G0       | `libfc_sensor`        | Facilitates UORB communication between the `apps_proc` and `slpi_proc` |
| G1       | MAVLink / IP          | Using the networking stack on the `apps_proc`, MAVLink connections are facilitated over IP |

### Image Sensor Interfaces

Shown called out as `Z`, QTY6 4-lane MIPI-CSI2 + QTY4 CCI interfaces are available via the `J6`, `J7` and `J8` connectors.  See [VOXL 2 camera configs](/voxl2-camera-configs/) for more details.

## Theory of Operation

### Startup

- A service defined by `/etc/systemd/system/voxl-px4.service` is started on bootup by systemd
- The `voxl-px4.service` runs the `voxl-px4` bash script which serves as a CLI wrapper utility for starting `px4`  
- Before starting `px4`, if the PX4 parameters are not present in `/data/px4/parameters`, they are initialized by `/etc/modalai/voxl-px4-set-default-parameters.config`
- A `px4` instance is then started as a daemon process, executing the startup script defined by `/etc/modalai/voxl-px4.config`

## Common Use Cases

### How to Access the PX4 Shell (pxh)

You can access the PX4 shell from the VOXL2 command line by using a `px4-` prefix on commands, which will route them to the PX4 daemon process.

Sometimes, it's easier to debug and get access to the PX4 shell by instead running PX4 as a process:

- adb onto VOXL2


```
adb shell
voxl2:/$
```

- disable the `voxl-px4` deamon by running `systemctl disable voxl-px4`

```
voxl2:/$ systemctl disable voxl-px4
Removed /etc/systemd/system/multi-user.target.wants/voxl-px4.service.
```

- also disable `voxl-mavlink-server` using `systemctl disable voxl-mavlink-server` which can start `voxl-px4` as a dependency

```
voxl2:/$ systemctl disable voxl-mavlink-server
Removed /etc/systemd/system/multi-user.target.wants/voxl-mavlink-server.service.
```

- reboot VOXL2

```
exit
adb reboot
```

- adb back on VOXL2


```
adb shell
voxl2:/$
```

- manually run `voxl-px4`

```
voxl2:/$ voxl-px4
Found DSP signature file
INFO  [px4] mlockall() enabled. PX4's virtual address space is locked into RAM.
INFO  [px4] assuming working directory is rootfs, no symlinks needed.

______  __   __    ___
| ___ \ \ \ / /   /   |
| |_/ /  \ V /   / /| |
|  __/   /   \  / /_| |
| |     / /^\ \ \___  |
\_|     \/   \/     |_/

px4 starting.

INFO  [px4] Calling startup script: /bin/sh /etc/modalai/voxl-px4.config 0
Running on M0054
...
```

- Hit ENTER to get the shell to show up after the console stops updating

```
...
INFO  [uorb] Advertising remote topic vehicle_attitude
INFO  [uorb] Advertising remote topic vehicle_attitude_setpoint
INFO  [uorb] Advertising remote topic vehicle_rates_setpoint

pxh>
```

- Some drivers run on the applications processor.  You can issue these commands directly, like:

```
pxh> commander status
INFO  [commander] arming: STANDBY
```

- Some drivers run on the DSP, and can be accessed using `qshell` like:

```
qshell px4io status

...

INFO  [muorb] SLPI: Direct PWM values: 0 0 0 0 0 0 0 0
INFO  [muorb] SLPI: Ok executing command: px4io status
INFO  [muorb] SLPI: Sending qshell retval with timestamp 151805206, current timestamp 151805206
INFO  [qshell] qshell return value timestamp: 151805206, local time: 151808943
```

### Show a PX4 Parameter

```
px4-param show | grep UART
```

### Backup Parameters from Device

The parameters are stored in `/data/px4/param/parameters`.

### Spin Motors

#### Using UART ESC

The following would spin, sequentially, PX4 motors 1, 2, 3 and 4 at 1000 RPM for ~1 second.

```bash
px4-qshell modalai_esc -i 1 -r 1000 rpm
px4-qshell modalai_esc -i 2 -r 1000 rpm
px4-qshell modalai_esc -i 4 -r 1000 rpm
px4-qshell modalai_esc -i 8 -r 1000 rpm
```

### View uORB Data

```bash
voxl2:/$ px4-listener sensor_accel

TOPIC: sensor_accel
 sensor_accel_s
	timestamp: 126098074  (0.002515 seconds ago)
	timestamp_sample: 126097753  (321 us before timestamp)
	y: -0.2508
	z: -9.7809
	temperature: 32.4728
	error_count: 0
	clip_counter: [0, 0, 0]
```

Other useful topics:

- `px4-listener sensor_accel`
- `px4-listener sensor_gyro`
- `px4-listener sensor_gps`
- `px4-listener sensor_baro`
- `px4-listener battery_status`
- `px4-listener input_rc`

## Source Code

The current [PX4 branch is here](https://github.com/modalai/px4-firmware/tree/voxl-master/).

To build, see the docker container project [available here](https://gitlab.com/voxl-public/rb5-flight/rb5-flight-px4-build-docker/-/tree/master)

[Next: VOXL 2 HITL Guide](/voxl2-PX4-hitl/){: .btn .btn-green }
