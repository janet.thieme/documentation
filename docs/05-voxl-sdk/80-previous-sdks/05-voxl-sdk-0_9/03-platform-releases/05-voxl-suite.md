---
layout: default
title: VOXL Suite 0.9
parent: Software Releases 0.9
search_exclude: true
nav_order: 5
has_children: false
has_toc: false
permalink: /voxl-suite-0_9/
---

# VOXL Suite
{: .no_toc }


## Table of contents
{: .no_toc }

1. TOC
{:toc}

## Overview

VOXL Suite is the collection of VOXL SDK software packages that are installed on VOXL itself, as opposed to tools and build environments that are installed on a desktop. It is defined by a meta-package, voxl-suite_x.x.x.deb, that depends on all of the core ModalAI tools and services.

voxl-suite is bundled up with a matching [system image](/voxl-system-image/) to create what's called a [Platform Release](/platform-release/).


## Upgrade VOXL Suite Independently (Advanced)

It is highly recommended to stick to using [Platform Releases](/platform-release/) as-is since they are tested as a whole with System Image and VOXL Suite together. If you are an experienced developer and know what you are doing then you can follow the instructions on the [voxl-configure-pkg-manager page](/configure-pkg-manager/) to configure VOXL to pull packages from different voxl-suite release, but we will not support software issues that arise from this.


## CHANGELOG

### v0.9.5

This is a maintenance release for SDK-0.9

```
- More robust writing to pipes
- Allow mavlink 1 packets through libmodal_pipe and voxl-mavlink-server
- voxl-mavlink-server can auto-select between 2 uart ports on APQ8096
- fix 3D map bug in voxl-portal
```

| Package | Version | APQ8096 | QRB5165|
|:---|:---:|:---:|:---:|
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server) | 0.3.1 (unchanged) | ✅ |  |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server) | 1.0.3 (unchanged) | ✅ |  |
| [apq8096-libpng](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-libpng) | 1.6.38-1 (unchanged) | ✅ |  |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) | 0.1.3 (unchanged) | ✅ |  |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks) | 0.1.3 (unchanged) | ✅ |  |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite) | 2.8.3-1 (unchanged) | ✅ |  |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io) | 0.6.0 (unchanged) | ✅ |  |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv) | 0.2.3 (unchanged) | ✅ | ✅ |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure) | 0.0.7 (unchanged) | ✅ | ✅ |
| [libmodal-journal](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-journal) | 0.2.1 (unchanged) | ✅ | ✅ |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json) | 0.4.3 (unchanged) | ✅ | ✅ |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe) | 2.6.0 --> 2.8.2 | ✅ | ✅ |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) | 0.1.0 (unchanged) |  | ✅ |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math) | 1.3.0 (unchanged) | ✅ | ✅ |
| [libvoxl-cci-direct](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cci-direct) | 0.1.5 (unchanged) | ✅ | ✅ |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils) | 0.1.1 (unchanged) | ✅ | ✅ |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/voxl-sdk/utilities/mavlink-camera-manager) | 0.1.0 (unchanged) | ✅ |  |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server) | 0.1.0 (unchanged) |  | ✅ |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server) | 0.5.0 (unchanged) |  | ✅ |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks) | 0.1.5 (unchanged) |  | ✅ |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite) | 2.8.0-2 (unchanged) |  | ✅ |
| [voxl-bind](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-bind) | 0.0.1 (unchanged) |  | ✅ |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration) | 0.2.3 (unchanged) | ✅ | ✅ |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server) | 1.3.5 (unchanged) | ✅ | ✅ |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver) | 1.14.0-9 (unchanged) | ✅ | ✅ |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor) | 0.3.0 (unchanged) | ✅ | ✅ |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support) | 1.2.4 (unchanged) | ✅ | ✅ |
| [voxl-eigen3](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-eigen3) | 3.4.0 (unchanged) | ✅ | ✅ |
| [voxl-flir-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-flir-server) | 0.2.0 (unchanged) | ✅ | ✅ |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server) | 0.0.10 (unchanged) | ✅ | ✅ |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo) | 2.1.3-4 (unchanged) | ✅ | ✅ |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc) | 1.0.7 (unchanged) | ✅ | ✅ |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger) | 0.3.4 (unchanged) | ✅ | ✅ |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper) | 0.1.5 (unchanged) | ✅ | ✅ |
| [voxl-mavlink](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mavlink) | 0.1.0 (unchanged) | ✅ | ✅ |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) | 0.2.0 --> 0.3.0 | ✅ | ✅ |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) | 0.16.1 (unchanged) | ✅ | ✅ |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose) | 7.7.0-1 (unchanged) | ✅ | ✅ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools) | 0.7.6 (unchanged) | ✅ | ✅ |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) | 0.3.6 (unchanged) | ✅ | ✅ |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt) | 2.5.0-4 (unchanged) | ✅ | ✅ |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv) | 4.5.5-1 (unchanged) | ✅ | ✅ |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal) | 0.4.2 --> 0.5.0 | ✅ | ✅ |
| [voxl-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4) | 1.12.31 (unchanged) |  | ✅ |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server) | 0.1.2 (unchanged) |  | ✅ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server) | 0.8.2 (unchanged) | ✅ | ✅ |
| [voxl-remote-id](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-remote-id) | 0.0.5 (unchanged) |  | ✅ |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-streamer) | 0.4.1 (unchanged) | ✅ | ✅ |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server) | 0.3.1 (unchanged) | ✅ | ✅ |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils) | 1.2.2 (unchanged) | ✅ | ✅ |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server) | 0.1.3 (unchanged) | ✅ | ✅ |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-px4) | 1.4.0 (unchanged) | ✅ | ✅ |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox) | 1.1.3 (unchanged) | ✅ | ✅ |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn) | 0.0.6 (unchanged) | ✅ |  |


### v0.9.4

Requires VOXL (APQ8096) system image >= v4.0.0 and VOXL2/RB5F system image 1.4.1+

Major features:

```
IMU
 * Improve stability of ICM42688 driver
 * New Timestamp Filter module in librc_math
 * improved timestamps in qrb5165-imu-server
 * qrb5165-imu-server publishes live FFT of IMU data
 * enable AAF filter for ICM42688 accel on QRB5165
Portal
 * improve robustness un poor network conditions
 * Image framerate and quality reacts more quickly
 * new live FFT plot (qrb5165 only)
 * better CPU and IMU plots
Camera
 * PMD TOF support on both APQ8096 and QRB5165
Other
 * VIO level calibration
 * voxl-flir-server for Lepton 3 on both platforms
 * VIO packet includes cam_id field for feature points
 * point cloud packet includes ID and source fields
 * QRB5165 more robust pipe disconnection detection
 * voxl-mapper overhaul
 * QRB5165 support external UART flight controller in voxl-mavlink-server
 * VOA fallback mode when no VIO or attitude data is present
 * QVIO quality metric based on distribution of features
 * Preliminary (Beta) RemoteID support on QRB5165
```


| Package | Version | APQ8096 | QRB5165|
|:---|:---:|:---:|:---:|
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server) | 0.3.0 --> 0.3.1 | ✅ |  |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server) | 1.0.2 --> 1.0.3 | ✅ |  |
| [apq8096-libpng](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-libpng) | 1.6.38-1 (unchanged) | ✅ |  |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) | 0.1.2 --> 0.1.3 | ✅ |  |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks) | 0.1.2 --> 0.1.3 | ✅ |  |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite) | 2.2.3-4 --> 2.8.3-1 | ✅ |  |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io) | 0.5.8 --> 0.6.0 | ✅ |  |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv) | 0.1.0 --> 0.2.3 | ✅ | ✅ |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure) | 0.0.7 (unchanged) | ✅ | ✅ |
| [libmodal-journal](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-journal) | 0.2.1 (new) | ✅ | ✅ |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json) | 0.4.2 --> 0.4.3 | ✅ | ✅ |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe) | 2.4.0 --> 2.6.0 | ✅ | ✅ |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) | 0.1.0 (unchanged) |  | ✅ |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math) | 1.1.8 --> 1.3.0 | ✅ | ✅ |
| [libvoxl-cci-direct](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cci-direct) | 0.1.5 (new) | ✅ | ✅ |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils) | 0.1.1 (unchanged) | ✅ | ✅ |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/voxl-sdk/utilities/mavlink-camera-manager) | 0.1.0 (unchanged) | ✅ |  |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server) | 0.0.5 --> 0.1.0 |  | ✅ |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server) | 0.2.4 --> 0.5.0 |  | ✅ |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks) | 0.1.3 --> 0.1.5 |  | ✅ |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite) | 2.8.0-2 (unchanged) |  | ✅ |
| [voxl-bind](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-bind) | 0.0.1 (unchanged) |  | ✅ |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration) | 0.2.2 --> 0.2.3 | ✅ | ✅ |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server) | 1.1.0 --> 1.3.5 | ✅ | ✅ |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver) | 1.14.0-7 --> 1.14.0-9 | ✅ | ✅ |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor) | 0.2.6 --> 0.3.0 | ✅ | ✅ |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support) | 1.2.4 (unchanged) | ✅ | ✅ |
| [voxl-eigen3](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-eigen3) | 3.4.0 (new) | ✅ | ✅ |
| [voxl-flir-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-flir-server) | 0.0.4 --> 0.2.0 | ✅ | ✅ |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server) | 0.0.10 (unchanged) | ✅ | ✅ |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo) | 2.1.3-4 (unchanged) | ✅ | ✅ |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc) | 1.0.6 --> 1.0.7 | ✅ | ✅ |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger) | 0.3.2 --> 0.3.4 | ✅ | ✅ |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper) | 0.0.7 --> 0.1.5 | ✅ | ✅ |
| [voxl-mavlink](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mavlink) | 0.1.0 (unchanged) | ✅ | ✅ |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) | 0.1.3 --> 0.2.0 | ✅ | ✅ |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) | 0.15.2 --> 0.16.1 | ✅ | ✅ |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose) | 7.6.0 --> 7.7.0-1 | ✅ | ✅ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools) | 0.7.2 --> 0.7.6 | ✅ | ✅ |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) | 0.3.3 --> 0.3.6 | ✅ | ✅ |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt) | 2.5.0-4 (unchanged) | ✅ | ✅ |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv) | 4.5.5-1 (unchanged) | ✅ | ✅ |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal) | 0.2.8 --> 0.4.2 | ✅ | ✅ |
| [voxl-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4) | 1.4.16 --> 1.12.31 |  | ✅ |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server) | 0.1.2 (unchanged) |  | ✅ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server) | 0.7.1 --> 0.8.2 | ✅ | ✅ |
| [voxl-remote-id](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-remote-id) | 0.0.5 (new) |  | ✅ |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-streamer) | 0.3.6 --> 0.4.1 | ✅ | ✅ |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server) | 0.2.7 --> 0.3.1 | ✅ | ✅ |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils) | 1.1.4 --> 1.2.2 | ✅ | ✅ |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server) | 0.1.1 --> 0.1.3 | ✅ | ✅ |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-px4) | 1.2.0 --> 1.4.0 | ✅ | ✅ |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox) | 1.0.4 --> 1.1.3 | ✅ | ✅ |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn) | 0.0.6 (unchanged) | ✅ |  |



### v0.8

Requires VOXL system image 3.8+ or VOXL2/RB5F system image 1.3.1+

This VOXL Suite is BETA only for VOXL 1 since it includes camera server updates allowing improved support for most cameras but no PMD TOF functionality

```
* IMPROVEMENT:       apq8096 stereo cameras no longer have a high-pass filter and look much cleaner
* IMPROVEMENT:       libmodal-cv beginning to support CVP functionality on QRB5
* IMPROVEMENT:       libmodal-pipe support for setting process/thread priority
* IMPROVEMENT:       libmodal-pipe/qvio/vvpx4 updated vio struct to match latest mavlink
* IMPROVEMENT:       voxl-camera-calibration command line options for more modular use
* IMPROVEMENT:       voxl-camera-server support for ov9782 cameras
* IMPROVEMENT:       voxl-logger major efficiency rework
* NEW PACKAGE:       voxl-bind (qrb5165-only)
* RENAME PACKAGE:    qrb5165-camera-server > voxl-camera-server (now cross-platform)
```

| Package                                                                                                   |        Version        | APQ8096 | QRB5165 |
|:----------------------------------------------------------------------------------------------------------|:---------------------:|:-------:|:-------:|
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server)                 |    0.2.6 --> 0.3.0    |    ✅    |         |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server)                 |    1.0.1 --> 1.0.2    |    ✅    |         |
| [apq8096-libpng](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-libpng)                      |    1.6.38-1 (new)     |    ✅    |         |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) |    0.1.0 --> 0.1.2    |    ✅    |         |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks)          |   0.1.2 (unchanged)   |    ✅    |         |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite)                      |  2.2.3-4 (unchanged)  |    ✅    |         |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io)                          |   0.5.8 (unchanged)   |    ✅    |         |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv)                              |    0.0.5 --> 0.1.0    |    ✅    |    ✅    |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure)                  |   0.0.7 (unchanged)   |    ✅    |    ✅    |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json)                          |   0.4.2 (unchanged)   |    ✅    |    ✅    |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe)                          |    2.1.7 --> 2.4.0    |    ✅    |    ✅    |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io)                          |   0.1.0 (unchanged)   |         |    ✅    |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math)                                |   1.1.8 (unchanged)   |    ✅    |    ✅    |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils)                        |   0.1.1 (unchanged)   |    ✅    |    ✅    |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/voxl-sdk/utilities/mavlink-camera-manager)        |    0.0.6 --> 0.1.0    |    ✅    |         |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server)                 |    0.0.4 --> 0.0.5    |         |    ✅    |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server)                 |    0.2.0 --> 0.2.4    |         |    ✅    |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks)          |    0.1.2 --> 0.1.3    |         |    ✅    |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite)                      |  2.8.0-2 (unchanged)  |         |    ✅    |
| [voxl-bind](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-bind)                                  |      0.0.1 (new)      |         |    ✅    |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration)      |    0.1.5 --> 0.2.2    |    ✅    |    ✅    |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server)                 |      1.1.0 (new)      |    ✅    |    ✅    |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver)                | 1.14.0-5 --> 1.14.0-7 |    ✅    |    ✅    |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor)                     |    0.2.5 --> 0.2.6    |    ✅    |    ✅    |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support)              |   1.2.4 (unchanged)   |    ✅    |    ✅    |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server)               |  0.0.10 (unchanged)   |    ✅    |         |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo)                    |  2.1.3-4 (unchanged)  |    ✅    |    ✅    |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2)                    |   0.0.4 (unchanged)   |    ✅    |    ✅    |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc)                            |   1.0.6 (unchanged)   |    ✅    |    ✅    |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger)                              |    0.1.2 --> 0.3.2    |    ✅    |    ✅    |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper)                               |    0.0.6 --> 0.0.7    |    ✅    |    ✅    |
| [voxl-mavlink](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mavlink)                          |    0.0.7 --> 0.1.0    |    ✅    |    ✅    |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server)               |    0.1.1 --> 0.1.3    |    ✅    |    ✅    |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem)                                |   0.15.1 --> 0.15.2   |    ✅    |    ✅    |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose)                        |   7.6.0 (unchanged)   |    ✅    |    ✅    |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools)                        |    0.6.3 --> 0.7.2    |    ✅    |    ✅    |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros)                      |    0.3.2 --> 0.3.3    |    ✅    |    ✅    |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt)                              |  2.5.0-4 (unchanged)  |    ✅    |    ✅    |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv)                            |  4.5.5-1 (unchanged)  |    ✅    |    ✅    |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal)                               |    0.2.6 --> 0.2.8    |    ✅    |    ✅    |
| [voxl-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4)                                     |     1.4.16 (new)      |         |    ✅    |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server)               |    0.1.1 --> 0.1.2    |         |    ✅    |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server)                     |    0.4.6 --> 0.7.1    |    ✅    |    ✅    |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-streamer)                          |    0.3.1 --> 0.3.6    |    ✅    |    ✅    |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector)                   |   0.0.4 (unchanged)   |    ✅    |    ✅    |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server)                 |    0.2.3 --> 0.2.7    |    ✅    |    ✅    |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils)                                |    1.1.1 --> 1.1.4    |    ✅    |    ✅    |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server)                       |   0.1.1 (unchanged)   |    ✅    |    ✅    |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-px4)                       |    1.0.5 --> 1.2.0    |    ✅    |    ✅    |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox)                          |    1.0.3 --> 1.0.4    |    ✅    |    ✅    |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn)                                    |   0.0.6 (unchanged)   |    ✅    |         |

### v0.7.0

Requires VOXL system image 3.8+ or VOXL2 system image 1.2.1+

This is the first SDK release with support for both VOXL (APQ8096) and VOXL2 (QRB5165). Every package has been updated with dual-platform support (where applicable) and the entire SDK is now built via CI.

Starting with V0.7.0, the new SDK repository is hosted at [http://voxl-packages.modalai.com/dists/](http://voxl-packages.modalai.com/dists/).

```
* RENAME PACKAGE:    voxl-camera-server > apq8096-camera-server (apq8096 only)
* RENAME PACKAGE:    voxl-imu-server > apq8096-imu-server (apq8096 only)
* RENAME PACKAGE:    libvoxl_io > libapq8096-io (apq8096 only)
* NEW PACKAGE:       qrb5165-camera-server (qrb5165-only)
* NEW PACKAGE:       qrb5165-imu-server (qrb5165-only)
* NEW PACKAGE:       qrb5165-dfs-server (qrb5165-only)
* NEW PACKAGE:       libmodal-cv
* NEW PACKAGE:       voxl-mavlink-server
* NEW PACKAGE:       voxl-px4-imu-server (qrb5165 only)
* NEW PACKAGE:       libqrb5165-io (qrb5165-only)
* NEW PACKAGE:       qrb5165-tflite (qrb5165-only)
* NEW PACKAGE:       voxl-px4 (qrb5165-only)
* IMPROVEMENT:       voxl-camera-calibration accuracy and usability
* IMPROVEMENT:       voxl-vision-px4 talks to PX4 through voxl-mavlink-server
* IMPROVEMENT:       voxl-portal: dynamic image quality, point cloud viewer
* CLEANUP:           Remove old wizard from voxl-configure-mpa
* NEW TOOL:          voxl-configure-pkg-manager
```

| Package | Version | APQ8096 | QRB5165 |
| --- | --- | --- | --- |
| [apq8096-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-camera-server) | 0.9.0 (new) | ✅ |  |
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server) | 0.2.6 (new) | ✅ |  |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server) | 1.0.1 (new) | ✅ |  |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) | 0.1.0 (new) | ✅ |  |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks) | 0.1.2 (new) | ✅ |  |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite) | 2.2.3-4 (new) | ✅ |  |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io) | 0.5.8 (new) | ✅ |  |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv) | 0.0.5 (new) | ✅ | ✅ |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure) | 0.0.7 (new) | ✅ | ✅ |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json) | 0.4.2 (new) | ✅ | ✅ |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe) | 2.1.7 (new) | ✅ | ✅ |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) | 0.1.0 (new) |  | ✅ |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math) | 1.1.8 (new) | ✅ | ✅ |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils) | 0.1.1 (new) | ✅ | ✅ |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/voxl-sdk/utilities/mavlink-camera-manager) | 0.0.2 --> 0.0.6 | ✅ |  |
| [qrb5165-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-camera-server) | 0.2.0 (new) |  | ✅ |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server) | 0.0.4 (new) |  | ✅ |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server) | 0.2.0 (new) |  | ✅ |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks) | 0.1.2 (new) |  | ✅ |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite) | 2.8.0-2 (new) |  | ✅ |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration) | 0.1.1 --> 0.1.5 | ✅ | ✅ |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver) | 1.14.0-5 (new) | ✅ | ✅ |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor) | 0.2.0 --> 0.2.5 | ✅ | ✅ |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support) | 1.1.3 --> 1.2.4 | ✅ | ✅ |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server) | 0.0.10 (new) | ✅ |  |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo) | 2.1.3-4 (new) | ✅ | ✅ |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2) | 0.0.4 (new) | ✅ | ✅ |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc) | 1.0.6 (new) | ✅ | ✅ |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger) | 0.1.2 (new) | ✅ | ✅ |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper) | 0.0.6 (new) | ✅ | ✅ |
| [voxl-mavlink](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mavlink) | 0.0.2 --> 0.0.7 | ✅ | ✅ |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) | 0.1.1 (new) | ✅ | ✅ |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) | 0.12.0 --> 0.15.1 | ✅ | ✅ |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose) | 7.6.0 (new) | ✅ | ✅ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools) | 0.3.6 --> 0.6.3 | ✅ | ✅ |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) | 0.3.2 (new) | ✅ | ✅ |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt) | 2.5.0-4 (new) | ✅ | ✅ |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv) | 4.5.5-1 (new) | ✅ | ✅ |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal) | 0.1.2 --> 0.2.6 | ✅ | ✅ |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server) | 0.1.1 (new) |  | ✅ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server) | 0.3.4 --> 0.4.6 | ✅ | ✅ |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-streamer) | 0.2.6 --> 0.3.1 | ✅ | ✅ |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector) | 0.0.2 --> 0.0.4 | ✅ | ✅ |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server) | 0.1.5 --> 0.2.3 | ✅ | ✅ |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils) | 0.8.5 --> 1.1.1 | ✅ | ✅ |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server) | 0.1.1 (new) | ✅ | ✅ |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-px4) | 0.9.5 --> 1.0.4 | ✅ | ✅ |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox) | 1.0.3 (new) | ✅ | ✅ |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn) | 0.0.3 --> 0.0.6 | ✅ |  |

### v0.5.0

Requires VOXL system image 3.3+

```
* NEW TOOL:    voxl-calibrate-cameras utility
* NEW TOOL:    imu thermal calibration
* NEW TOOL:    voxl-configure-opkg
* NEW TOOL:    voxl-calibrate-px4-horizon tool
* NEW TOOL:    voxl-portal web interface
* NEW TOOL:    voxl-wait-for-fs service
* NEW TOOL:    voxl-list-pipes
* ADDITION:    libmodal_pipe pause-resume function
* ADDITION:    voxl-configure extrinsics add support for Starling
* ADDITION:    voxl-mpa-to-ROS dynamic pipe detection (no more config!!)
* IMPROVEMENT: file system syncs more frequently
* IMPROVEMENT: voxl-qvio-reset is more complete
* IMPROVEMENT: voxl-configure cameras wizard tweaks
* IMPROVEMENT: voxl-configure-mpa support new part numbers
* IMPROVEMENT: voxl-inspect-services speedup
* IMPROVEMENT: too many little tweaks and fixes to list
```

| Package                                                                                         | Package Version |
|---                                                                                              |---              |
| [libjpeg_turbo](https://gitlab.com/voxl-public/third-party/voxl-jpeg-turbo)                     | 9.0.4+   |
| [libmodal_exposure](https://gitlab.com/voxl-public/core-libs/libmodal_exposure)                 | 0.0.2+   |
| [libmodal_json](https://gitlab.com/voxl-public/core-libs/libmodal_json)                         | 0.3.6+   |
| [libmodal_pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe)           | 2.1.1+   |
| [librc_math](https://gitlab.com/voxl-public/core-libs/librc_math)                               | 1.1.5+   |
| [libvoxl_cutils](https://gitlab.com/voxl-public/core-libs/libvoxl_cutils)                       | 0.0.2+   |
| [libvoxl_io](https://gitlab.com/voxl-public/core-libs/libvoxl_io)                               | 0.5.4+   |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/utilities/mavlink-camera-manager)       | 0.0.2+   |
| [mongoose](https://gitlab.com/voxl-public/third-party/voxl-mongoose)                            | 7.3.0+   |
| [opencv](https://gitlab.com/voxl-public/core-libs/voxl-opencv)                                  | 4.5.2-2+ |
| [openmp](https://gitlab.com/voxl-public/other/voxl-openmp)                                      | 10.0.2+  |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-calibration) | 0.1.1+   |
| [voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server) | 0.7.1+   |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor)     | 0.2.0+   |
| [voxl-dfs-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server)       | 0.2.2+   |
| [voxl-docker-support](https://gitlab.com/voxl-public/utilities/voxl-docker-support)             | 1.1.3+   |
| [voxl-gphoto2](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-gphoto2)             | 0.0.5+   |
| [voxl-imu-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server)       | 0.9.1+   |
| [voxl-mavlink](https://gitlab.com/voxl-public/third-party/voxl-mavlink)                         | 0.12.0+  |
| [voxl-modem](https://gitlab.com/voxl-public/utilities/voxl-modem)                               | 0.0.2+   |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools)         | 0.3.6+   |
| [voxl-nodes](https://gitlab.com/voxl-public/ros/voxl_mpa_to_ros)                                | 0.2.0+   |
| [voxl-portal](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-portal)               | 0.1.1+   |
| [voxl-qvio-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server)     | 0.3.4+   |
| [voxl-streamer](https://gitlab.com/voxl-public/utilities/voxl-streamer)                         | 0.2.6+   |
| [voxl-tag-detector](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tag-detector)   | 0.0.2+   |
| [voxl-tflite](https://gitlab.com/voxl-public/third-party/voxl-tflite)                           | 2.2.3+   |
| [voxl-tflite-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tflite-server) | 0.1.5+   |
| [voxl-utils](https://gitlab.com/voxl-public/utilities/voxl-utils)                               | 0.8.4+   |
| [voxl-vision-px4](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-vision-px4)       | 0.9.5+   |
| [voxl-vpn](https://gitlab.com/voxl-public/utilities/voxl-vpn)                                   | 0.0.3+   |



### v0.4.6

Requires System Image 3.3+

Contains the following packages:

| Package                                                                                         | Package Version |
|---                                                                                              |---              |
| [voxl-utils](https://gitlab.com/voxl-public/utilities/voxl-utils)                               | 0.7.1+   |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools)         | 0.2.6+   |
| [libmodal_exposure](https://gitlab.com/voxl-public/core-libs/libmodal_exposure)                 | 0.0.2+   |
| [libmodal_json](https://gitlab.com/voxl-public/core-libs/libmodal_json)                         | 0.3.5+   |
| [libmodal_pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe)           | 2.0.7+   |
| [librc_math](https://gitlab.com/voxl-public/core-libs/librc_math)                               | 1.1.5+   |
| [libvoxl_cutils](https://gitlab.com/voxl-public/core-libs/libvoxl_cutils)                       | 0.0.2+   |
| [libvoxl_io](https://gitlab.com/voxl-public/core-libs/libvoxl_io)                               | 0.5.4+   |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/utilities/mavlink-camera-manager)       | 0.0.2+   |
| [opencv](https://gitlab.com/voxl-public/core-libs/voxl-opencv)                                  | 4.5.2-1+ |
| [openmp](https://gitlab.com/voxl-public/other/voxl-openmp)                                      | 10.0.2+  |
| [voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server) | 0.7.1+   |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor)     | 0.1.7+   |
| [voxl-dfs-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server)       | 0.2.0+   |
| [voxl-docker-support](https://gitlab.com/voxl-public/utilities/voxl-docker-support)             | 1.1.3+   |
| [voxl-gphoto2](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-gphoto2)             | 0.0.5+   |
| [voxl-imu-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server)       | 0.8.1+   |
| [voxl-modem](https://gitlab.com/voxl-public/utilities/voxl-modem)                               | 0.12.0+  |
| [voxl-nodes](https://gitlab.com/voxl-public/ros/voxl-nodes)                                     | 0.1.6+   |
| [voxl-qvio-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server)     | 0.3.1+   |
| [voxl-streamer](https://gitlab.com/voxl-public/utilities/voxl-streamer)                         | 0.2.3+   |
| voxl-tflite(no repo)                                                                            | 0.0.1+   |
| [voxl-tflite-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tflite-server) | 0.1.0+   |
| [voxl-vision-px4](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-vision-px4)       | 0.9.2+   |
| [voxl-vpn](https://gitlab.com/voxl-public/utilities/voxl-vpn)                                   | 0.0.3+   |


### v0.3.4

Requires System Image 3.2+

Contains the following packages:

| Package                                                                                   | Package Version |
|---                                                                                        |---              |
| [voxl-utils](https://gitlab.com/voxl-public/utilities/voxl-utils)                         | 0.6.0+    |
| [libmodal_json](https://gitlab.com/voxl-public/core-libs/libmodal_json)                   | 0.3.4+    |
| [libmodal_pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe)     | 1.7.8+    |
| [librc_math](https://gitlab.com/voxl-public/core-libs/librc_math)                         | 1.1.4+    |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/utilities/mavlink-camera-manager) | 0.0.1+    |
| [opencv](https://gitlab.com/voxl-public/core-libs/voxl-opencv)                            | 4.5.1+    |
| [openmp](https://gitlab.com/voxl-public/other/voxl-openmp)                                | 10.0.1+   |
| [voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server) | 0.5.6+ |
| [voxl-dfs-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server) | 0.0.7+    |
| [voxl-docker-support](https://gitlab.com/voxl-public/utilities/voxl-docker-support)       | 1.1.1+ |
| [voxl-gphoto2](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-gphoto2)       | 0.0.5+ |
| [voxl-hal3-tof-cam-ros](https://gitlab.com/voxl-public/ros/voxl-hal3-tof-cam-ros)         | 0.0.5+ |
| [voxl-imu-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server) | 0.7.8+ |
| [voxl-modem](https://gitlab.com/voxl-public/utilities/voxl-modem)                         | 0.11.0+ |
| [voxl-mpa-tflite-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tflite-server) | 0.0.2+ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools)   | 0.1.6+ |
| [voxl-nodes](https://gitlab.com/voxl-public/ros/voxl-nodes)                               | 0.1.0+ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server) | 0.2.1+ |
| [voxl-rtsp](https://gitlab.com/voxl-public/utilities/voxl-rtsp)                           | 1.0.3+ |
| [voxl-streamer](https://gitlab.com/voxl-public/utilities/voxl-streamer)                   | 0.2.1+ |
| [voxl-vision-px4](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-vision-px4) | 0.8.1+ |
| [voxl-vpn](https://gitlab.com/voxl-public/utilities/voxl-vpn)                             | 0.0.3+ |


### v0.2.0

Changes:

- Updated `voxl-vision-px4`, PX4 disconnect detection, better handling of 'CRC errors', UART port and baud configurable, keep publishing data if VIO is off, wizard improvements
- Updated `voxl-modem`, added LTE v2 support
- Updated `voxl-utils`, added `voxl-backup` util for camera calibration backup
- Added new `voxl-vpn` package
- Added new `voxl-time-sync` package
- Removed ffmpeg as it's normally used via Docker containers

Contains the following packages:

| Package                                                                        | Package Version |
|---                                                                             |---              |
| docker                                                                         | 1.9.0  |
| imu_app                                                                        | 0.0.6  |
| libmodal_pipe                                                                  | 1.2.2  |
| librc_math                                                                     | 1.1.2  |
| modalai-vl                                                                     | 0.1.3  |
| [libvoxl_io](https://gitlab.com/voxl-public/libvoxl_io)                        | 0.5.2  |
| [voxl-cam-manager](https://gitlab.com/voxl-public/voxl-cam-manager)            | 0.2.2  |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-docker-support)      | 1.1.1  |
| [voxl-hal3-tof-cam-ros](https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros)  | 0.0.2  |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-modem)                        | 0.10.0 |
| [voxl-nodes](https://gitlab.com/voxl-public/voxl-nodes)                        | 0.0.8  |
| [voxl-rtsp](https://gitlab.com/voxl-public/voxl-rtsp)                          | 1.0.2  |
| [voxl-time-sync](https://gitlab.com/voxl-public/voxl-time-sync)                | 0.0.1  |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-utils)                        | 0.5.2  |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-vision-px4)              | 0.6.8  |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-vpn)                            | 0.0.2  |
| voxl_imu                                                                       | 0.0.4  |


Tested against:

- System Image 2.5.2-1.0.1
- System Image 2.3.0-1.0.1


### v0.1.1

Contains the following packages:

| Package                                                                        | Package Version (min) |
|---                                                                             |---                    |
| docker                                                                         | 1.9.0 |
| [ffmpeg](https://gitlab.com/voxl-public/voxl-ffmpeg)                           | 4.2.2 |
| imu_app                                                                        | 0.0.6 |
| [librc_math](https://gitlab.com/voxl-public/librc_math)                        | 1.1.2 |
| [libvoxl_io](https://gitlab.com/voxl-public/libvoxl_io)                        | 0.4.1 |
| [libvoxl_pipe](https://gitlab.com/voxl-public/libvoxl_pipe)                    | 1.0.1 |
| modalai-vl                                                                     | 0.1.3 |
| [voxl-cam-manager](https://gitlab.com/voxl-public/voxl-cam-manager)            | 0.2.2 |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-docker-support)      | 1.1.1 |
| [voxl-hal3-tof-cam-ros](https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros)  | 0.0.2 |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-modem)                        | 0.9.3 |
| [voxl-nodes](https://gitlab.com/voxl-public/voxl-nodes)                        | 0.0.8 |
| [voxl-rtsp](https://gitlab.com/voxl-public/voxl-rtsp)                          | 1.0.2 |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-utils)                        | 0.5.1 |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-vision-px4)              | 0.6.1 |
| voxl_imu                                                                       | 0.0.4 |


Tested against: System Image 2.3.0-1.0.1
