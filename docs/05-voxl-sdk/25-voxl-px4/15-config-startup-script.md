---
title: Configuration and Startup Script
layout: default
parent: VOXL PX4
has_children: true
nav_order: 15
permalink: /voxl-px4-config-and-startup/
---

# PX4 startup on VOXL2

PX4 is auto-started via the systemd service using the file `etc\systemd\system\voxl-px4.service`
This service file relies on the `/usr/bin/voxl-px4` convenience script to start PX4
in daemon mode. The voxl-px4 script is a simple bash script.

## voxl-px4

The voxl-px4 convenience script performs a few simple tasks:
- Checks for the existence of the configuration file in `/etc/modalai/voxl-px4.conf`. If the configuration file exists it will be sourced to set the values of various important
environment variables that control the startup process
- Checks for the existence of the signature file that is required to run any custom
software on the sensors DSP. If it does not exist it is created.
- Parse any command line options. These options can be used to overwrite any environment
variable values set by the configuration file (if one existed).
- Check for existence of a parameters file in `/data/px4/param`. If one does not
exist it will create one with the parameters listed in `/etc/modalai/voxl-px4-set-default-parameters.config`
- Finally it will start the px4 and pass it the option to run the startup script located
at `/usr/bin/voxl-px4-start`. The voxl-px4-start startup script is also a simple bash script.
- As the voxl-px4 convenience script runs it will print out useful information

## Creating the configuration file

The `voxl-configure-px4` script will create the configuration file at `/etc/modalai/voxl-px4.conf`.

## Auto start and daemon mode

By default PX4 will be auto-started and will run as a system daemon. For development
purposes it is often more appropriate to start PX4 manually from the command line.
To disable the auto-start function use `systemctl disable voxl-px4` and then reboot
the system. Once the system has restarted you can manually start PX4 by running `voxl-px4`.
Be aware that the configuration file may be set up to run PX4 in daemon mode so even
though it was started manually you will not be able to interact with the PX4 shell. In order to not start in daemon mode the configuration script can be updated.

If voxl-px4 was auto-started and you want to see all of the normal console messages
you can use this command: `journalctl -b -u voxl-px4`

## voxl-px4-start startup script

The startup script `/usr/bin/voxl-px4-start` is the most important part of PX4 startup process.
This script is responsible for starting all drivers and modules on both the applications
processor and the DSP. It is important to note that the `qshell` driver is needed
to interact with the PX4 code running on the DSP. All drivers and modules started
on the DSP must be done via `qshell`.

## Interacting with PX4 after startup

If PX4 has been started manually and is not in daemon mode then you can interact
directly with the PX4 shell. If it has been auto-started or is in daemon mode then
you cannot directly interact with the PX4 shell. However, many of the commands are
still available from an adb shell or ssh session terminal prompt. Simply prefix the
desired command with `px4-` and it will interact with the running PX4 process. A very
common use case is to run `px4-listener` from a different shell to see the current
output of a desired topic. For example `px4-listener sensor_gps` will give you the
current value of the sensor_gps topic if it is being currently published by the GPS
device driver.