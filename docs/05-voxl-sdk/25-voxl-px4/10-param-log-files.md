---
title: Param Files and Log Files
layout: default
parent: VOXL PX4
has_children: true
nav_order: 10
permalink: /voxl-px4-files/
---

# Parameters

- The full PX4 parameter guide can be found [here](https://docs.px4.io/main/en/advanced_config/parameter_reference.html)

The default value for all parameters is built into the software image. Any parameter 
values that are changed from their default values are stored in a parameter file in
the file system. The default location for this file is `/data/px4/param/parameters`.
In order to get back to the default setting for all parameters one can delete
this file.

## ModalAI Parameter Sets

ModalAI provides sets of parameters that can be used for the various reference drones
and for various use cases. These parameter sets are stored in a format that allows
them to be loaded onto the drone with QGroundControl using the `load from file` option.
These parameter sets are available [here](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-px4-params)

If PX4 is started via the `/usr/bin/voxl-px4` script and the parameters file does not exist
it will create one populated with the parameters listed in the file `/etc/modalai/voxl-px4-set-default-parameters.config`

## Viewing/Editing Parameters

You can use the param subsystem to interact with the parameters, e.g. viewing:

```
px4-param show
```

![voxl-param-show.gif](/images/voxl-sdk/voxl-px4/px4-param-show.gif)

# Log files

PX4 log files can be a valuable way to tune performance of your drone or debug
problems. The PX4 logger module is responsible for collecting the information and
storing it into the log file. This module is started in the standard startup script
with some standard options. The normal approach is to start a log file once the drone
has been armed and close it once the drone has been disarmed. The log files are stored
in the file system at `/data/px4/log`.

In some cases it is desirable to start the logs immediately upon the start of PX4.
To enable this add the `-e` option to the logger start line. Care must be used with
this option because if power is removed from the drone (e.g. at the end of the flight)
then it is possible to corrupt the log file and make it unusable. Instead it is advised
to log into the drone via adb or ssh and issue the command `px4-logger stop` to cause
the logger to finish the log file and close it safely before removing power.


