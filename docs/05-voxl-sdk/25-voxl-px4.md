---
title: VOXL PX4 
layout: default
parent: VOXL SDK
has_children: true
nav_order: 25
permalink: /voxl-px4/
---

# VOXL PX4

The `voxl-px4` package provides the infrastructure to install and run the PX4 flight controller as a Linux service on QRB5165 based platforms (VOXL2, VOXL2 mini, and RB5 Flight).

![voxl-px4](/images/voxl-sdk/voxl-px4/voxl-px4.jpg)

## Overview

The `voxl-px4` project is essentially a wrapper for the QRB5165 based build of
the [PX4 Flight Controller](https://px4.io/). The PX4 flight controller and other
related components are open source projects and have extensive online documentation
to describe what they are, how to use them, and how to develop with them. It is
recommended to start with that documentation to build an understanding of PX4 and
how it is generally used. This documentation focuses more on how the QRB5165 based
implementation of PX4 is unique and the aspects of it's use and/or development that
differ from the standard use cases. Furthermore, this documentation is specific
to the VOXL SDK 1.0.0 release.

Please refer to the following sections for more details. It is recommended to start
with the system architecture section to get an idea of how the QRB5165 based build of
PX4 differs from other implementations.
