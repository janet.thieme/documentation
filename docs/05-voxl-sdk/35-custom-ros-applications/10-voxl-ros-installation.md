---
title: ROS Installation VOXL 1
layout: default
parent: Custom ROS Applications
nav_order: 10
permalink: /ros-installation-voxl/
---


# ROS Installation on VOXL 1

ROS must be installed on both the Host computer and on VOXL. This page guides you through this process.


## 1. Set up ROS on Host PC

### Install

You can use whichever distribution of ROS 1 on your host PC (i.e. ROS Melodic on Ubuntu 18.04). Follow the ROS wiki instructions to install `ros-melodic-desktop-full`: <http://wiki.ros.org/melodic/Installation/Ubuntu>

### Environment Setup Script

We will set up a `my_ros_env.sh` script on the host pc:

```bash
me@mylaptop:~/$ touch ~/my_ros_env.sh
me@mylaptop:~/$ echo ". ~/my_ros_env.sh" >> ~/.bashrc
```

Then paste the following into your `my_ros_env.sh` script. This is where you will edit IP addresses as necessary.

``` bash
#!/bin/bash

# Script loads ROS envrionment variables and sets IP addresses

# load main ros environment
if [ -f /opt/ros/melodic/setup.bash ]; then
    source /opt/ros/melodic/setup.bash
elif [ -f /opt/ros/kinetic/setup.bash ]; then
    source /opt/ros/kinetic/setup.bash
elif [ -f /opt/ros/indiego/setup.bash ]; then
    source /opt/ros/indigo/setup.bash
fi

# if a catkin workspace is setup then make sure the launch
# files and run files are available in the ROS PATH
if [ -f ~/catkin_ws/devel/setup.bash ]; then
    source ~/catkin_ws/devel/setup.bash
fi
if [ -f ~/catkin_ws/install/setup.bash ]; then
    source ~/catkin_ws/install/setup.bash
fi

# ignore ROS_HOSTNAME, it only causes problems and overrides ROS_IP
unset ROS_HOSTNAME

# configure ROS IPs HERE
export ROS_MASTER_IP=192.168.8.1
export ROS_IP=192.168.8.91

# ROS_MASTER_URI is derived from ROS_MASTER_IP
export ROS_MASTER_URI=http://${ROS_MASTER_IP}:11311/

## bonus alias to ssh into ros master
alias sshh='sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${ROS_MASTER_IP}'
```

The script above provides an alias 'sshh' which allows you to ssh remotely into your VOXL using default credentials. Update `ROS_MASTER_IP` and `ROS_IP` to match your network configuration. On your host machine, also be sure to update `ROS_MASTER_IP` and `ROS_IP` to match this VOXL IP address.

You will need to install sshpass to use the aforementioned alias.

```bash
# ON HOST PC
me@mylaptop:~/$ sudo apt install sshpass
```

### Troubleshooting

`Note:` Due to SSH environment variable issues using the default terminal, we recommend that users with host Ubuntu machine versions newer than Ubuntu 14.04 use XTerm (not the default terminal) to connect to VOXL via SSH while using ROS.



## 2. Set up ROS on VOXL

### Set up Environment Variables

VOXL ships with ROS Indigo and the `voxl-utils` package pre-installed. `voxl-utils` installs the script `my_ros_env.sh` to `/home/root/` which sources the normal `/opt/ros/indigo/setup.bash` script to configure the ROS environment variables. It also sets the IP address environment variables. Since everyone's VOXL will be on a different IP address, you will need to edit this script to match the network configuration for your desired ROS master/slave configuration which will be different depending on your use case.

We recommend having the ROS Master be on VOXL. The Master IP is already set up to `192.168.8.1` to match the VOXL's default IP address when in SoftAP mode. If you have your VOXL in station mode and connected on a WiFi network you will need to change this address to match your configuration.

```bash
voxl2:/$ vi ~/my_ros_env.sh

...

# IP Environment variables must be set after setup scripts
# Set ROS_IP & ROS_MASTER_URI appropriately for your configuration
# 192.168.8.1 is default for the robot in soft access point mode
export ROS_MASTER_URI=http://localhost:11311/
export ROS_IP=192.168.8.1
unset ROS_HOSTNAME
```

You can refresh your environment variables after updating this script by running:

```bash
voxl2:/$ exec bash
```

And then check that they have been updated by using the `voxl-env` tool or just grep'ing the bash environment variables.

```bash
voxl2:/$ voxl-env show
ROS_MASTER_URI=http://localhost:11311/
ROS_IP=192.168.1.150
CAM_CONFIG_ID=1
HIRES_CAM_ID=-1
TRACKING_CAM_ID=0
STEREO_CAM_ID=1
TOF_CAM_ID=-1

voxl2:/$ env | grep "ROS"
ROS_ROOT=/opt/ros/indigo/share/ros
ROS_PACKAGE_PATH=/home/root/catkin_ws/install/share:/home/root/catkin_ws/install/stacks:/opt/ros/indigo/share:/opt/ros/indigo/stacks
ROS_MASTER_URI=http://localhost:11311/
ROSLISP_PACKAGE_DIRECTORIES=
ROS_DISTRO=indigo
ROS_IP=192.168.8.1
ROS_ETC_DIR=/opt/ros/indigo/etc/ros
```


### Testing ROS Configuration

To test that your ROS configuration is working, start `roscore` on VOXL. 

Note: Currently an [issue](https://gitlab.com/voxl-public/documentation/issues/4) can be seen when attempting to use ROS using the native Ubuntu terminal. As a workaround XTerm can be used.

```bash
# ON VOXL
voxl2:/$ echo $SHELL
/bin/bash
voxl2:/$ roscore

process[rosout-1]: started with pid [5706]
started core service [/rosout]

```

Now on your host PC use `rostopic` to see if it can communicate with VOXL.

```bash
# ON HOST PC
me@mylaptop:~$  rostopic list
/rosout
/rosout_agg
```

### Known Issue Running ROS Through SSH

If you try to use ROS commands while SSH'd to VOXL and see errors such as this:

```bash
load_parameters: unable to set parameters (last param was [/rosdistro=indigo
]): <Fault 1: "<class 'xml.parsers.expat.ExpatError'>:not well-formed (invalid token): line 13, column 15">
```

The cause is a known issue with the GNOME Terminal, the default terminal emulator of Ubuntu. The solution is to use another terminal emulator such as XTerm or Konsole instead of GNOME Terminal.

More information here: https://gitlab.com/voxl-public/documentation/issues/4


### Next Steps

Now explore the [VOXL MPA to ROS](/voxl-mpa-to-ros/) package.

See the [building ROS nodes for VOXL](/build-ros-nodes-for-voxl/) for information on making your own ROS nodes.
