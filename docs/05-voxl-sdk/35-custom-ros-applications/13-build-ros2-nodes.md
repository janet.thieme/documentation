---
title: Building ROS2 Nodes
layout: default
parent: Custom ROS Applications
nav_order: 13
permalink: /building-ros2-nodes/
---


# Building ROS2 Nodes
{: .no_toc }

---

## Overview

 **Note:** This is only meant to be done on VOXL2

VOXL2 ships with the [`voxl-utils`](https://gitlab.com/voxl-public/voxl-utils) and ROS2 FOXY pre-installed on target.

You can choose to compile ROS2 nodes either directly on-target (on VOXL2), or in the `qrb5165-emulator` docker on a host PC and then push the compiled application onto the VOXL2 (see [voxl-emulator](/voxl-emulator/) for more info). 

{% include youtubePlayer.html id="ohb80xtMrUY" %}

## Building on VOXL2

1) ADB or ssh into VOXL and go to your VOXL2's home directory. This assumes you have followed the previous setup pages in this manual to get ADB and/or ssh working.

```bash
me@mylaptop:~$ adb shell

...

voxl2:/$ cd ~/
voxl2:~$ 
```

3) make a new colcon workspace

```bash
voxl2:~$ mkdir -p colcon_ws/src
voxl2:~$ cd colcon_ws
```

4) Make a new node source directory in the src folder. Here we use the PX4's [`px4_ros_com`](https://github.com/PX4/px4_ros_com) and PX4's [`px4_msgs`](https://github.com/PX4/px4_msgs)

```bash
voxl2:~/colcon_ws$ cd src
voxl2:~/colcon_ws/src$ git clone https://github.com/PX4/px4_ros_com --recursive
voxl2:~/colcon_ws/src$ git clone https://github.com/PX4/px4_msgs --recursive
voxl2:~/colcon_ws/src$ cd ../
voxl2:~/colcon_ws$
```

5) Build!

```bash
voxl2:~/colcon_ws$ colcon build
```

6) This will build out the px4_ros_com ROS2 node as well as the px4_msgs required by this node. It will take about 3 minutes to finish compiling. Once finished compiling - you will want to source the install directory created from build. This contains all the path setups.

```bash
voxl2:~/colcon_ws$ source install/setup.bash
```

7) Now you can run your new ROS2 node!

```bash
voxl2:~/colcon_ws$ ros2 run px4_ros_coms px4_ros_coms
```

8) This can be done in the qrb5165-emulator as well. Just follow the same steps above in this demo and once compiled - `adb push` the `install` and `build` folders created in the `~/colcon_ws/` directory to your colcon_ws on target (VOXL2).