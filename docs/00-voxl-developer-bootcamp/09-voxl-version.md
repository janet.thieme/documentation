---
layout: default
title: 9. VOXL Version
parent: VOXL Developer Bootcamp
nav_order: 9
permalink: /voxl-version/
---

# Finding VOXL's SDK Version
{: .no_toc }


---
`voxl-version` is a handy utility, especially if you ever open a case on the [ModalAI forum](https://forum.modalai.com/) (always tell us your SDK version!).

In VOXL's bash shell, run the command:
```
voxl2:/$ voxl-version 
--------------------------------------------------------------------------------
system-image: 1.6.2-M0054-14.1a-perf
kernel:       #1 SMP PREEMPT Fri May 19 22:19:33 UTC 2023 4.19.125
--------------------------------------------------------------------------------
hw version:   M0054
--------------------------------------------------------------------------------
voxl-suite:   1.0.0
--------------------------------------------------------------------------------
Packages:
Repo:  http://voxl-packages.modalai.com/ ./dists/qrb5165/sdk-1.0/binary-arm64/
Last Updated: 2023-03-02 12:58:23
List:
       libmodal-cv                  0.3.1
       libmodal-exposure            0.0.8
       libmodal-journal             0.2.2
       libmodal-json                0.4.3
       ...
```
The `a.b.c` in "voxl-suite: a.b.c" is the SDK version, what you probably care about most.

However, there are a few other version numbers printed. This is because VOXL's firmware consists of two separate parts--"voxl-suite" and the System Image. The `voxl-suite` version number matches the version for the whole SDK release.

`voxl-suite` contains core ModalAI tools and services that run on VOXL. The system image contains the basic operating system, drivers, Linux kernel and various system partitions to make VOXL boot. While it's possible to install these independently, we HIGHLY recommend running matched pairs of voxl-suite and system image as installed by a stable SDK installer.

On the next page, you'll learn about flashing a new SDK, which flashes both `voxl-suite` and the System Image as a matched pair.

<br>
[Next: Upgrading SDK Version](/flash-system-image/){: .btn .btn-green }
