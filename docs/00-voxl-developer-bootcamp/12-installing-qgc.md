---
layout: default
title: 12. Installing QGC
parent: VOXL Developer Bootcamp
nav_order: 12
permalink: /installing-qgc/
---

# Installing QGC
{: .no_toc }

[QGroundControl](http://qgroundcontrol.com/) is a popular multi-platform ground control station software among PX4 autopilot developers. We will use it throughout the SDK documentation so it's good to install it at this point.

Here, we will be installing QGC on an Ubuntu 22.04 machine. 

---

### Install Dependencies

```bash
me@mylaptop:~$ sudo usermod -a -G dialout $USER
me@mylaptop:~$ sudo apt update
me@mylaptop:~$ sudo apt remove -y modemmanager
me@mylaptop:~$ sudo apt install -y gstreamer1.0-plugins-bad gstreamer1.0-libav gstreamer1.0-gl libqt5gui5 libfuse2 libsdl2-dev
```

### Download the AppImage

```bash
me@mylaptop:~$ cd ~/Desktop
me@mylaptop:~/Desktop$ wget https://d176tv9ibo4jno.cloudfront.net/latest/QGroundControl.AppImage
me@mylaptop:~/Desktop$ chmod +x QGroundControl.AppImage
```

### Launch

In Ubuntu 20.04, launch QGC by double-clicking on the QGroundControl.AppImage icon on the desktop.

In Ubuntu 22.04, right click on the desktop icon and select "Run as a program".

### Troubleshooting

If QGC does not launch, try running it from a terminal. This will print any debug messages it prints.

```bash
me@mylaptop:~$ cd ~/Desktop
me@mylaptop:~/Desktop$ ./QGroundControl.AppImage
```


<br>
<!-- google SEO has us listed here with a permalink of qgc-wifi... so use qgc-wifi
[Next: Connect to QGroundControl](/connect-to-qgc/){: .btn .btn-green }
-->
[Next: Connect to QGroundControl](/qgc-wifi/){: .btn .btn-green }
