---
layout: default
title: 6. Modal Pipe Architecture
parent: VOXL Developer Bootcamp
nav_order: 6
permalink: /mpa/
---

# Modal Pipe Architecture
{: .no_toc }

This is an introduction to VOXL's software architecture!

## Overview

ModalAI has built an ecosystem of [systemd](digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units) services for VOXL that run in the background. Paired with a suite of inspect-tools and utilities, a user can easily use these services to achieve high level functionalities like autonomous flight.

These services, tools, and utilities require inter-process communication to function. For example, [voxl-qvio-server](/voxl-qvio-server/) consumes camera data from [voxl-camera-server](/voxl-camera-server/) and IMU data from [voxl-imu-server](/voxl-imu-server/) to provide [Visual Inertial Odometry (VIO)](/vio/). We use POSIX pipes as the underlying transport mechanism for this inter-process communication due to their robustness, efficiency, and portability.

This combination of services, tools, and pipes that connect between them is what we call the Modal Pipe Architecture (MPA).


[![MPA-1.0-diagram_revC_Full.png](/images/voxl-sdk/MPA-1.0-diagram_revC_Full.png)](/images/voxl-sdk/MPA-1.0-diagram_revC_Full.png)



To provide standardization and ease of use, all [MPA services](/mpa-services/) use the C/C++ library [libmodal_pipe](/libmodal-pipe/) to create, publish, and subscribe to MPA data. A single process can also be both a server and a client to multiple pipes.

You can see every data-type that is being passed through pipes here: [modal_pipe_interfaces.h](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe/-/blob/master/library/include/modal_pipe_interfaces.h).


Note: You can find a comprehensive list of all services on the Gitlab: https://gitlab.com/voxl-public/voxl-sdk/services?page=1

## Using Services
Services can be manually started and stopped with the following `systemd` commands:

```
voxl2:/$ systemctl start SERVICENAME
```
```
voxl2:/$ systemctl stop SERVICENAME
```

These services can also be configured to activate (or deactivate) on boot, using the following commands:
```
voxl2:/$ systemctl enable SERVICENAME
```
```
voxl2:/$ systemctl disable SERVICENAME
```

<br />

`voxl-inspect-services` is a simple tool to show which services are enabled (start on boot), running, and what their cpu usage is (if running). The CPU usage should be taken with a grain of salt, as it a momentary snapshot of CPU usage. We recommend using the command `top` for more accurate process-specific cpu utilization.


### Typical Use

<img src="/images/voxl-developer-bootcamp/voxl-inspect-services.png" alt="voxl-inspect-services output"/>


### Optionally Show Version Numbers

The version column is disabled by default since the version number lookup is slow compared with the default three columns. However, it can be enabled with the `-v` or `--version` flag

<img src="/images/voxl-developer-bootcamp/voxl-inspect-services-version.png" alt="voxl-inspect-services -v output"/>

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/bin/voxl-inspect-services).

MAVROS and MAVSDK are out of the scope of this Bootcamp, but you can read more about them in the SDK section of the docs.

## Tools and Utilities
To learn about some basic MPA tools and utilities, see the next page.

<br>
[Next: Basic Tools and Utilities](/basic-functionalities/){: .btn .btn-green }
