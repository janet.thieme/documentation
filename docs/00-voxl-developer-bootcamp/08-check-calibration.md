---
layout: default
title: 8. Check Calibration
parent: VOXL Developer Bootcamp
nav_order: 8
permalink: /check-calibration/
---

# Check Calibration
{: .no_toc }

Run this command to check the calibration status of your VOXL:
```
voxl2:/$ voxl-check-calibration
```

Dev drones should come pre-calibrated, in which case you should expect an output like this (these particular examples are from a Starling drone):

<img src="/images/voxl-developer-bootcamp/check-calibration-success.png" alt="check calibration all calibration files present"/>

The exact calibration files necessary varies with different hardware and is determined automatically based on the SKU contained in the `/data/modalai/sku.txt` file onboard the VOXL.

If you lost your calibration files or are starting from a bare VOXL boards, you should expect an output like this:

<img src="/images/voxl-developer-bootcamp/check-calibration-missing.png" alt="check calibration has missing files"/>


Follow the [calibration guide](/calibration/) to calibrate your drone if necessary.

<br>
[Next: VOXL Version](/voxl-version/){: .btn .btn-green }
