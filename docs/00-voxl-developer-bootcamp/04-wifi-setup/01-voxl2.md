---
layout: default
title: 4.1 VOXL 2 WiFi
parent: 4. WiFi Setup
nav_order: 05
permalink: /voxl-2-wifi-setup/
has_children: false 
---

# VOXL 2 WiFi Setup

## Overview

This page serves as a WiFi setup guide for all devices based the VOXL 2 and VOXL 2 mini autopilots including the [Starling](/starling/) and [Sentinel](/sentinel/) development drones.

These models keep the core size and weight low by minimizing component counts and taking techniques like moving modems and USB Ports to add-on cards. Below are options for getting them on a network.

### WiFi Adapters

For desktop development, this is an ideal low-latency option.

Since VOXL 2 does not have USB onboard, we'll need to find a way to add USB ports. There are some options:

| Image                                                                                     | Description | Link |
|-------------------------------------------------------------------------------------------|-------------|-------|
| <img src="/images/voxl-developer-bootcamp/voxl2-m0078-wifi.jpg" alt="voxl2-m0078-wifi"/>  | <b>M0078</b><br> VOXL / VOXL 2 USB Expansion Board with Fastboot v2 <br><br> Shown with MCBL-00009 and AWUS036ACS | [Buy](https://www.modalai.com/collections/voxl-2-add-ons/products/m0078) <br> [Datasheet](/usb-epxansion-with-fastboot-v2-datasheet/)|
| <img src="/images/voxl-developer-bootcamp/voxl2-m0090-wifi.jpg" alt="voxl2-m0090-wifi"/>  | <b>M0090</b><br> VOXL 2 5g Modem Carrier <br><br> Shown with MCBL-00009 and AWUS036ACS | [Buy](https://www.modalai.com/collections/voxl-2-add-ons/products/m0090)  <br> [Datasheet](/5G-Modem-datasheet/) |
| <img src="/images/voxl-developer-bootcamp/voxl2-m0125-wifi.jpg" alt="voxl2-m0125-wifi"/>  | <b>M0125</b><br> VOXL 2 USB3.0 / UART Expansion Adapter <br><br> Shown with MCBL-00022 and AWUS036ACS  | [Buy](https://www.modalai.com/collections/voxl-2-add-ons/products/m0125) <br> [Datasheet](/voxl2-usb3-uart-add-on/) |
| <img src="/images/voxl-developer-bootcamp/voxl2-m0141-wifi.jpg" alt="voxl2-m0141-wifi"/>  | <b>M0141</b><br> VOXL 2 USB-A Breakout Board + UART/I2C/SPI <br><br> Shown here with AWUS036EACS | [Datasheet](/usb2-type-a-breakout-add-on/) |
| <img src="/images/voxl-developer-bootcamp/voxl2-mini-wifi.jpg" alt="voxl2-mini-wifi"/>    | <b>MCBL-00022</b><br><br> Shown here with AWUS036EACS | [Buy](https://www.modalai.com/products/mcbl-00022) <br> [Datasheet](/cable-datasheets/#mcbl-00022) |

The purpose of the USB port is to plug in a USB WiFi adapter. For a well-tested, plug and play solution: [VOXL 2 WiFi Add-On Kit](https://www.modalai.com/collections/accessories/products/voxl-2-wifi-add-on-kit)

Other Supported and Tested USB WiFi Dongles:

| Dongle                          | Chipset            | NDAA '20 Section 848 Compliant | Radio & FCC Report                                          | Link                                                                                                                                                                     |                                                                                                                                                                   |
|---------------------------------|--------------------|--------------------------------|--------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Alfa AWUS036EACS 802.11ac AC600  | Realtek RTL8812CU | Yes (COO Taiwan)               | Frequency Range: 5150.0-5825.0, 2412.0-2472.0 | [Link](https://fcc.report/FCC-ID/2AB878811)                                                                                                                              | [Product](https://www.alfa.com.tw/products/awus036eacs?_pos=1&_sid=a209c80dd&_ss=r&variant=39371526242376)  |
| Alfa AWUS036ACS 802.11ac AC600  | Realtek RTL8812AU  | Yes (COO Taiwan)               | Frequency Range: 5180.0-5240.0, 5745.0-5825.0, 2412.0-2462.0 | [Link](https://fcc.report/FCC-ID/2AB878811)                                                                                                                              | [Product](https://www.alfa.com.tw/products/awus036acs) [Amazon](https://www.amazon.com/Network-AWUS036ACS-Wide-Coverage-Dual-Band-High-Sensitivity/dp/B0752CTSGD) |
| Alfa AWUS036EAC 802.11ac AC1200 | Realtek RTL8812AU  | Yes (COO Taiwan)               |                                                              | [Product](https://www.alfa.com.tw/products/awus036eac?variant=36473966035016) [Amazon](https://www.amazon.com/Alfa-Long-Range-AWUS036EAC-Wireless-Adapter/dp/B00WC4C3X4) |                                                                                                                                                                   |
| TP-link TL-WN725N N150          | Realtek RTL8818EUS | No (COO China)                 |                                                              | [Product](https://www.tp-link.com/us/home-networking/adapter/tl-wn725n/) [Link](https://www.amazon.com/wifi-adapter-usb-pc-network/dp/B008IFXQFU/)                       |                                                                                                                                                                   |


If you prefer to use ethernet instead of a wireless setup, any USB to ethernet adapter, like Apple's A1277, should work.


## Configuration and Connection

{% include youtubePlayer.html id="YI0gTHtXQZk" %}

All devices support two WiFi modes, Station and SoftAP. Station Mode refers to the device being configured into a typical 'WiFi client' on a wireless network like your phone or laptop. Software enabled Access Point Mode (SoftAP) configures the device to generate its own hotspot so that other WiFi devices can connect to it. This is useful if you are in the field away from your home or office WiFi network. On VOXL 2, switching between the two modes can be done at any time using the `voxl-wifi` wizard.

### Configure and Connect in Station Mode

To configure the VOXL 2 device into Station Mode, run the following command from the host machine:

```bash
me@mylaptop:~$ adb shell voxl-wifi
Wifi is currently set up as follows:
	Current mode: station
	Current ssid: Jarvis

Station mode is for connecting to a normal wifi network.
In softap mode, the VOXL will broadcast its own wifi network.
Factory mode will set up softap with the following unique SSID:
	VOXL-00:c0:ca:ae:23:77

Please enter the intended Wi-Fi operation
Options:
	1) do nothing and quit
	2) station
	3) softap
	4) factory
```

Type `2` and press Enter to select station mode. From here, you will then be prompted for the SSID and Password of your network. 

To finish the setup, reboot the device:

```bash
me@mylaptop:~$ adb reboot && adb wait-for-device
```

Once the device has rebooted, you can use the command `voxl-my-ip` to get the IP address of your device on the network like:

```bash
me@mylaptop:~$ adb shell voxl-my-ip
wlan0: 192.168.1.217
```
And so in this case the IP address is `192.168.1.217`. Now you can connect via `ssh` as:

```bash
me@mylaptop:~$ ssh root@192.168.1.217
```
Where here the IP address following `root@` should correspond to the above output from the above command. The password to connect will be `oelinux123`. 

*Note:* It's not currently possible to assign a static IP, only DHCP is supported. If you need a static IP on your own wifi network it's best to configure that in your router's software.

### Configure and Connect in SoftAP Mode
By default, the unit ships configured in SoftAP mode with an SSID of `VOXL-XX:XX:XX:XX:XX:XX` with password `1234567890` (where XX:XX:XX:XX:XX:XX is the unit's MAC address. To configure the VOXL into Access Point Mode with your desired SSID, use the `voxl-wifi` wizard:

```bash
me@mylaptop:~$ adb shell voxl-wifi
Wifi is currently set up as follows:
	Current mode: station
	Current ssid: Jarvis

Station mode is for connecting to a normal wifi network.
In softap mode, the VOXL will broadcast its own wifi network.
Factory mode will set up softap with the following unique SSID:
	VOXL-00:c0:ca:ae:23:77

Please enter the intended Wi-Fi operation
Options:
	1) do nothing and quit
	2) station
	3) softap
	4) factory
```

Here you can either press `4` to load the device into SoftAP mode with default factory settings or you can press `3` to use a custom SSID and password.

Now on your host machine you should connect to the SSID above using either the default password `1234567890` or the custom password you set. The VOXL will assign itself the IP `192.168.8.1` and so then from your host machine you can use `ssh` to connect as:

```bash
me@mylaptop:~$ ssh root@192.168.8.1
```

Your device will be assigned an IP with DHCP in the 192.168.8.X subnet, usually 192.168.8.60, but not always.


## Appendix:

### Configure Hostname for Soft AP Mode (Optional)

It may be desired to have a dedicated hostname for your target instead of using an IP. This [thread](https://gitlab.com/voxl-public/documentation/issues/3) describes how to enable.

### Known Error Running ROS Through SSH

If you try to use ROS commands while SSH'd to VOXL and see errors such as this:

```bash
load_parameters: unable to set parameters (last param was [/rosdistro=indigo
]): <Fault 1: "<class 'xml.parsers.expat.ExpatError'>:not well-formed (invalid token): line 13, column 15">
```

The cause is a known issue with the GNOME Terminal, the default terminal emulator of Ubuntu. The solution is to use another terminal emulator such as xterm or Konsole instead of GNOME Terminal.

More information here: https://gitlab.com/voxl-public/documentation/issues/4

<br>
[Next: Inspect and Configure SKU ](/sku/){: .btn .btn-green }

