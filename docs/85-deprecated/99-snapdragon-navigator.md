---
layout: default
title: Snapdragon Navigator
parent: Deprecated
nav_order: 99
search_exclude: true
has_children: false
permalink: /snapdragon-navigator/
---

# Snapdragon Navigator
{: .no_toc }

---
## This package is not compatible with Modal Pipe Architecture (MPA) 
{: .no_toc }

voxl-suite 3.0 and later build around the MPA architecture. Snapdragon Navigator flight control has yet to be ported to this new architecture. To continue to use Snapdragon Navigator on the latest system image, please disable the following services:

```
yocto:~# systemctl disable voxl-qvio-server
yocto:~# systemctl disable voxl-camera-server
yocto:~# systemctl disable voxl-imu-server
```



---


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

ModalAI maintained a fork of Snapdragon Navigator to support VOXL hardware.

## Installation

1) Make sure you have an account at [developer.modalai.com](https://developer.modalai.com/login)

2) Down the `snav-modalai` IPK package from [https://developer.modalai.com/asset](https://developer.modalai.com/asset)

3) Make sure the VOXL [factory and software bundles](/install-software-bundles/) are installed.

4) Push the `snav-modalai` IPK package to VOXL.

```bash
me@mylaptop:~$ adb shell mkdir -p /home/root/ipk/
me@mylaptop:~$ adb push snav-modalai_1.3.0_8x96.ipk /home/root/ipk/
```

6) Log into VOXL via ADB or SSH. Remove the ModalAI Vision lib (it conflicts) before installing SNAV.

```bash
me@mylaptop:~$ adb shell
/ $ bash # adb starts an sh shell, start a bash shell manually
yocto:# opkg remove modalai-vl
yocto:# opkg install ~/ipk/snav-modalai_1.3.0_8x96.ipk
```
### License

An OEM license for Snapdragon Navigator and Machine Vision SDK should already be installed. If you want to use your own license, do the following:

```
adb push snapdragon-flight-license.bin /opt/qcom-licenses/snapdragon-flight-license.bin
```

## Configuration

1) While still logged into VOXL, run the configuration script. You will be prompted by the first question:

```bash
/home/root$ bash /etc/snav/configure_snav.sh

Which camera channel is the tracking camera connected to?
0 for DFT-only configuration
1 for stereo configuration
2 for TOF configuration
```

The wide-angle tracking camera will enumerate as camera ID 0, 1, or 2 depending on what other cameras are connected. This not NOT the physical port the camera is connected to, this is the camera ID that it is enumerated as in software. The 3 most common configurations and their corresponding camera ID are listed in the command prompt and repeated above for reference.

If you are using another configuration refer to the [Camera Configuration](camera-configurations.md) page. You can always check the cameras before this step with the camera-test app built into the VOXL image or with the voxl-cam-ros node. You can also go back later and change the camera channel used by the ModalAI Vision Lib by updating the "override_cam_id" value in /etc/snav/camera.downward.xml

2) Next you will be prompted with the second question:

```bash
Which physical configuration is this for?

1) 200_quad_2S_pack          3) voxlcam_upright_tracking_cam
2) 230_quad_3S_pack          4) voxlcam_upside_down_tracking_cam
```

This configures the position of the IMU relative to the wide-angle tracking camera for the Visual Inertial Odometry function. Most use cases will fall into these 3 options. For custom use cases refer to the [camera imu coordinate frames](camera-imu-coordindate-frames.md) page. This also sets up the orientation of the IMU relative to the drone and flight control parameters such as if it's using a 2S or 3S lipo.

3) Now we need to calibrate the IMU. Set up your VOXL in whichever orientation you will consider roll/pitch to be zero. Ideally this will be done with VOXL mounted to the quadcotper/robot and sitting still with no vibration or human contact. For VOXLCam this orientation should be with VOXLCam upright and looking forward with no tilt. For a red tray mount this is with the tray level.

```bash
the next step is static IMU calibration
Make sure the quadcopter or VOXLCAM is flat/upright (roll/pitch=0)
be patient, this will take 20-30 seconds
PRESS ENTER to start static IMU calibration
```

This process will take 20-30 seconds, be patient. You can ignore any systemctl warnings that show up. When it's done you should see:

```bash
[100] Static accel calibration was completed successfully
Restarting SNAV

DONE
```

## SNAV IMU Position Relative to Robot

In SNAV, the position of the IMU relative to the center of the drone/robot is set in /usr/lib/rfsa/adsp/snav_params.xml Note that this file is probably a simlink to another settings file and that these snav xml settings files often include other xml files.

For example, the base snav install has /usr/lib/rfsa/adsp/snav_params.xml link to /usr/lib/rfsa/adsp/200qc_runtime_params.xml which includes /usr/lib/rfsa/adsp/imu_offset.xml containing:

Position of IMU1 in red VOXL Tray for dragonfly quadcopter as distance from dragonfly center of mass to IMU1 in drone coordinate frame.

```xml
<SnavParameters>
  <orientation_params>
    <param name="vehicle_center_to_imu_x" value="0.050"/>
    <param name="vehicle_center_to_imu_y" value="0.0152"/>
    <param name="vehicle_center_to_imu_z" value="-0.023"/>
  </orientation_params>
</SnavParameters>
```

These distances are in meters along the vehicle coordinate frame which matches the ROS convention with X forward from center, Y to the left from center, and Z up vertically from center. The measurements are to IMU0 not IMU1.

For Voxl-Cam, the IMU1 position is roughly:

```xml
<SnavParameters>
  <orientation_params>
    <!-- in VOXLCAM, center is centroid of VOXL PCB. -0.001, -0.344, 0.0155
    is to IMU1 in corner of VOXL by camera ports -->
    <param name="vehicle_center_to_imu_x" value="-0.001"/>
    <param name="vehicle_center_to_imu_y" value="-0.0344"/>
    <param name="vehicle_center_to_imu_z" value="0.0155"/>
  </orientation_params>
</SnavParameters>
```

## SNAV IMU Rotation Relative to Robot

Orientation params also includes the rotation matrix from IMU to body coordinates. By default this is in /usr/lib/rfsa/adsp/eagle.xml which is included by /usr/lib/rfsa/adsp/200qc_runtime_params.xml

Rotation matrix for Voxl-Core in red tray:

```xml
<SnavParameters>
  <orientation_params>
    <param name="imu_R00" value="0.0"/>
    <param name="imu_R01" value="1.0"/>
    <param name="imu_R02" value="0.0"/>
    <param name="imu_R10" value="-1.0"/>
    <param name="imu_R11" value="0.0"/>
    <param name="imu_R12" value="0.0"/>
    <param name="imu_R20" value="0.0"/>
    <param name="imu_R21" value="0.0"/>
    <param name="imu_R22" value="1.0"/>
```

Rotation matrix for Voxl-Cam:

```xml
<SnavParameters>
  <orientation_params>
    <param name="imu_R00" value="0.0"/>
    <param name="imu_R01" value="0.0"/>
    <param name="imu_R02" value="-1.0"/>
    <param name="imu_R10" value="0.0"/>
    <param name="imu_R11" value="-1.0"/>
    <param name="imu_R12" value="0.0"/>
    <param name="imu_R20" value="-1.0"/>
    <param name="imu_R21" value="0.0"/>
    <param name="imu_R22" value="0.0"/>
```
