---
layout: default
title: VOXL Flight Deck Datasheet
parent: VOXL Flight Deck
nav_order: 10
permalink: /flight-deck-datasheet/
---

# VOXL Flight Deck Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL Flight Deck Platform is a fully assembled and calibrated flight kit, ready to mount to a vehicle and attach a battery!  It consists of the following core components:

- [VOXL Flight](/voxl-flight-datasheet/) computing platform mounted in VOXL-TRAY with:
  - [Stereo Sensors](/M0015/)
  - [Tracking Sensor](/M0014/)
  - [Hires Sensor](/M0025/)
  - Wi-Fi Antennas, Cooling Fan, [RC inpout cable](/cable-datasheets/#mcbl-00005/)
- [VOXL Power Module Kit v2](/power-module-v2-datasheet/)
- PWM Breakout Kit (M0022 board and [MCBL-00004 cable](/cable-datasheets/#mcbl-00004/))

To see an example of the VOXL Fight Deck in use, check out the [VOXL-m500](/voxl-m500-functional-description/)!

## High-Level Specs

| Specicifcation | [VOXL-FLIGHT-DECK-R2-1](https://www.modalai.com/collections/voxl-development-kits/products/voxl-flight-deck-r1) |
| --- | --- |
| Take-off Weight | TBD |
| Computing | [VOXL Flight](/voxl-flight-datasheet/) |
| Flight Control | PX4 on [VOXL Flight](/voxl-flight-datasheet/) |
| Image Sensors | [Stereo](/M0015/), [Tracking](/M0014/), [4k High-resolution](/M0025/) |
| Communication | WiFi built-in, [LTE](/lte-modem-and-usb-add-on-datasheet/) or [Microhard](/microhard-add-on-datasheet/) optional |
| Power Module | [Power Module v2](power-module-v2-datasheet) | 
| Battery | Up to 5S (not included, [recommended](https://www.thunderpowerrc.com/products/tp3400-4spx25)) |

## 2D/3D Drawings

* [VOXL_FLIGHT_DECK_R1-NO_PCB.STEP](https://storage.googleapis.com/modalai_public/modal_drawings/VOXL_FLIGHT_DECK_R1-NO_PCB.STEP)
* [Barometer Cap 3D STL](https://storage.googleapis.com/modalai_public/modal_drawings/voxl-flight-deck-baro-cap-v5.STL)

![voxl-flight-deck-cad](/images/voxl-flight-deck/voxl-flight-deck/flight-deck-cad.png)


## Primary Flight Deck Connections

Below describes the primary connections needed to get the Flight Deck configured on a vehicle and flying.

![voxl-flight-deck-connectors](/images/voxl-flight-deck/voxl-flight-deck/voxl-flight-deck-connectors.png)

|    | Name | Description |
|--- |--- |--- |
| A  | GPS Input | Dronecode Compliant GPS connection, 9optional when using [ModalAI's PX4 branch)(/flight-core-firmware/), GPS not included |
| B  | USB to QGroundControl | Connect VOXL Flight to QGroundControl for configuring PX4 using included cable |
| C  | RC Input | Connect to RC receiver using included cable, RC receiver not included |
| D  | USB to VOXL (under) | Connect to companion computer using adb over USB using Micro USB cable, cable not included |
| E  | PX4 MicroSD Card Slot (under) | For PX4 logging, [information on supported cards](https://dev.px4.io/v1.9.0/en/log/logging.html#sd-cards), SD card not included |
| F  | Power Input | Connects included [VOXL Power Module v3](/power-module-v3-datasheet/) using included cable |
| G  | PWM Output | Connects included PWM Breakout Board using included cable |

## VOXL Flight Connections

The VOXL Flight Deck consists of a VOXL Flight, with [the detailed pinouts described here](/voxl-flight-datasheet-connectors/)
