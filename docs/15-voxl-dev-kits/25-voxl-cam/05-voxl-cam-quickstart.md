---
layout: default
title: VOXL CAM Quickstart
parent: VOXL CAM
nav_order: 05
permalink: /voxl-cam-quickstart/
---

# VOXL-CAM Quickstart

VOXL-CAM is built around a VOXL and a Flight Core. Please refer to the [VOXL Quickstart](/quickstarts/) and the [Flight Core Quickstart](/flight-core-getting-started/) pages.