---
layout: default
title: VOXL 2 IO Firmware
parent: VOXL 2 IO
nav_order: 98
has_children: false
permalink: /voxl2-io-firmware/
---

# VOXL 2 IO Firmware / Bootloader
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## How to Tell What FW Version You Have

### SDK 1.3.0+

The **current** version which uses a VOXL ESC based firmware has LED pattern like this upon power up:

This is when there is no RC attached.

<iframe width="560" height="315" src="https://www.youtube.com/embed/DyPKEv7IGkQ?si=7vdwKSxwq3lLH3Ji" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe>

This is when there is no RC attached and bound.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ja9Mr-_q-qw?si=LjJX2YnhjAFt15an" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe>

### SDK 1.1.1+

<iframe width="560" height="315" src="https://www.youtube.com/embed/MFhsogbSmZQ?si=M86Kfquvm2Q2yplh" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe>

### Legacy

The **legacy** version which uses PX4IO has LED pattern like this upon power up:

<iframe width="560" height="315" src="https://www.youtube.com/embed/nG__wp8szCA?si=HUvaExB5r_YztgSR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe>

To update from the legacy version to the new version, see the section below.

If you would like to get a replacement by sending your hardware in, please contact us.

## Release Notes

### VOXL SDK 1.2.X (WIP)

- VOXL 2 IO (`M0065`) firmware version: 
    - Software version: 2
    - Hardware version: 35

Bug Fixes/Enhancements:

- add timeout on both LEDs (blue and orange). if there is no activity, the LEDs will be off
- define a new pwm control message 4-8 channels (dynamic), specify pwm width with 50ns resolution
  - this allows to set any PWM value for any of the 8 outputs between 0 and 2000us with resolution of 50ns
- switched to generating OneShot PWM signals (for a single control message, there is only one pulse)
  - removes the need for timeout to stop continuous pwm output in case of lack of commands
  - synchronizes the pwm output to the flight controller update cycle (lower latency)
  - voxl2-io cannot distinguish (and does not care) that the signal is OneShot style
    - this also inherently supports OneShot125 pwm pulse, which further allows to reduce control latency
  - Maximum update rate with regular pwm now is 400hz, 800hz for OneShot125 (since flight controller is updating at 800hz)
- If SBUS RC was connected to M0065, the PWM output from mixer was only sent out at the rate of SBUS input packets (100hz)
- fix the ESC calibration issues:
  - when using Blheli32 ESC (Tmotor), the calibration procedure resulted in the DISABLED pulse set as the minimum calibration pulse. To fix this, the calibration period had to be extended so that the ESC interprets the calibration minimum pulse correctly.
  - Calibration procedure was using the same values for calibrating the ESC and the armed min and max, which is problematic because this allowed the mixer to potentially command such low PWM command that would cause the motor to stop in the armed state. The fix was to specify separate parameters for calibration and for normal armed ranges. 
  - the updated calibration procedure has been tested using the standard pwm and oneshot125 signals.
- the mixer value range was 0-800, which is confusing because everyone is used to around 1000-2000us signals. The update now uses standard ranges in terms of microseconds (0-2000)

Features Added:

- added VOXL2_IO prefix for all the prints from the module, so it is easy to see it’s output: `voxl-px4 -d | grep VOXL2_IO`
- moved the M0065 version check to init and using the extended version message that has firmware hash. If M0065 is not detected, the voxl2_io module will not run
- added higher resolution command for setting desired PWM signal on M0065
- added support of up to 8 pwm channels (with the new packet). The pwm channels are configured as standard actuators
- added params for pwm range of calibration procedure, updated the armed pwm command range to have a safe margin and prevent motor from inadvertently shutting off when armed
- simplify SBUS packet handling (the packet that is coming from M0065)

Features Removed:
- command line actuator test (this is redundant - use the QGC instead so that we are consistently testing the same way)
- configuration packet for m0065 has been removed. with the new pwm control message, there is no need for any configuration of M0065

Misc:
- changed the failsafe actuator values from 0 to -1, which is the suggested default and it lets PX4 select the failsafe behavior based on the actuator type
  - Suggested parameter set:
    - DISABLED (VOXL2_IO_DIS)         1000
    - CALIBRATION MIN (VOXL2_IO_CMIN) 1050
    - CALIBRATION MAX (VOXL2_IO_CMAX) 2000
    - ARMED MIN (VOXL2_IO_MIN)        1100
    - ARMED MAX (VOXL2_IO_MAX)        2000
- update to Suggested ESC calibration procedure

### VOXL SDK 1.1.1

- VOXL 2 IO (`M0065`) firmware version: 
    - Software version: 1
    - Hardware version: 35
- Support for 4 channel 400Hz PWM output
- Support for SBUS 

## How to Update Legacy Firmware to New Firmware

### Send into ModalAI

We strive to enable in field updates, but when a bootloader update is required, sometimes lower level tools are needed. If you would like to update the bootloader and firmware on your VOXL 2 IO, you can send in your existing hardware to be updated without charge by contacting contact@modalai.com.

## Getting New Firmware

New firmware will ship with VOXL SDK updates and VOXL 2 command line tools will be able to update FW.

Additionally, FW files may be hosted on the [public downloads](https://downloads.modalai.com/) page.

## How to Update Firmware

### Via VOXL 2 Command Line Tools

{% include youtubePlayer.html id="YH9wGHlrBvw" %}

1. Disable voxl-px4 on your VOXL 2: ```systemctl disable voxl-px4```
2. Power off drone
3. Disconnect VOXL 2 IO from drone
4. Power on drone
5. Place the code below into a bash script, or you can run each command separately (Script is to be executed on host computer in either case).[^1] [^2]
     ```bash
     #!/bin/bash 
     set -e +x 
     
     # You will need to provide the path to the firmware here if you place it somewhere other than the root directory!
     FIRMWARE_FILE="~/m0065_firmware.bin"
     
     adb push m0065_firmware.bin /home/root
     adb shell "cd /usr/share/modalai/voxl-esc-tools; \
                 ./voxl-esc-upload-firmware.py \
                     --id 0 \
                     --firmware-file $FIRMWARE_FILE \
                     --device /dev/slpi-uart-2 \
                     --skip-firmware-reset 1     
                 "
     ```

6. You will see an output like the image below. Reconnect your VOXL 2 IO now.
<img src="/images/voxl2-io/firmware/fw_update_connect.png" width="640"/>    

7. Your VOXL 2 IO will now be flashed with the latest firmware and you should see an output like the image below.
<img src="/images/voxl2-io/firmware/fw_update_success.png" width="640"/>

8. You can re-enable voxl-px4 and power cycle now: ```systemctl enable voxl-px4```

<!-- 
### Via STLink[^3]

{% include youtubePlayer.html id="TnEr5L47R_s" %}

1. Connect the following pins between your STLink and VOXL 2 IO:

     | Connection | STLink Pin | VOXL 2 IO Pin 
     |:---:|:---:|:---:|
     | VTRef | 1  | 1 |
     | GND   | 4  | 6 |
     | SWDIO | 7  | 4 |
     | SWCLK | 9  | 5 |
     | RST   | 15 | 7 |    
    
     - STLink V2 pinout    
     <img src="/images/voxl2-io/firmware/stlink_pinout.png" width="480"/>    


2. Place the code below into a bash script and run on host computer.[^1]    
     ```bash
     #!/bin/bash
     set -e -x
     
     # You will need to provide the path to the firmware here if you place it somewhere other the same directory as this script!
     FIRMWARE_FILE="m0065_firmware.bin"
     
     sudo openocd -f /usr/share/openocd/scripts/interface/stlink.cfg \
         -f /usr/share/openocd/scripts/target/stm32f1x.cfg -c \
         "adapter speed 4000" -c \
         "init" -c \
         "sleep 100" -c \
         "reset halt" -c \
         "sleep 500" -c \
         "flash write_image erase $FIRMWARE_FILE  0x08001C00" -c \
         "reset run" -c \
         "exit"
     ```

3. Your VOXL 2 IO will now be flashed with the latest firmware and you should see an output like the image below.
<img src="/images/voxl2-io/firmware/fw_update_stlink.png" width="640"/>


## How to Update Bootloader

If you would like to update the bootloader on your VOXL 2 IO, please email devops @ modalai . com for more information.

### Via STLink

### Video

**Note**: This method uses the tool OpenOCD which may require some setup, guides can be found online.

1. Connect the following pins between your STLink and VOXL 2 IO (same as for firmware update via STLink):

| Connection | STLink Pin | VOXL 2 IO Pin 
|:---:|:---:|:---:|
| VTRef | 1  | 1 |
| GND   | 4  | 6 |
| SWDIO | 7  | 4 |
| RST   | 9  | 5 |
| VTRef | 15 | 7 |

<img src="/images/voxl2-io/firmware/stlink_pinout.png" width="480"/>    


2. Place the flashing script below into a bash script and run on host computer.
    - **Note**: You may need to run the command ```chmod +x {flash_script}``` in order to make your bash script executable.
3. Your VOXL 2 IO will now be flashed with the latest bootloader and you should see an output like the image below.
<img src="/images/voxl2-io/firmware/bl_update_success.png" width="640"/>

#### Flashing Script 
```
#!/bin/bash
set -e -x

# You will need to provide the path to the bootloader here if you place it somewhere other the same directory as this script!
BOOTLOADER_FILE="bootloader_m0065.bin"

sudo openocd -f /usr/share/openocd/scripts/interface/stlink.cfg \
	-f /usr/share/openocd/scripts/target/stm32f1x.cfg -c \
	"adapter speed 4000" -c \
	"init" -c \
	"sleep 100" -c \
	"reset halt" -c \
	"sleep 500" -c \
	"flash write_image erase $BOOTLOADER_FILE 0x08000000" -c \
	"reset run" -c \
	"exit" \
``` -->

---

Footnotes:

[^1]: You may need to run the command ```chmod +x {flash_script}``` in order to make your bash script executable.    
[^2]: You will need to change the port ```--device /dev/slpi-uart-2``` to ```--device /dev/slpi-uart-7``` if you have VOXL 2 IO connected to J19 on voxl2.    
[^3]: This method uses the tool OpenOCD which may require some setup, guides can be found online.    