---
layout: default
title: VOXL 2 IO Legacy Guide
parent: VOXL 2 IO
nav_order: 99
has_children: false
permalink: /voxl2-io-legacy-guide/
---

# VOXL 2 IO Legacy Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

To help with searching in our docs, the `VOXL 2 IO` board will be called `legacy PX4IO based board`

### VOXL SDK Requirements

This is not valid for VOXL SDK 1.1.1 or newer.  See [here](/voxl2-io-user-guide/) for newer guide.

This is accurate for SDK 0.9.5, and there were some breaking changes in SDK 1.0 when the PX4IO driver dropped from mainline PX4.

- SDK 1.1.0 - no change
- SDK 1.0.1 - PX4 1.14, no PWM support, SBUS RC input support only
- SDK 0.9.5 - PX4 1.12, basic 4 channel support, see following documentation

### Summary

The legacy PX4IO based board requires a host running PX4 running the [px4io](https://github.com/modalai/px4-firmware/tree/voxl-master/src/modules/px4iofirmware) module, such as VOXL 2.  The IO board provides PWM outputs (8) and expands the native Spektrum RC support by adding S.BUS support (and provides passthrough Spektrum as well).

The IO board is powered from and communicates via VOXL 2's RC port.  The VOXL 2 communicates to the legacy PX4IO based board using a UART connection.

### Limitations

#### Supported Mixers and Airframe Setup

Airframe selection via QGroundControl is not supported.

This has validated against `quad_x` and `hex_x` airframes only.  In the current release, configuring the mixer is done manually by editing the `/usr/lib/rfsa/adsp/quad_x_io.main.mix` file.

Quad:

```
R: 4x 10000 10000 10000 0
```

Hex:

```
R: 6x 10000 10000 10000 0
```

#### UART ESC Compatibility

This has not been validated to be used in parallel to the ModalAI UART ESC yet to expose additional PWM channels while also using the ModalAI UART ESC.

#### PX4 Parameters

The defaults set by `voxl-px4-set-default-parameters.config`.  Ensure you vehicle is configured correctly.

```
CBRK_IO_SAFETY 22027
PWM_MAIN_MAX   2000
PWM_MAIN_MIN   1060
PWM_MAIN_RATE   400
PWM_MAIN_DISARM 900
```

#### Spektrum RC Binding

The Spektrum bind feature invoked via QGroundControl does not work and needs to be ran manually from VOXL2.  To enable Spektrum bind:

Connect to VOXL 2's shell:

```
adb shell
```

Run the following commands to put Spektrum RC in bind mode.

```
px4-qshell px4io safety_on
px4-qshell px4io bind dsmx8
```

#### PWM ESC Calibration

NOTE: the following feature is still in beta.

For this process to work, you need to have physical access to J1 (PWM output port).

**Use with caution**, as this will be setting PWM outputs which could spin motors inadvertently.

**REMOVE ALL PROPS.  REPEAT: REMOVE ALL PROPS**

Using the Sentinel drone with PWM based ESCs as an example:

- Power off the drone
- Remove the PWM cable from legacy PX4IO based connector J1
- Power on the drone
- adb or ssh onto the drone
- get prepared to re-attach the cable (but not yet)

After you do the next step, you have 10 seconds to connect the PWM cable back to J1.

- from the terminal, run `px4-qshell pwm cal_backdoor`

*Note: you will see an timeout error message, this is OK, for example:*

```
voxl2:/$ px4-qshell pwm cal_backdoor
INFO  [qshell] Send cmd: 'pwm cal_backdoor'
ERROR [qshell] command timed out
ERROR [qshell] Command failed
```

- re-attach the cable, ESCs will make a short power on jingle
- in 10 seconds, a set of beeps being lasting several seconds
- after the jingles are done, you are done

{% include youtubePlayer.html id="2swQM7-jQwQ" %}

Here's the waveform generated over time:

<img src="/images/voxl2-io/legacy/m0065-pwm-cal.png" alt="m0065-pwm-cal"/>

And the same in video, for fun ;)

{% include youtubePlayer.html id="MMUcWQpLTV8" %}

#### VOXL 2 Firmware Update

The Firmware Update feature from host to legacy PX4IO based board is currently not supported.

## Wiring Guides

### Host Connection

#### VOXL 2 as Host (M0054)

<img src="/images/voxl2-io/legacy/m0065-host-m0054-2.png" alt="m0065-host-m0054"/>

From VOXL2, connect J19 pins 9-12 to VOXL2 IO J4 pins 1-4 as shown here:

| VOXL 2                 | legacy PX4IO based board          |
|---                     |---                 |
| J19 Pin 9  - 3P3V      | J4 Pin 1 - 3P3V_IO |
| J19 Pin 10 - Host TX   | J4 Pin 2 - IO RX   |
| J19 Pin 11 - Host RX   | J4 Pin 3 - IO TX   |
| J19 Pin 12 - GND       | J4 Pin 4 - GND     |

#### RB5-Flight as Host (M0052)

<img src="/images/voxl2-io/legacy/m0065-host-m0052-2.png" alt="m0065-host-m0052"/>

| RB5-Flight             | legacy PX4IO based board         |
|---                     |---                |
| J12 Pin 1 - 3P3V      | J4 Pin 1 - 3P3V_IO |
| J12 Pin 2 - Host TX   | J4 Pin 2 - IO RX   |
| J12 Pin 3 - Host RX   | J4 Pin 3 - IO TX   |
| J12 Pin 4 - GND       | J4 Pin 4 - GND     |


### RC Connections

#### Using S.BUS (FrSky R-XSR)

In the following example, the FrSky R-XSR receiver is being used with `MCBL-00065-1`:

<img src="/images/voxl2-io/legacy/rc-connections-sbus-molex.png" alt="m0065"/>

#### Using S.BUS (Graupner GR-16)

In the following example, the Graupner GR-16 receiver is being used has had it's channel 8 setup as `SBUS` with  `MCBL-00064-1`:

<img src="/images/voxl2-io/legacy/rc-connections-sbus-dupont.png" alt="m0065"/>

#### Using Spektrum (DSMX)

In the following example, the Spektrum SMX-SPM9745 receiver is being used with `MCBL-00005-1`:

<img src="/images/voxl2-io/legacy/rc-connections-spektrum-2.png" alt="m0065"/>

### PWM Connection

In the following example, the PWM Breakout Board (`MCCA-M0022`) and cable (`MCBL-00004-1`) are used to expose PWM based actuation:

<img src="/images/voxl2-io/legacy/m0065-pwm-2.png" alt="m0065"/>

## Modes and LED Patterns

The legacy PX4IO based board can be in the following modes accompanied with a unique LED pattern.

- Bootloader Mode
- Application Running in Waiting Mode
- Application Running in Standard Mode
- Firmware update mode
- Communication error mode

### Bootloader Mode

For the first 200ms after power on, the legacy PX4IO based board is in bootloader mode.  The serial firmware update could be performed during this time.

Note: this mode is very short lived an almost not perceptible to the eye.

| legacy PX4IO based board LED | State              | Meaning                  |
|---            |                    |---                       |
| Green         | Solid              | Power Good               |
| Blue          | Off                | Application not running  |
| Orange        | Fast Blink (10Hz)  | In bootloader mode       |

### Application Running in Waiting Mode

When the system is running and communications with VOXL 2 have not started and it is waiting, the following LED pattern can be seen.

| legacy PX4IO based board LED | State              | Meaning                      |
|---            |                    |---                           |
| Green         | Solid              | Power Good                   |
| Blue          | Blink Slow (2Hz)   | Application running OK       |
| Orange        | Solid              | Warning, waiting for VOXL 2  |

### Application Running in Standard Mode

When the system is running, the following LED pattern can be seen.

| legacy PX4IO based board LED | State              | Meaning                      |
|---            |                    |---                           |
| Green         | Solid              | Power Good                   |
| Blue          | Blink Slow (2Hz)   | Application running OK       |
| Orange        | Off                | No error or warning          |

### Firmware Update Mode

When the system is in firmware update mode, the following LED pattern can be seen.

| legacy PX4IO based board LED | State              | Meaning                     |
|---            |                    |---                          |
| Green         | Solid              | Power Good                  |
| Blue          | Fast Blink (10Hz)  | Firmware update in progress |
| Orange        | Fast Blink (10Hz)  | In bootloader mode          |


### Communication Error Mode

When communication errors occur, the following LED pattern can be seen.

Note: early RB5 based releases showed this mode when acting as an RC passthrough. 

| legacy PX4IO based board LED | State               | Meaning                                 |
|---            |                     |---                                      |
| Green         | Solid               | Power Good                              |
| Blue          | Blink Slow (2Hz)    | Communication with VOXL 2 OK            |
| Orange        | Mediaum Blink (4Hz)| Warning, not getting actuation messages  |




## Developer Guide

This is not valid for VOXL SDK 1.1.1 or newer.  See (here)[/voxl2-io-user-guide/] for newer guide.

This is valid for VOXL SDK 0.9.5 - 1.1.0.

### Hardware

<img src="/images/voxl2-io/legacy/m0065-dev-guide-bd-0.jpg" alt="m0065-dev-guide-bd-0"/>

- A battery powers VOXL2 (`M0054`) through a Power Module (`M0041`), and the battery also powers the ESCs/Motors
- VOXL2 J19 connects to VOXL2 IO J4 to provide both power and communications using a UART
- VOXL2 IO connects to an RC receiver (either J2 or J3, depending on user's receiver type)
- VOXL2 IO J1 connects to a PWM breakboard board (`M0022`) and 'communicates' to the ESCs using PWM, which in turn spin the motors


### Software

<img src="/images/voxl2-io/legacy/m0065-dev-guide-bd-1.jpg" alt="m0065-dev-guide-bd-1"/>

#### High Level Idea

VOXL2 runs PX4 and VOXL2 IO runs PX4IO.  They share a register map and keep it synchronized.  The VOXL2 IO has a control loop that includes mixing, and reacts to updates that are from VOXL2s `px4io` driver which populates the register map based updates to subscriptions.

#### Running on VOXL2

- On VOXL2 (`M0054`), the `px4io` driver runs on the sDSP/SLPI proc
- The `px4io` driver creates a bit of a hybrid device which has a few similarities as `pwm_output` device
- The `voxl-px4.config` script starts the detection and configuration process, including loading a mixer, which actually runs on VOXL2 IO (`M0065`)
- The `px4io` driver subscribes to various topics and updates the 'registers' on the VOXL2 IO board by sending commands over UART following a simple [protocol](https://github.com/PX4/PX4-Autopilot/blob/main/src/modules/px4iofirmware/protocol.h)

#### Running on VOXL2 IO

- Upon bootup, VOXL2 (`M0054`) detects the VOXL2 IO (`M0065`) board, and configures the VOXL2 IO board including loading of a mixer
- A control loop takes place, which involves checking various flags, updating status based upon updates from VOXL2, checking for inputs, handling outputs, etc

## PX4IO Based Firmware

Note: this is no longer maintained. See [here](/voxl2-io-firmware/) for newer information.

### Bootloader

#### Build

| Component    | Notes   |
|---           |---      |
| Build Docker | https://gitlab.com/voxl-public/flight-core-px4/px4-bootloader-build-docker |
| Source Code  | https://github.com/PX4/PX4-Bootloader.git |

To build:

```bash
git clone https://gitlab.com/voxl-public/flight-core-px4/px4-bootloader-build-docker
cd px4-bootloader-build-docker
./docker-build-image.sh

cd ..
git clone -b https://github.com/PX4/PX4-Bootloader.git --recursive
cd px4-bootloader
cp ../px4-bootloader-build-docker/docker-run-image.sh .

./docker-run-image.sh
make modalai_voxl2_io_bl -j4
```

Output at `build/modalai_voxl2_io_bl/modalai_voxl2_io_bl.bin`

#### Flash

Write the 4kb bootloader to `0x08000000`

### Application

#### Build

| Component    | Notes   |
|---           |---      |
| Build Docker | https://gitlab.com/voxl-public/flight-core-px4/px4-stm32-build-docker.git |
| Source Code  | https://github.com/modalai/px4-firmware/tree/voxl2-io |

To build:

```bash
https://gitlab.com/voxl-public/flight-core-px4/px4-stm32-build-docker.git
cd px4-stm32-build-docker
./docker-build-image.sh

cd ..
git clone -b voxl2-io-1.11 https://github.com/modalai/px4-firmware --recursive
cd px4-firmware
cp ../px4-stm32-build-docker/docker-run-image.sh .

./docker-run-image.sh
make modalai_voxl2-io
```

Output at `build/modalai_voxl2-io/modalai_voxl2_io.bin`

#### Flash

Write the 60kb application to `0x08001000`
