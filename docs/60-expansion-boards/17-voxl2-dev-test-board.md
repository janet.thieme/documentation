---
layout: default3
title: VOXL 2 Developer Test Board
nav_order: 17
parent: Expansion Boards
has_children: true
permalink: /voxl2-dev-test-board/
thumbnail: /voxl2-dev-test-board.png
buylink: https://www.modalai.com/products/voxl2-dev-test-board
summary: VOXL 2 Developer Test Board (M0144)
---

# VOXL 2 Developer Test Board

{: .no_toc }

The VOXL 2 Developer Test Board (M0144) is the first of-its-kind plug-in expansion board that allows Hardware and Software developers of the VOXL 2 ecosystem to access and utilize all GPIO/QUP signals (and others) from J3 and J5.
Benefits of this board include:

- No need to design your own custom breakout hardware for software and proof-of-concept efforts
- Significant Software and Hardware de-risking by validating your code on-target with actual hardware before integrating your custom hardware
- Allows for “what-if” code and hardware development and testing
- Promotes sandbox testing and validating of your VOXL 2 should you suspect any Hardware issues after a drone crash or other anomalous behavior
- Several “fun” features included such as:
- - PCA9685 16-Channel I2C to PWM driver direct on board for PWM/LED development
- - RGB LED plus discrete Orange, Green, and Blue LEDs for various/flexible indication or GPIO validation
- - Quickly access ANY GPIO from J3 or J5 on large header pins (GND posts throughout the design)
- - Remap any QUP port from J3 or J5 into an SPI, I2C, or UART function with direct on-board buffers/drivers mapped to standard JST Dronecode format cable connectors

- USB3 host port in the 10-pin ModalAI format with a high-current jumper option for powering devices requiring more than 1A of VBUS
- uSD Card Socket direct on-board
- Dedicated Linux Kernel Debug UART Port
- Jumpers and jumper wires included as part of a kit (see ordering info) to get you started immediately

*** ModalAI Provided Schematics to get you started with your own design!!*** We are not able to provide schematics for VOXL 2, and we understand this creates challenges for designers. This plug-in board is a great way to accelerate your own design and enhance your understanding of what hardware is proven to work, giving you a huge advantage for your design cycle times. Please refer to the datasheets page for a schematic download link: https://docs.modalai.com/voxl2-dev-test-board-datasheet/


<a href="https://www.modalai.com/products/voxl2-dev-test-board" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![m0144-hero](/images/voxl2-dev-test-board/m0144-hero-3d-top.jpg)

The following reference links are pre-requisite material for ensuring success with this design (these pages are written under the premise the following pages are understood):

- VOXL 2 Overall User Guide: https://docs.modalai.com/voxl2-user-guides/
- - Special emphasis on this page for SW usage: https://docs.modalai.com/voxl2-linux-user-guide/
- Expansion Hardware Design Guide: https://docs.modalai.com/expansion-design-guide/


