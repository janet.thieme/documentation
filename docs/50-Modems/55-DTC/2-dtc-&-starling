---
layout: default
title: DTC Starling Integration
parent: DTC Modems
nav_order: 5
permalink: /dtc-&-starling/
---

# DTC Starling Integration (Community Supported)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview
This page will cover how to add and mount a DTC Radio onto the Starling.

Below are some helpful links from DTC:

[DTC Products](https://www.domotactical.com/products/the-blucore-family)

[BLUSDR-6 Datasheet](https://www.domotactical.com/assets/downloads/Datasheets/BluSDR-6-BluSDR-Module.pdf)

![Starling with DTC Radio](/images/modems/dtc/starling/Starling_DTC.jpg)

As this is a community supported modem the ModalAI team cannot provide direct support. Instead please direct questions to US.Technical.Support@domotactical.com or UK.Technical.Support@domotactical.com

For Setting up the DTC Radio with the VOXL2,refer to the DTC User Guide Page located [here](/dtc-user-guide/).

## Requirements

### Hardware

The following ModalAI hardware is required to establish a DTC network connection between a VOXL-based product and a host device (computer, tablet, etc.). 

| Part Number | Description | Link |
| --- | --- | --- |
| MCCA-M0078 | VOXL USB Expansion Board v2 | [Purchase](https://www.modalai.com/products/voxl-debug-board-v2?_pos=2&_sid=21f1ad5c9&_ss=r)|
| MRB-D0005-4-V2-C6-M22 | Starling V2 Drone (with Battery) | [Purchase](https://www.modalai.com/products/starling?variant=45182917673264)|

## 3D Drawings
*** Below are links to the individual STEP and STL files for download. the links will take you to the Google Drive file location ***
* [Center Starling Mount STEP](https://drive.google.com/file/d/1nTcc4Su2a_iHPXDoPejo0Pu-4MIFROIf/view?usp=drive_link)
* [DTC Mount Holder STEP](https://drive.google.com/file/d/1H0_R1si7VENLEx27wqusICMNjiBGJZ3H/view?usp=drive_link)
* [Starling Center Mount STL](https://drive.google.com/file/d/1ZsdAWo-RgR-9b9NSQijjkxLt06AiTwA7/view?usp=drive_link)
* [DTC Mount Holder STL](https://drive.google.com/file/d/1kDzLV84nuVNhwLBOk-3_3nwQV1Ferb3f/view?usp=drive_link)

## Hardware Setup

***Please Note: the Li-Ion battery, recommended for the Starling is rated to handle ONLY the Starling. We recommend getting a separate 2 Cell or 3 Cell battery with a higher mAh when using the Starling with the DTC radio for long flights.***

### Starling Side
1. Disconnect the GPS Tower from the Starlings VOXL2.
2. To remove the GPS Tower from the Starling, unscrew the four center screws on the bottom of the Starling.
3. Next Unfasten the USB Hat from the Starling (Hold the standoffs under the VOXL2 to prevent twisting while unscrewing).
4. Remove the USB Hat from the VOXL2 along with the wifi dongle attached.
5. Socket the USB Expansion Board V2 onto the VOXL2 J3.
6. Fasten the USB Expansion Board V2 down.
7. Attach the DTC Mount by lining up the holes on the center mount with the Starling holes. (Image below)

![Starling Center Mount Holes](/images/modems/dtc/starling/Center_Mount_Hole.jpg)

8. Lastly, fasten down the DTC Mount with M2 screws and M2 nut.
(Optional) You can reattach the GPS Tower by placing it further back and reconnect it to the VOXL2. 

![Starling with Mount](/images/modems/dtc/starling/Starling_mount.jpg)

### Powering DTC Radio
We will need a method to power both the DTC Radio and the Starling, The simplest way to accomplish this is to send power from the battery to both the Power module and the DTC Radio.

#### Required Components:
- 2 Pin Female Rectangular Molex Connector
- XT30  Male Connector
- XT30 Female Connector
- Wires
- Wire Cutters
- Wire Stripper

#### Instructions:
1. Solder on the wires to the the XT30 Male connector taking note of the positive and negative wires.
2. Split the wire coming from the XT30 Male Connector.
3. On the first end of the split wire solder on the XT30 Female Connector.
4. On the second end of the split wire solder on the Female Molex Connector.

![Power Splitter](/images/modems/dtc/starling/Long_Splitter.jpg)

*** Note: Since the DTC Radio requires between 16 and 6V to operate we have to use the power from the battery and not splice off the power going into the VOXL2. Secondly since all the ESC pads are being used for the motors there isn't an easier method of getting the required voltage other than from the battery. ***

### DTC Radio
*** Please be sure that the DTC Radio is configured properly as a peripheral before mounting it onto the Starling. ***
1. Connect the Antennas to the DTC radio.
2. Place the DTC Radio in the Mount holder and make sure the holes line up.
3. Next Wrap the Antennas over the top of the Radio crossing one another to reach the sides of the DTC mount.

![DTC in Mount](/images/modems/dtc/starling/DTC_mount.jpg)

4. Point the 3M VHB Pads toward the DTC Radio.
5. Attach the Antennas to the sides, either by the VHB pads or using Velcro, or tape.

### Connecting it All Together

1. Connect the DTC Mount to the DTC Mount Holder
2. Fasten down using 4 x M2 Screws
3. Plug in the Power splitter to the Power Module for the Starling.

![Connect Power Splitter](/images/modems/dtc/starling/power_splitter_to_module.jpg)

4. Connect the Power splitter's Molex end to the Molex connector on the the DTC's Power cable.
5. Connect the 5-pin jst of the communications cable to the USB2 port on the DTC Radio
6. Connect the 4-pin jst of the Communications cable to the USB port on the USB2 HAT for the VOXL2

![Final Results](/images/modems/dtc/starling/Starling_DTC.jpg)

7. Lastly, connect the battery to the Power splitter and the VOXL2 and DTC radio will now be Powered 

Check the other DTC Radio to ensure that the mesh shows that two nodes are appearing on the network.