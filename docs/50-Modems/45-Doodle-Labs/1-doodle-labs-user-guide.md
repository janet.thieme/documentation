---
layout: default
title: Doodle Labs User Guide
parent: Doodle Labs Modems
nav_order: 4
permalink: /doodle-labs-user-guide/
---

# Doodle Labs User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

VOXL and VOXL2 support the ability to quickly and easily add a Doodle Labs wireless connection to a Ground Control Station.  The following guide provides you the necessary details to do so.

Below are some helpful link from Doodle Labs:

[Doodle Labs Technical Library](https://doodlelabs.com/technologies/tech-library/)

[Doodle Labs Datasheets](https://doodlelabs.com/technologies/tech-library/technical-library-datasheets/)

[Doodle Labs Helix Datasheet](/images/modems/Doodle_Labs_Smart_Radio-RM-2025-2L_Datasheet.pdf)

![doodle-labs-modem](/images/modems/doodle-labs/doodle-labs-helix.png)

## Requirements

### Hardware

The following ModalAI hardware is required to establish a Doodle Labs network connection between a VOXL-based product and a host device (computer, tablet, etc.). 

| Part Number | Description | Link |
| --- | --- | --- |
| MCCA-M0078 | VOXL USB Expansion Board v2 | [Purchase](https://www.modalai.com/products/voxl-debug-board-v2?_pos=2&_sid=21f1ad5c9&_ss=r)| 
| MCCA-M0041 | Power Module v3 | [Purchase](https://www.modalai.com/products/apmv3?variant=31984449650739)| 
| M0006, M0019, or M0054 | VOXL, VOXL-Flight, or VOXL2 | Purchase links: [VOXL2](https://www.modalai.com/products/voxl-2?variant=39914779836467), [VOXL-Flight](https://www.modalai.com/products/voxl-flight?variant=31636287094835), [VOXL](https://www.modalai.com/products/voxl?_pos=1&_sid=f8681cb53&_ss=r)| 

### Software

In order for the required drivers to be available on target, the following software is required:

#### VOXL2
- [VOXL2 Platform Release 1.3.1-0.8+ ](https://developer.modalai.com/asset/2)
- [voxl-modem_0.16.0+](http://voxl-packages.modalai.com/dists/qrb5165/)

#### VOXL
- [VOXL Platform Release 3.8.0-0.8+ ](https://developer.modalai.com/asset/1)
- [voxl-modem_0.16.0+](http://voxl-packages.modalai.com/dists/apq8096/)

## Hardware Setup

Below is a list of required hardware in order to create a connection between a VOXL-based device and a ground control station via. Doodle Labs Helix modems.

| QTY | Part Number            | Description                                      | Notes                                                                                                                                                                                                                                                  |
|-----|------------------------|--------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | MCCA-M0078-2           | VOXL USB Expansion Board v2                      | [Purchase](https://www.modalai.com/products/voxl-debug-board-v2?_pos=2&_sid=21f1ad5c9&_ss=r)                                                                                                                                                           |
| 1   | MCCA-M0041-B           | Power Module v3                                  | [Purchase](https://www.modalai.com/products/apmv3?variant=31984449650739)                                                                                                                                                                              |
| 1   | M0006, M0019, or M0054 | VOXL, VOXL-Flight, or VOXL2                      | Purchase links: [VOXL2](https://www.modalai.com/products/voxl-2?variant=39914779836467), [VOXL-Flight](https://www.modalai.com/products/voxl-flight?variant=31636287094835), [VOXL](https://www.modalai.com/products/voxl?_pos=1&_sid=f8681cb53&_ss=r) |
| 1   | Ground station device  | Computer, laptop, etc.                           | Device must have [smsc95XX driver](https://github.com/torvalds/linux/blob/master/drivers/net/usb/smsc95xx.c) support                                                                                                                                   |
| 1   | Power source           | Power supply for ground station modem            | 5V Operating Voltage                                                                                                                                                                                                                                   |
| 1   | USB-C cable            | USB-C to USB on specified port on ground station | USB-C, A, etc.                                                                                                                                                                                                                                         |
| 2   | Helix modem            | Doodle Labs Helix modem                          | [Description](https://doodlelabs.com/products/smart-radio/helix/)                                                                                                                                                                                      |
| 1   | Helix breakout board   | Ground station side, provide power and USB       | 5V Input                                                                                                                                                                                                                                               |
| 2   | Helix 15-pin cable     | Modem data / power cables                        | 1 modified (see below), 1 stock                                                                                                                                                                                                                        |
| 2   | Helix Antennas         | Cables, antennas                                 | Attenuators for desktop development only                                                                                                                                                                                                               |

Below are the pinout details for the modified 15-pin modem to 4-pin JST cable, or use `MCBL-00068-1`:

| Pin # | Name  | Pin # | Name |
| --- | --- | --- | --- |
| 1 | 5-V Input | 1 | VCC |
| 2 | - | - | - |
| 3 | GND | 4 | GND |
| 4 | - | - | - |
| 5 | - | - | - |
| 6 | - | - | - |
| 7 | - | - | - |
| 8 | - | - | - |
| 9 | - | - | - |
| 10 | - | - | - |
| 11 | - | - | - |
| 12 | - | - | - |
| 13 | - | - | - |
| 14 | USB-Dev-D- | 2 | D- |
| 15 | USB-Dev-D+ | 3 | D+ |

### VOXL Hardware Setup

#### Using M0078

The [USB Expansion Board with Fastboot v2](/usb-epxansion-with-fastboot-v2-datasheet/) exposes a USB 2.0 port on VOXL and VOXL2, and provides up to 5A to peripherals. 

<img src="/images/voxl2/m0054-helix-2.png" alt="m0054-helix-2"/>

![doodle-voxl-setup](/images/modems/doodle-labs/voxl2-doodle.jpg)

Pictured above for your reference is a VOXL2 flight deck with all cameras connected and Doodle Labs Helix connected.

***IMPORTANT! Before modifying hardware setup, ensure power is off or disconnected.***

- Attach `M0078` add-on board to `J3` of `VOXL2`.

- Ensure jumper is set to `USB PWR 5V` side in order to provide sufficient power to modem.

- Attach antennas to ports on Helix modem, if doing desktop development use the attenuators between cable and antenna in order to prevent RF saturation in a bench top environment.

- Use modified `15-pin to 4-pin JST` cable to connect modem to `J3` of `M0078`

- Attach `M0041` APM to VOXL2 and provide power

- Plug in `USB-C cable` from desktop to VOXL2 in order to access `ADB` later on.

### Ground Station Hardware Setup

![doodle-ground-station-setup](/images/modems/doodle-labs/ground-station-doodle.jpg)

Pictured above for your reference is a ground station setup showing a Doodle Labs Helix modem and breakout board.

***IMPORTANT! Before modifying hardware setup, ensure power is off or disconnected.***

- Attach antennas to ports on Helix modem, if doing desktop development use the attenuators between cable and antenna in order to prevent RF saturation in a bench top environment.

- Use un-modified `15-pin to 15-pin cable` to connect the `Helix modem` to the `breakout board`

- Use `USB-C cable` to `breakout board` to `host` ground station

- Connect power (5V) to `green connector`, see above photo to determine +(red) and -(black)

- Turn on power from power supply

## Software Setup

### VOXL2 Software Setup

- adb onto VOXL2

```
adb shell

voxl2:/$
```

- Run the following the begin modem setup:

```
voxl2:/$ voxl-configure-modem 
 
What type of modem are you using?

If you are unsure of which modem you have, take a look at the following datasheets:

v2 LTE Modem: https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/
Microhard Modem: https://docs.modalai.com/microhard-add-on-datasheet/

1) v2
2) microhard
3) doodle
4) quectel
```

- Select `doodle` from the menu

```
#? 3
 
Enter the IP address you would like to the VOXL to use on the Doodle network:
Note: The chosen IP address must be of the form: 10.223.0.XXX

Default - 10.223.0.100

1) default
2) custom
```

- Select the static IP you would like your VOXL2's `eth0` network interface to be set to, either choose default or another IP on the `10.223` subnet

```
#? 1 

qrb5165 based device detected
reloading systemd services
enabling voxl-modem systemd service
starting voxl-modem systemd service
DONE configuring voxl-modem
```

- The VOXL2 will now enable the `voxl-modem` service in order to enable the Helix modem connection and set the static IP on bootup

- Verify that the `eth0` network interface has enumerated and that the IP has been set correctly: 

```
ifconfig eth0
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.223.0.100  netmask 255.0.0.0  broadcast 10.255.255.255
        ether 00:30:1a:4f:96:05  txqueuelen 1000  (Ethernet)
        RX packets 2930  bytes 661816 (661.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 5264  bytes 290037 (290.0 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

### Ground Station Software Setup

- Set a static IP address for the host PC on the Doodle network, Ubuntu 18.04 shown below:

![doodle-labs-default-ip](/images/modems/doodle-labs/doodle-host-ip.png)

In this case, the IP address of the host PC will be set to `10.223.0.150`

## Connecting to QGroundControl

Now that both the VOXL side and ground station side software have been setup you should be able to ping one device from the other.

For example, one can ping the ground station from VOXL by doing the following:

```
voxl2:/$ ping 10.223.0.150
PING 10.223.0.150 (10.223.0.150): 56 data bytes
64 bytes from 10.223.0.150: icmp_seq=0 ttl=64 time=0.126 ms
64 bytes from 10.223.0.150: icmp_seq=1 ttl=64 time=0.169 ms
64 bytes from 10.223.0.150: icmp_seq=2 ttl=64 time=0.115 ms
64 bytes from 10.223.0.150: icmp_seq=3 ttl=64 time=0.117 ms
64 bytes from 10.223.0.150: icmp_seq=4 ttl=64 time=0.121 ms
```

Since we are able to get packets from drone to ground station we can now connect our VOXL2 / drone to QGroundControl.

Check the configuration of [voxl-mavlink-server](/voxl-mavlink-server/) if it does not connect immediately. [voxl-mavlink-server](/voxl-mavlink-server/) is the main routing process for MAVLink data on VOXL 2. 