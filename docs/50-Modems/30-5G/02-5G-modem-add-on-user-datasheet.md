---
layout: default
title: 5G Modem Datasheet
parent: 5G Modems
nav_order: 2
has_children: false
permalink: /5G-Modem-datasheet/
---

# 5G Modem Add-on Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![5G](/images/modems/5G/m0090-labeled.jpg)


---

| PCB                                             | H x W x D        | Weight | Drawings                                                                                                                                                                                                                             |
|-------------------------------------------------|------------------|--------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ModalAI 5G Modem Carrier Board (M0067 or M0090) | 67.5 x 36 x 10mm | 12.8g  | [2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0067-mechanical-drawing%20v1.pdf) <br> [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0067_VOXL2_5G_MODEM_DEBUG_REVA_S1_P9.step) |
| Quectel RM502Q-AE                               | 52× 30 × 2.3mm   | 8.7g   | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/Quectel_RM500Q-AERM502Q-AE_Hardware_Design_V1.0.pdf)                                                                                               |

## Block Diagrams

### M0090 Block Diagram

![5G](/images/modems/5G/M0090-block-diagram.jpg)

## Drawings

### 3D Step

[M0090 step file](https://storage.googleapis.com/modalai_public/modal_drawings/M0090_DUAL_VOXL_5G_M.2_REVA-3-01-V2-QUECTEL.step)

## Connectors

### J2 VOXL 2 Board to Board

See J3 at [VOXL 2 Connectors](/voxl2-connectors/)

### J3 nano-SIM CARD

Connector Type: SF72S006VBAR2500

### J4 VOXL 2 Board to Board

See J5 at [VOXL 2 Connectors](/voxl2-connectors/)

### J5 m.2 B-key

Connector Type: 2199230-3
Description: M.2 0.5PITCH 4.2H KEY B 15U'' AU

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    |            |                           |

### J6 VOXL 2 Kernel Serial

Not installed

### J8 SPI

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | VREG_3P3V            |                           |
| 2    | MISO_3P3V            |                           |
| 3    | MOSI_3P3V            |                           |
| 4    | SCLK_3P3V            |                           |
| 5    | CS0_3P3V             |                           |
| 6    | GND                  |                           |


### J9 UART / i2c

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | VREG_3P3V            |                           |
| 2    | TX_3P3V              |                           |
| 3    | RX_3P3V              |                           |
| 4    | SDA_3P3V             |                           |
| 5    | SCL_3P3V             |                           |
| 6    | GND                  |                           |


### J11 USB 3 10-pin

Applies to: M0067, M0090

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5VDC                 | 5V protected power output |
| 2    | USB1_HS_HUB3_CON_D_N |                           |
| 3    | USB1_HS_HUB3_CON_D_P |                           |
| 4    | GND                  |                           |
| 5    | USB1_HUB3_SS_RX_N    |                           |
| 6    | USB1_HUB3_SS_RX_P    |                           |
| 7    | GND                  |                           |
| 8    | USB1_HUB3_SS_TX_N    |                           |
| 9    | USB1_HUB3_SS_TX_P    |                           |
| 10   | GND                  |                           |

### J12 USB 2 4-pin

Applies to: M0067, M0090

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5VDC                 | 5V protected power output |
| 2    | USB1_HS_HUB4_CON_D_N |                           |
| 3    | USB1_HS_HUB4_CON_D_P |                           |
| 4    | GND                  |                           |


## Switches

### SW3 TBD

## Supported Modems

### Sierra Wireless EM9291

The M0090-3-02 PCBA supports the Sierra Wireless EM9291 modem. Official Quectel [datasheet](https://www.sierrawireless.com/iot-modules/5g-modules/em9291/).

* 5G NR3GPP Release 15 NSA/SA operation, Sub-6 GHz5G NR
* LTE CategoryDL Cat 20/ UL Cat 18

| Air Interface   | Supported Bands                                                                  |
|-----------------|----------------------------------------------------------------------------------|
| 5G NR Sub-6     | n1, n2, n3, n5, n7, n8, n12, n13, n14, n18, n20, n25, n26, n28, n29, n30, n38, n39, n40, n41, n46, n48, n66, n70, n71, n75, n76, n77, n78, n79                                                               |
| 4G LTE Cat-20   | B1, B2, B3, B4, B5, B7, B8, B12, B13, B14, B18, B19, B20, B21, B25, B26, B28, B29, B30, B32, B34, B38, B39, B40, B41, B42, B43, B46, B48, B66, B71              |


### Quectel RM502Q-AE

The M0090-3-01 PCBA supports the Quectel RM502Q-AE modem. Official Quectel [datasheet](https://www.quectel.com/wp-content/uploads/2021/03/Quectel_RM502Q-AE_5G_Specification_V1.2.pdf).

* 5G NR3GPP Release 15 NSA/SA operation, Sub-6 GHz5G NR
* LTE CategoryDL Cat 20/ UL Cat 18

| Air Interface   | Supported Bands                                                                  |
|-----------------|----------------------------------------------------------------------------------|
| 5G NSA          | n38/n41/n77/n78                                                                  |
| 5G SA           | n1/n2/n3/n5/n7/n8/n12/n20/n25/n28/n38/n40/n41/n48/n66/n71/n77/n78                |
| 5G DL 4 ×4 MIMO | n1/n2/n3/n7/n25/n38/n40/n41/n48/n66/n77/n7                                       |
| 5G UL 2 ×2 MIMO | 41/n77/n78                                                                       |
| LTE-FDD         | B1/B2/B3/B4/B5/B7/B8/B12/B13/B14/B17/B18/B19/B20/B25/B26/B28/B29/B30/B32/B66/B71 |
| LTE-TDD         | B34/B38/B39/B40/B41/B42/B43/B48                                                  |
| LAA             | B46                                                                              |
| DL 4 ×4 MIMO    | B1/B2/B3/B4/B7/B25/B30/B32/B34/B38/B39/B40/B41/B42/B43/B48/B66                   |
| UMTS WCDMA      | B1/B2/B3/B4/B5/B6/B8/B19                                                         |

