---
layout: default
title: VOXL m500 Unboxing
parent: M500 User Guides
nav_order: 1
has_children: false
permalink: /voxl-m500-getting-started/
---

# VOXL m500 User Guide
{: .no_toc }

This guide will walk you from taking the VOXL m500 out of the box and up into the air!

For technical details, see the [datasheet](/voxl-m500-reference-drone-datasheet/).

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

![m500-out-of-box](/images/m500/m500-out-of-box.jpg)

### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation) 
- LiPo 4S battery with XT60 connector
  - Buy [Here](https://www.modalai.com/products/4s-battery-pack-gens-ace-3300mah) 

- Host computer with:
  - QGroundControl 3.5.6+
  - Wi-Fi
  - a terminal program

### What's in the Box

Included in the box are:

- VOXL m500 with Spektrum receiver (note: the system supports FrSky receivers like X8R, see [below](#rc-radio-setup))
- 10" props
- USB-to-JST Cable (not used in this guide)
- Spare landing gear mounting screws

[Next Step: Vehicle Components](/voxl-m500-components/){: .btn .btn-green }