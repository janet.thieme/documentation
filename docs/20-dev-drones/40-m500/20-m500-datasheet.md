---
layout: default
title: M500 Datasheet
parent: M500
nav_order: 20
has_children: false
permalink: /m500-datasheet/
---

# M500 Reference Drone Datasheet
[Buy Here](https://www.modalai.com/m500)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![voxl-m500](../images/m500/m500-shopify.jpg)

## High-Level Specs

| Specicifcation                         | [MRB-D0001-2-V1.1-F1-B1-C3](https://www.modalai.com/m500)                                                                                                                                                                                     |
|----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Take-off Weight                        | 1075g with battery (820g w/o battery)                                                                                                                                                                                                                                                      |
| Size                                   | Length: 15.5" <br> Width: 15.5" <br> Height to: <br> - Bottom Plate: 2.25" <br> - Top of Flight Deck: 4.5" <br> - Top of GPS: 8"                                                                                                                                                           |
| Flight Time                            | >20 minutes                                                                                                                                                                                                                                                                                |
| Payload Capacity (Impacts Flight Time) | 1kg                                                                                                                                                                                                                                                                                        |
| Motors                                 | [2216 880KV](https://shop.holybro.com/motor2216-880kv-1pc_p1154.html)                                                                                                                                                                                                                      |
| Propellers                             | 10" (1045) <br>[Replacement](https://shop.holybro.com/s500-v2-propeller10452pair_p1155.html)                                                                                                                                                                                               |
| Frame                                  | s500 <br>[Replacement](https://shop.holybro.com/s500-v2-frame-kit_p1139.html)                                                                                                                                                                                                              |
| ESCs*                                  | [BLHeli 20A](https://shop.holybro.com/blheli-esc-20a_p1143.html)                                                                                                                                                                                                                           |
| Computing                              | [VOXL Flight](/voxl-flight-datasheet/)                                                                                                                                                                                                                                                     |
| Flight Control                         | PX4 on [VOXL Flight](/voxl-flight-datasheet/)                                                                                                                                                                                                                                              |
| Image Sensors                          | [Stereo](/M0015/), [Tracking](/M0014/), [4k High-resolution](/M0025/)                                                                                                                                                |
| IP Communication                       | WiFi built-in, [LTE](/lte-modem-and-usb-add-on-datasheet/) or [Microhard](/microhard-add-on-datasheet/) optional                                                                                                                                                                           |
| R/C (Remote Control)                   | Configured for Spektrum DSMX (either DSMX-SPM9645 or DSMX-SPM9745), compatible with most DIY R/C systems through expansion I/O on [Flight Core](/flight-core-datasheet/)                                                                                                                   |
| Power Module                           | [Power Module v3](power-module-v3-datasheet)                                                                                                                                                                                                                                               |
| Battery                                | Up to 5S (not included, recommended: [Thunderpower TP3400-4SPX25 - select XT60 connector](https://www.thunderpowerrc.com/products/tp3400-4spx25?variant=31080855339072)) or [GensAce 3300mAh 11.1V 50C 3S1P Lipo Battery Pack With XT60 Plug](https://www.genstattu.com/ga-b-50c-3300-3s1p-xt60t.html) |
| Additional Components                  | N/A                                                                                                                                                                                                                                                                                        |
| Landing Gear                           | [Polycarbonate Round Tube 1/2" OD, 3/8" ID, Clear](https://www.mcmaster.com/2044T42/)                                                                                                                                                                                                      |
| Battery Standoffs                      | Screws: Black-Oxide Alloy Steel Socket Head Screw, M2.5 x 0.45 mm Thread, 20 mm Long [91290A108](https://www.mcmaster.com/catalog/126/3290/) <br> Spacers: Aluminum Unthreaded Spacer, 4.500 mm OD, 14 mm Long, for M2.5 Screw Size [94669A108](https://www.mcmaster.com/catalog/126/3464) |

## System Overview

[View in fullsize](/images/m500/VOXL-m500-R2-architecture.png){:target="_blank"}
![m500-.png](/images/m500/VOXL-m500-R2-architecture.png)

## m500 Labeled

[View in fullsize](/images/m500/labeled_m500.png){:target="_blank"}
![m500-labeled.png](/images/m500/labeled_m500.png)

## PX4 Tuning Parameters

Our engineers have spent a lot of time finely tuning this vehicle, and the [parameters are available here](https://gitlab.com/voxl-public/px4-parameters)

## Motor Configuration

The propellers have arrows on them as to which way they should turn. Motor configuration in the image below.

![voxl-m500-motor-configuration.png](/images/m500/voxl-m500-motor-configuration.png)

## Connectivity for Remote Operation

| Connectivity Option | Use Case | Details |
| --- | --- | --- |
| Spektrum R/C | Manual remote control of the vehicle | [Configure](https://docs.modalai.com/configure-rc-radio/) |
| WiFi | Short range (~100m) IP connectivity for debug and nearby flights | [Setup](https://docs.modalai.com/wifi-setup/) |
| LTE | Long Range, BVLOS operation | [User Manual](/lte-v2-modem-manual/) <br> [Datasheet](/lte-modem-and-usb-add-on-v2-datasheet/) |
| Microhard | Medium Range (1-3km) IP connectivity for control, telemetry and video | |
