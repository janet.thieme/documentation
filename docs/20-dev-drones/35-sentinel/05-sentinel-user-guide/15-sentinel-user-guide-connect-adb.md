---
layout: default
title: Sentinel Connect Over ADB
parent: Sentinel User Guide
nav_order: 15
has_children: false
permalink: /sentinel-user-guide-connect-adb/
---

# Sentinel Connect Over ADB
{: .no_toc }

1. TOC
{:toc}

## How to Setup ADB

See [here](/setup-adb/)

## How to Connect to Sentinel Over ADB

After installing adb, connect your host PC to Sentinel using a USB type C cable.
Then, open your terminal, and run the following:

```bash
adb shell
```
Now you are running a shell inside the Sentinel's Ubuntu OS!

```
voxl2:/
```


[Next Step: Connect to Network](/sentinel-user-guide-connect-network/){: .btn .btn-green }