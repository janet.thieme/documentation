---
layout: default
title: Sentinel Unboxing
parent: Sentinel User Guide
nav_order: 1
has_children: false
permalink: /sentinel-user-guide-unboxing/
---

# Sentinel  Unboxing
{: .no_toc }

1. TOC
{:toc}

This guide will walk you from taking Sentinel out of the box and up into the air!

For technical details, see the [datasheet](/sentinel-datasheet/).

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*


---

## Overview

### What's in the Box

![sentinel-drone-only](/images/sentinel/sentinel-kit.jpg)

- Sentinel Drone
- 10" props
- Two sets of Propellor ring adapters
- 5G Modem or Microhard Modem or Wifi (Optional)

![sentinel-connectivity-options](/images/sentinel/sentinel-connectivity.png)


### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- Power Supply or Battery with XT60 connector
  - e.g. Gens Ace 5000mAh, or any 3S battery with XT60 connector
  - Buy Battery [Here](https://www.modalai.com/products/gens-ace-11-1v-60c-3s-5000mah-lipo-battery-pack-with-xt60-plug?_pos=2&_sid=0558e5a83&_ss=r) 
  - Buy Power Supply [Here](https://www.modalai.com/products/ps-xt60?_pos=1&_sid=f5b241f03&_ss=r)  
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi
- USB Type C Cable


[Next Step: Vehicle Components](/sentinel-user-guide-components/){: .btn .btn-green }
