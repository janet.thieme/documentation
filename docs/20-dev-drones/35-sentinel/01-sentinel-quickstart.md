---
layout: default
title: Sentinel Quickstart
parent: Sentinel
nav_order: 1
has_children: False
permalink: /sentinel-quickstart/
---

# Sentinel Reference Drone Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

The [User's Guide](/sentinel-user-guides/) walks through taking Sentinel out of the box and up into the air!

For technical details, see the [datasheet](/sentinel-datasheet/).

<br>
{% include youtubePlayer.html id="hMhQgWPLGXo" %}


{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

---

## Overview

### What's in the Box

![sentinel-drone-only](/images/sentinel/sentinel-kit.jpg)

- Sentinel Drone
- 10" props
- Two sets of Propellor ring adapters
- 5G Modem or Microhard Modem or Wifi (Optional)

![sentinel-connectivity-options](/images/sentinel/sentinel-connectivity.png)


### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- Power Supply or Battery with XT60 connector
  - e.g. Gens Ace 5000mAh, or any 3S battery with XT60 connector
  - Buy Battery [Here](https://www.modalai.com/products/gens-ace-11-1v-60c-3s-5000mah-lipo-battery-pack-with-xt60-plug?_pos=2&_sid=0558e5a83&_ss=r) 
  - Buy Power Supply [Here](https://www.modalai.com/products/ps-xt60?_pos=1&_sid=f5b241f03&_ss=r)  
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi
- USB Type C Cable

## Sentinel Vehicle Components

### Overview

The following images give an overview of the vehicle components.

#### Front
[View in fullsize](/images/sentinel/sentinel-labeled-1.png){:target="_blank"}
![Sentinel-Labeled-1](/images/sentinel/sentinel-labeled-1.png)

#### Side
[View in fullsize](/images/sentinel/sentinel-labeled-2.png){:target="_blank"}
![Sentinel-Labeled-2](/images/sentinel/sentinel-labeled-2.png)

#### Top
[View in fullsize](/images/sentinel/sentinel-labeled-3.png){:target="_blank"}
![Sentinel-Labeled-3](/images/sentinel/sentinel-labeled-3.png)

#### Bottom
[View in fullsize](/images/sentinel/sentinel-labeled-4.png){:target="_blank"}
![Sentinel-Labeled-4](/images/sentinel/sentinel-labeled-4.png)

[Next Step: Power and Battery](/sentinel-user-guide-power/){: .btn .btn-green }

## Sentinel Power Supply and Battery

### Safety First!

For setup and configuration of your Sentinel you must **Remove the Propellers** for safety.

### Battery Information

Sentinel requires a Gens Ace 5000mAh, or any 3S battery with XT60 connector.

Buy Battery [Here](https://www.modalai.com/products/gens-ace-11-1v-60c-3s-5000mah-lipo-battery-pack-with-xt60-plug) 

![vehicle-battery-connected](/images/sentinel/sentinel-unplugged.jpg)

To install a 3s battery, slide into the body as shown. There is a stop at the front of the vehicle that will prevent the battery from sliding too far forward. The battery should be pressed all the way in until it hits the stop for a consistent center of mass. 

![vehicle-battery-connected](/images/sentinel/sentinel-powered.jpg)

Power on Sentinel by connecting the battery to the XT60 connector. 

## Benchtop Power Supply

For desktop use, a 12VDC wall power supply can be used and is [available here](https://www.modalai.com/products/ps-xt60). This is convenient when doing development.

