---
layout: default
title: RB5 WiFi
parent: Qualcomm Flight RB5 SDK
nav_order: 55
has_children: false
permalink: /Qualcomm-Flight-RB5-wifi/
search_exclude: true
---

# Qualcomm Flight RB5 WiFi
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Connecting to a WiFi Network

In order to connect your Qualcomm Flight RB5 to a WiFi network, you need to specify your network's `SSID` and `PASSWORD` in the configuration file by doing the following:

```
adb shell

vi /data/misc/wifi/wpa_supplicant.conf
```

You will see a file similar to the following: 
```
# Only WPA-PSK is used. Any valid cipher combination is accepted.
ctrl_interface=/var/run/wpa_supplicant
update_config=1

network={
#Open
#        ssid="example open network"
#        key_mgmt=NONE
#WPA-PSK
	ssid="Your SSID here"
	#proto=WPA
	key_mgmt=WPA-PSK
	pairwise=TKIP CCMP
	group=TKIP CCMP
	psk="Your PASSWORD here"
#WEP
#	ssid="example wep network"
#	key_mgmt=NONE
#	wep_key0="abcde"
#	wep_key1=0102030405
#	wep_tx_keyidx=0
}
wowlan_triggers=magic_pkt
```

Modify the `ssid` and `psk` fields to correspond with your WiFi network's details.

Save and exit out of `vi` with `:wq!` and reboot your Qualcomm Flight RB5 for your changes to take effect.

If you enter a valid ssid and psk and are within range of your WiFi router, the next time you boot your device it will automatically connect to your network.
