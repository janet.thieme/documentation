---
layout: default
title: VOXL SDK for RB5 Upgrade Guide
parent: Qualcomm Flight RB5 SDK
nav_order: 1
has_children: false
permalink: /Qualcomm-Flight-RB5-voxl-sdk-upgrade-guide/
search_exclude: true
---

# VOXL SDK for RB5 Flight Upgrade Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

# RB5 Platform Release

The latest RB5 SDK builds are all using the VOXL SDK!. Please download the [RB5 Flight with VOXL SDK Platform Releases](https://developer.modalai.com/asset/3) when you update your drone.

## Requirements

The following are required in order to install the platform release:
- Ubuntu 18.04+ host computer
- ADB/fastboot

## Backing up files

From the previous RB5 Flight system image to the new platform release, there has been an update to the partition table layout. Due to this, all files on the RB5 Flight device will be lost during the flash. It is crucial to backup any files that are important before flashing the device.

We do require that you backup your camera calibration files before flashing. This can be done by pulling the following files to your local computer:

``` bash
mkdir ~/cam_cal_files # Directory to store camera calibration files locally
adb pull /data/modalai/opencv_tracking_intrinsics.yml 
adb pull /data/modalai/opencv_stereo_intrinsics.yml
adb pull /data/modalai/opencv_stereo_rear_intrinsics.yml
adb pull /data/modalai/opencv_stereo_extrinsics.yml
adb pull /data/modalai/opencv_stereo_rear_extrinsics.yml
```

These files came stock when each RB5 Flight was shipped. If these files are not present on your device, they were most likely accidentally erased, most likely through a previous RB5 system image flash. If this is the case and you need to regenerate these files, you can do so using the [voxl-calibrate-camera](https://docs.modalai.com/calibrate-cameras/) tool after installing the platform release.

## Installing Platform Release

In order to upgrade begin by downloading the latest [RB5 platform release](https://developer.modalai.com/asset/3) on your host computer and navigating into that directory:
``` bash
cd rb5_platform_0.9.5
```

{: .alert .warning-alert}
**WARNING:** Make sure to backup important camera calibration files and other important files before continuing

Next, you can begin the install:
```bash
./install.sh
```

The following message will appear:
``` bash
[WARNING] This system image flash is intended only for the following
          platform: RB5 Flight (m0052) 

          Make sure that the device that will be flashed is correct.
          Flashing a device with an incorrect system image will lead
          the device to be stuck in fastboot.

Would you like to continue with the RB5 Flight (m0052) system image flash?
1) Yes
2) No
```

This message is confirming that you are indeed flashing an RB5 Flight unit. Enter 1 to continue.

**Warning** If the prompt is showing the platform as VOXL2 (m0054), then you have the wrong version of the platform release that is meant for the VOXL2 platform. Make sure the listed platform is RB5 Flight (m0052). Flashing the platform release for the incorrect device will lead the device to be stuck in fastboot.

After continuing, you'll be prompted that your partition table is incorrect:
``` bash
[WARNING] Partition layout was found to be incorrect, this requires
          flashing the full partition table. Flashing the partition
          table will also flash every partition causing all data on
          the device to be lost.

When doing a full flash, make sure to backup any important files

Would you like to continue with the full wipe/flash now?
1) Yes
2) No
```

As mentioned before, this is expected since the partition table has been updated to include new data (/data), configuration (/etc/modalai), and calibration (/data/modalai) partitions. Enter 1 to continue.

Finally the system image will flash, then the voxl-suite SDK will install. Once complete the installation complete message will appear:
``` bash
===================================================== 
|     Done installing voxl-suite for QRB5165        |
|                                                   |
|         Please visit our online guides at         |
|             https://docs.modalai.com/             |
|                                                   |
|      To configure Modal Pipe Architecture (MPA)   |
|       services, please run voxl-configure-mpa     |
|                                                   |
| To see what MPA services are enabled and running, |
|           please run voxl-inspect-services        |
|                                                   |
|   To see a list of MPA utilities available, adb   |
|      or ssh into VOXL and type voxl{TAB}{TAB}     |
===================================================== 

Processing triggers for libc-bin (2.27-3ubuntu1.2) ...
[INFO] Done installing voxl-suite
```

The platform release is now successfully installed! 

## Pushing Backup Files Back to RB5 Flight

Any backed up files can be pushed back to the device using the adb push command. In order to move the backed up camera calibration files, you can use the following commands:
``` bash
cd ~/cam_cal_files
adb push opencv_tracking_intrinsics.yml /data/modalai 
adb push opencv_stereo_intrinsics.yml /data/modalai
adb push opencv_stereo_rear_intrinsics.yml /data/modalai
adb push opencv_stereo_extrinsics.yml /data/modalai
adb push opencv_stereo_rear_extrinsics.yml /data/modalai
```

The same can be done for any other files that were also backed up.
