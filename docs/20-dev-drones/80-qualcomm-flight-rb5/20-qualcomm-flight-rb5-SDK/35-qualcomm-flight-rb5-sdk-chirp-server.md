---
layout: default
title: RB5 Chirp Server
parent: Qualcomm Flight RB5 SDK
nav_order: 35
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-chirp-server/
search_exclude: true
---

# Qualcomm Flight RB5 SDK Chirp Server
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## rb5-chirp-server

The `rb5-chirp-server` allows you to publish data from the two chirp distance sensors.

### Chirp Information

Coming soon

### Architecture

Coming soon

### How to Build and Install

If you have a `Qualcomm Flight RB5`, it comes pre-loaded with the software and there's no need to re-install unless you want to.  If you want to see the source, build and tweak it, the repo is [COMING-SOON](https://docs.modalai.com/Qualcomm-Flight-RB5-sdk/).


### How to Use

#### Configure to Run on Bootup

Coming soon


#### View Service Status

Coming soon.