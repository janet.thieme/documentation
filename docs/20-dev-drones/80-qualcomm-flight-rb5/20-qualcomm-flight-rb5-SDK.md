---
layout: default
title: Qualcomm Flight RB5 SDK
parent: Qualcomm Flight RB5 
nav_order: 20
has_children: true
permalink: /Qualcomm-Flight-RB5-sdk/
search_exclude: true
---

# Qualcomm Flight RB5 Reference Drone SDK Overview

[Buy Here](https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL SDK for RB5 Flight

**Update 2022-07-20**: the Qualcomm Flight RB5 vehicle is now supported by the VOXL SDK!  It requires RB5 Flight System Image 1.3 or newer.

Information about a Platform Release that includes both an updated system image and the VOXL SDK for  is available [here](/voxl2-platform-release/)

Now, the VOXL2 and RB5 Flight share a similar system image and SDK.

## RB5 Flight SDK Overview

**NOTE**: the recommended software stack is the VOXL SDK.

![RB5 SDK Overview](/images/rb5/rb5-sdk/rb5-sdk-overview.png)

## Available Packages

| Name              | Description |
| ---               | --- |
| rb5-camera-server | Publishes camera frames and meta data to named pipes |
| rb5-streamer      | Video streaming management |
| rb5-qvio-server   | Publishes VIO data to named pipe |
| rb5-voa-to-px4    | Publishes VOA data to PX4 |
| rb5-chirp-server  | Publishes depth data from chirp sensors to named pipe |
| rb5-modem         | Manages 5G modem connection |
| rb5-utils         | Various Qualcomm Flight RB5 Utilities |
| rb5-opencv        | 32-bit and 64-bit OpenCV libraries |
| rb5-mv            | 32-bit support libraries to use Qualcomm MV SDK on 64-bit system |

## Releases

| Version                         | Changelog |
| ---                             | ---       |
| Qualcomm Flight RB5 SDK (1.1.3) | - barometer driver bug fixes <br> - default PX4 parameter fixes <br> - ability to set custom QGC IP and Port <br> - rb5-streamer: ability to set bitrate <br> - rb5-modem: auto-reconnect if cellular connection lost <br> - rb5-bind: new command line tool for binding Spektrum radio* |
| Qualcomm Flight RB5 SDK (1.0.5) | Initial Release |


\* rb5-bind (rb5-utils) utility requires M0094 Spektrum Bind Board, included with Qualcomm Flight RB5 as of 2/2/22. 

## Image Sensor Support

Below captures the image sensor data paths at a high level.

[View in fullsize](/images/rb5/rb5-camera-framework.png){:target="_blank"}

![RB5.png](/images/rb5/rb5-camera-framework.png)
