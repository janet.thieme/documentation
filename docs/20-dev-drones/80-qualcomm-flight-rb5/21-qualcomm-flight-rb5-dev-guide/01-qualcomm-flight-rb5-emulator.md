---
layout: default
title: RB5 Emulator
parent: Qualcomm Flight RB5 Developer Guide
nav_order: 1
has_children: false
permalink: /Qualcomm-Flight-RB5-emulator/
search_exclude: true
---

# Qualcomm Flight RB5 Emulator
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

The ```qrb5165-emulator``` is an armv8 Docker image that runs on your Linux PC to emulate the rb5-flight. This Docker image takes the root filesystem of the system image and builds it on top of the armv8 Docker 'scratch' image. This provides developers an off-target workflow for developing for the rb5-flight


## Requirements

- Ubuntu 18.04 host (other versions may work, but have not been tested)
- Docker
- QEMU, to install:

```bash
# install packages
sudo apt-get install qemu binfmt-support qemu-user-static

# Execute the registering scripts, see https://www.stereolabs.com/docs/docker/building-arm-container-on-x86/
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```

## Installation
In order to install the ```qrb5165-emulator``` begin by visiting <https://downloads.modalai.com> and downloading ```qrb5165-emulator (1.1.1)```

Next you can navigate to the directory where the file was installed and load the docker image
```bash
cd ~/Downloads # This location may be different for you
docker load < qrb5165-emulator_V1.1.1.tgz
```

Next make sure that the docker image was loaded
```bash
docker images | grep qrb5165-emulator
```

This should list the qrb5165-emulator
```
qrb5165-emulator             1.1.1     73f7d7d4a000   6 weeks ago     2.94GB
```

Finally, run the docker image
```bash
docker run --rm -it --privileged \
    --platform linux/arm64/v8 \
    --mount type=bind,source="$(pwd)",target=/home/user \
    qrb5165-emulator:1.1.1 /bin/sh
```

This will start the docker container and put you into an interactive shell. 

## Usage

A common use case for the ```qrb5165-emulator``` is to compile code in the docker image, and push the binaries on to the ```rb5-flight```. This can be done easily inside the docker container.

Begin by running the docker image. Once inside the interactive shell, begin writing your code.

For this example, we'll be compiling a hello_world.c example, which contains the following code:
```c
#include <stdio.h>

int main(){
    printf("Hello World\n");
    return 0;
}
```

You can then compile your code
```bash
gcc hello_world.c
```

Finally, outside of the docker image, you can push your binaries to the ```rb5-flight``` , using adb, so that the code can be ran there. In this case, we'll be pushing our binary to the ```/data``` directory
```bash
adb push ./a.out /data
```

