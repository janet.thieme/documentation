---
layout: default3
title: Qualcomm Flight RB5
nav_order: 80
has_children: true
parent: Dev Drones
permalink: /qualcommflightrb5/
summary: A comprehensive drone reference design with low-power, high-performance heterogenous computing, artificial intelligence, optional 5G connectivity, 7 image sensor concurrency, and more.
thumbnail: /rb5/rb5-hero-final.png
buylink: https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone
---

# Qualcomm Flight RB5
{: .no_toc }

**Update 2022-07-20: the Qualcomm Flight RB5 vehicle is now supported by the VOXL SDK!  Please [see here](/Qualcomm-Flight-RB5-voxl-sdk-upgrade-guide/) from more information**

A comprehensive drone reference design with low-power, high-performance heterogenous computing, artificial intelligence engine that is designed to deliver 15 TOPS, long-range Wi-Fi 6 and 5G (Optional) connectivity, support for 7 image sensor concurrency, computer vision, and vault-like security. 

<a href="https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/22/qualcomm-flight-rb5-5g-drone" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![RB5](/images/rb5/rb5-hero-final.jpg)
