---
layout: default
title: Seeker Components
parent: Seeker User Guide
nav_order: 10
has_children: false
permalink: /seeker-user-guide-components/
---


### Vehicle Components

The following gives an overview of the vehicle components.

![seeker-labeled-1](/images/seeker/seeker-labeled-1.jpg)

![seeker-labeled-2](/images/seeker/seeker-labeled-2.jpg)

![seeker-labeled-3](/images/seeker/seeker-labeled-3.jpg)

---

[Next Step: Power and Battery](/seeker-user-guide-power/){: .btn .btn-green }
