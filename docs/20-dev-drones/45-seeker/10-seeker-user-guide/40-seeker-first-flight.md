---
layout: default
title: Seeker First Flight
parent: Seeker User Guide
nav_order: 40
has_children: false
permalink: /seeker-user-guide-firstflight/
---

# Seeker First Flight
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*


<p style="color:red"> Make sure the battery is unplugged or the propellors are removed for safety while handling </p>
---

## Preflight Checks

Once your Seeker is in a safe location where you wish to fly, connect the battery while the Seeker is on the ground.

Wait for the connection with QGroundControl to be established.

### Attitude Check

Lift the vehicle and move it around, verify that the attitude reported in QGroundControl GUI looks and responds correctly. Try not to cover the tracking camera during this process.

![qgc_attitude_gui.png](/images/voxl-sdk/qgc/qgc_attitude_gui.png)


### First Flight (Position Mode)

You should be comfortable flying before proceeding! 

We recommend your first flight be in position hold (vio) mode outside with a GPS lock. 
For more information on flying in position mode, click the green button below for a vio overview.

Once confortable with position mode, now safely fly in manual mode! No instructions here, this is an advanced mode and you should know what you are doing if you're flying!


---

[Next Step: VIO](/seeker-user-guide-vio/){: .btn .btn-green }
