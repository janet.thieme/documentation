---
layout: default
title: Seeker Using VIO
parent: Seeker User Guide
nav_order: 45
has_children: false
permalink: /seeker-user-guide-vio/
---

# Seeker Streaming VIO

{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## Viewing Localization Data from Tracking Sensor

The Visual Intertial Odometry (VIO) feature uses the tracking image sensor with fisheye lens and an onboard IMU to provide localization data. At a high level, this provides you a realtime “X, Y, Z” in space.
To use this feature, simply run the following command:

```
voxl-inspect-qvio
```

The output displayed updates as you move the VOXL-CAM around in space:

![vio-output](/images/voxl-cam/voxl-cam-qvio-output.png)

You can move VOXL-CAM around and see the data update.

Hit `CTRL+C` to stop the program

See [Here](/voxl-inspect-qvio/) for more information

## Viewing Point Cloud (and Stereo Tracking Images)
(only needed if you mess with things).04 or newer
- [Setup ROS on PC](https://docs.modalai.com/setup-ros-on-host-pc/)
- [Setup ROS on VOXL](https://docs.modalai.com/setup-ros-on-voxl/) (already done on shipped VOXL-CAMs)
- RViz (`apt-get install rviz`)

### Configuration

Please ensure you've setup ROS on PC

The unit's software is shipped already configured and ready to use.

  *Note: If needed, you can follow the [Software Setup](#software-setup) section below to fully reconfigure the VOXL-CAM.*

### SSH into VOXL-CAM

If not already done, SSH into VOXL-CAM and change the shell to bash:

```bash
ssh root@192.168.8.1
~# bash
yocto:~#
```

Setup the environment (this assumes VOXL-CAM is in SoftAP with IP Address of 192.168.8.1 which is the default):

```bash
yocto:~# source /opt/ros/indigo/setup.bash
yocto:~# source ~/my_ros_env.sh 
```

Run the following command to run the MPA ROS Node:

```bash
yocto:~# roslaunch voxl_mpa_to_ros voxl_mpa_to_ros.launch
```
The exepected output is shown:

```bash
yocto:~# roslaunch voxl_mpa_to_ros voxl_mpa_to_ros.launch
... logging to /home/root/.ros/log/790d372a-a45e-11eb-ade8-ec5c68cd1ad7/roslaunch-apq8096-4239.log
Checking log directory for disk usage. This may take awhile.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://192.168.1.188:36869/

SUMMARY
========

PARAMETERS
 * /mpa/voxl_mpa_to_ros_node/image0_pipe: tracking
 * /mpa/voxl_mpa_to_ros_node/image0_publish: True
 * /mpa/voxl_mpa_to_ros_node/image1_pipe: hires_preview
 * /mpa/voxl_mpa_to_ros_node/image1_publish: True
 * /mpa/voxl_mpa_to_ros_node/image2_pipe: qvio_overlay
 * /mpa/voxl_mpa_to_ros_node/image2_publish: True
 * /mpa/voxl_mpa_to_ros_node/image3_pipe: 
 * /mpa/voxl_mpa_to_ros_node/image3_publish: False
 * /mpa/voxl_mpa_to_ros_node/image4_pipe: 
 * /mpa/voxl_mpa_to_ros_node/image4_publish: False
 * /mpa/voxl_mpa_to_ros_node/image5_pipe: 
 * /mpa/voxl_mpa_to_ros_node/image5_publish: False
 * /mpa/voxl_mpa_to_ros_node/imu0_pipe: imu0
 * /mpa/voxl_mpa_to_ros_node/imu0_publish: True
 * /mpa/voxl_mpa_to_ros_node/imu1_pipe: imu1
 * /mpa/voxl_mpa_to_ros_node/imu1_publish: True
 * /mpa/voxl_mpa_to_ros_node/stereo_pipe: stereo
 * /mpa/voxl_mpa_to_ros_node/stereo_publish: True
 * /mpa/voxl_mpa_to_ros_node/tof_cutoff: 100
 * /mpa/voxl_mpa_to_ros_node/tof_pipe: tof
 * /mpa/voxl_mpa_to_ros_node/tof_publish: True
 * /mpa/voxl_mpa_to_ros_node/vio0_pipe: qvio
 * /mpa/voxl_mpa_to_ros_node/vio0_publish: True
 * /mpa/voxl_mpa_to_ros_node/vio1_pipe: 
 * /mpa/voxl_mpa_to_ros_node/vio1_publish: False
 * /rosdistro: indigo
 * /rosversion: 1.11.21

NODES
  /mpa/
    voxl_mpa_to_ros_node (voxl_mpa_to_ros/voxl_mpa_to_ros_node)

ROS_MASTER_URI=http://192.168.1.188:11311/

core service [/rosout] found
process[mpa/voxl_mpa_to_ros_node-1]: started with pid [4257]
Param: "image3_publish" set to false, not publishing associated interface
Param: "image4_publish" set to false, not publishing associated interface
Param: "image5_publish" set to false, not publishing associated interface
Param: "vio1_publish" set to false, not publishing associated interface


MPA to ROS app is now running

Starting Manager Thread with 8 interfaces

Found pipe for interface: tracking, now advertising
Did not find pipe for interface: hires_preview,
    interface will be idle until its pipe appears
Did not find pipe for interface: qvio_overlay,
    interface will be idle until its pipe appears
Found pipe for interface: stereo, now advertising
Found pipe for interface: tof, now advertising
Found pipe for interface: imu0, now advertising
Found pipe for interface: imu1, now advertising
Found pipe for interface: qvio, now advertising
```

### Launch RVIZ on Desktop

On the host computer, run RVIZ

```bash
rviz
```
First configure the Global Options > Fixed Frame. By default, the dropdown is set to 'map'. Change this value to 'world'.

To view the Point Cloud from ToF sensor:

1. On the leftmost column click on the “Add” button
2. In the pop-up options click on “PointCloud2”
3. Change Display Name to “tof”
4. In the left column under "tof" tab select type in Image Topic as /tof/point_cloud
5. If needed, set the "Global Options" "Fixed Frame" to "map"
6. If needed, Under the "tof" node, set Style to "Points"

To View the Tracking image sensors:

1. On the leftmost column click on the “Add” button
2. In the pop-up options click on “Image”
3. Change Display Name to “tracking” and click "OK"
4. In the left column, expand the “tracking” node and select type in Image Topic as /tracking/image_raw

To View the Stereo image sensors:

1. On the leftmost column click on the “Add” button
2. In the pop-up options click on “Image”
3. Change Display Name to “stereo_left”
4. In the left column under “stereo_left” tab select type in Image Topic as /stereo/left/image_raw
5. On the leftmost column click on the “Add” button
6. In the pop-up options click on “Image”
7. Change Display Name to “stereo_right”
8. In the left column under “stereo_right” tab select type in Image Topic as /stereo/right/image_raw

![voxl cam RVIZ](/images/voxl-cam/voxl-cam-rviz.png)

---

